#define F_CPU 11059200UL

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

#define SZ_DATA_ARRAY 100

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();
    HAL_time_init();

    init_timer();

    sei();

    // Test data set for UART communcation
    uint8_t data_array[SZ_DATA_ARRAY];
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        data_array[i] = 'a' + i % 25;
    }

    printf("======= UARTM_Inst initialize =======\n");
    UARTM_Inst.init();

    /***** Test the hal uart i/o operation *****/
    printf("======= One byte write test =======\n");
    TypeOfUARTInst UART_Main = UARTM_Inst;
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        UART_Main.write_byte(data_array[i]);  // Write data with one byte method
    }

    _delay_ms(100);
    printf("\n======= Mulit-bytes write test =======\n");
    UART_Main.write_multi_bytes(
        data_array, SZ_DATA_ARRAY);  // Wrtie data with multi-bytes method

    _delay_ms(100);
    printf("\n======= One byte read test =======\n");
    UART_Main.read_byte(data_array);  // Read data with one byte method
    printf("\nError code: %d", *UART_Main.error_code);
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        printf("data_array[%d] = %02X %c\n", i, data_array[i], data_array[i]);
    }

    _delay_ms(100);
    printf("\n======= Mulit-bytes read test =======\n");
    UART_Main.read_multi_bytes(
        data_array, SZ_DATA_ARRAY);  // Read data with multi-bytes method
    printf("Error code: %d\n", *UART_Main.error_code);
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        printf("data_array[%d] = %02X %c\n", i, data_array[i], data_array[i]);
    }

    while (1)
        ;  // Block here
}
