#define F_CPU 11059200UL

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/exfunc.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"

typedef struct {
    uint8_t id;
} TestStruct_t;

void test_mode100trm(char ASAID, char RegAdd, char Bytes, void *Data_p,
                     void *str_p);
void test_mode100rec(char ASAID, char RegAdd, char Bytes, void *Data_p,
                     void *str_p);
void test_mode100ftm(char ASAID, char RegAdd, char Mask, char Shift,
                     void *Data_p, void *str_p);
void test_mode100frc(char ASAID, char RegAdd, char Mask, char Shift,
                     void *Data_p, void *str_p);

TestStruct_t inside_test_st = {.id = 23};

int main(void) {
    C4M_STDIO_init();
    uint8_t new_id = 0;
    SeiralPortMNewMode_t TWI_NewMode_st;
    TWIMNewMode_net(&TWI_NewMode_st);

    new_id = SeiralPortMNewMode_reg(&TWI_NewMode_st, test_mode100trm, test_mode100rec,
                         test_mode100ftm, test_mode100frc, &inside_test_st);

    TWIM_trm(new_id, 0, 0, 0, 0);
    TWIM_rec(new_id, 0, 0, 0, 0);
    TWIM_frc(new_id, 0, 0, 0, 0, 0);
    TWIM_ftm(new_id, 0, 0, 0, 0, 0);
}

void test_mode100trm(char ASAID, char RegAdd, char Bytes, void *Data_p,
                     void *str_p) {
    TestStruct_t *p = (TestStruct_t *)str_p;
    printf("test_trm_message!!\n");
    printf("testmessage = %d\n", p->id);
}

void test_mode100rec(char ASAID, char RegAdd, char Bytes, void *Data_p,
                     void *str_p) {
    printf("test_rec_message!!\n");
}

void test_mode100ftm(char ASAID, char RegAdd, char Mask, char Shift,
                     void *Data_p, void *str_p) {
    printf("TWI_NewMode_ftm_message!!\n");
}

void test_mode100frc(char ASAID, char RegAdd, char Mask, char Shift,
                     void *Data_p, void *str_p) {
    printf("TWI_NewMode_frc_message!!\n");
}
