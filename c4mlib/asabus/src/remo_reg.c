#include "remo_reg.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"

#include <stddef.h>
#include <stdint.h>

#include "c4mlib/config/remo_reg.cfg"

/* Library memory mapping table Start */
SerialIsr_t *ASAUARTSerialIsrStr = 0;
SerialIsr_t *ASASPISerialIsrStr = 0;
SerialIsr_t *ASATWISerialIsrStr = 0;
/* Library memory mapping table End */

#define RES_NULLPTR 255
#define RES_EXT_MAX 254

static uint8_t d_id = 0;     /**< 暫存待設定ID*/
static uint8_t ctl_flag = 0; /**< 儲存待處理控制旗標*/

void Null_ISR(){};

// Write UID config to EEPROM
void write_uid_config() {
    if (ctl_flag == 1) {
        AsaConfig_t temp_config = ASAConfigStr_inst;
        temp_config.ASA_ID = d_id;
        EEPROM_set(0, sizeof(AsaConfig_t), &temp_config);
        DEBUG_INFO("ctrl flag : %d\n", temp_config.ASA_ID);
    }
    else {
        DEBUG_INFO("Can not do eeprom\n");
    }
    // Reset ctl_flag
    ctl_flag = 0;
}

// Initialize the Remoregister type
void SerialIsr_net(SerialIsr_t *ISR_p, void *cbf_p) {
    if (ISR_p == NULL) {
        return;
    }

    // Register UID temporary memory and control flag memory
    RemoRW_reg(ISR_p, &d_id, 1);
    uint8_t ctl_id = RemoRW_reg(ISR_p, &ctl_flag, 1);

    // Register Control flag change event callback
    ISR_p->remo_reg[ctl_id].func_p = write_uid_config;

    switch (ISR_p->SERIAL_TYPE) {
        case SERIAL_TYPE_UART: {
            ASAUARTSerialIsrStr = ISR_p;
            // Connect Slave module UART_rx() to HAL_UART
            UARTS_Inst.rx_compelete_cb = cbf_p;
            // Initialize the UARTS module
            UARTS_Inst.init();
            break;
        }
        case SERIAL_TYPE_SPI: {
            ASASPISerialIsrStr = ISR_p;
            SPIS_Inst.spi_compelete_cb = cbf_p;
            SPIS_Inst.init();
            break;
        }
        case SERIAL_TYPE_TWI: {
            ASATWISerialIsrStr = ISR_p;
            // FIXME: ISR()必須修改成讀取ASATWISerialIsrStr來操作
            // FIXME: 需實作 HAL_SPI 並 於此將 TWIn_isr()連接到HAL_TWI的TWI_isr
            // cb function
            break;
        }
        default: { break; }
    }
}

// Configure remote accessible register
uint8_t RemoRW_reg(SerialIsr_t *ISR_p, void *data_p, uint8_t bytes) {
    if (data_p == NULL) {
        return RES_NULLPTR;
    }
    else if (ISR_p->reg_counter + 1 >= REG_MAX_COUNT) {
        return RES_EXT_MAX;
    }

    uint8_t new_reg_id = ISR_p->reg_counter;
    ISR_p->reg_counter++;

    // Configure the Register pointer and size
    ISR_p->remo_reg[new_reg_id].data_p = data_p;
    ISR_p->remo_reg[new_reg_id].sz_reg = bytes;

    // return the added register id
    return new_reg_id;
}
