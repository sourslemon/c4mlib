#ifndef C4MLIB_ASABUS_EXFUNC_H
#define C4MLIB_ASABUS_EXFUNC_H

#include "c4mlib/macro/src/std_type.h"

#include "c4mlib/config/interrupt.cfg"
#include "c4mlib/config/newmode.cfg"

#define MAX_EXTFUNC_FuncNum 2
#define EXFUNC_TWI 1
#define EXFUNC_UART 2
#define EXFUNC_SPI 3

/**
 * @brief 傳送通訊Callback函式資料型態
 *
 */
typedef char (*trmFun_t)(char ASAID, char RegAdd, char Bytes, void *Data_p,
                         void *str_p);

/**
 * @brief 接收通訊Callback函式資料型態
 *
 */
typedef char (*recFun_t)(char ASAID, char RegAdd, char Bytes, void *Data_p,
                         void *str_p);

/**
 * @brief 指標傳送通訊Callback函式資料型態
 *
 */
typedef char (*ftmFun_t)(char ASAID, char RegAdd, char Mask, char shift,
                         void *Data, void *str_p);

/**
 * @brief 指標接收通訊Callback函式資料型態
 *
 */
typedef char (*frcFun_t)(char ASAID, char RegAdd, char Mask, char shift,
                         void *Data_p, void *str_p);

/**
 * @brief 外掛通訊送收資料結構型態
 *
 */
typedef struct {
    trmFun_t trmFun; /*傳送通訊Callback函式 */
    recFun_t recFun; /*接收通訊Callback函式 */
    ftmFun_t ftmFun; /*指標傳送通訊Callback函式 */
    frcFun_t frcFun; /*指標接收通訊Callback函式 */
    void *DataStr_p; /*外掛函式通用結構化資料 */
} ExTranceverFunBlock_t;

/**
 * @brief 串列通訊埠新增模式管理用資料結構形態
 *
 */
typedef struct {
    uint8_t total;       /*紀錄本結構已新增模式的總數 */
    volatile ExTranceverFunBlock_t
        ExTCFB_list[MAX_EXTFUNC_FuncNum]; /*儲存所有新增模式 */
} SeiralPortMNewMode_t;

/**
 * @brief net the SeiralPortMNewMode_t structer in UART
 *
 * @param NewMode_p
 * @return char
 */
char UARTMNewMode_net(SeiralPortMNewMode_t *NewMode_p);

/**
 * @brief net the SeiralPortMNewMode_t structer in SPI
 *
 * @param NewMode_p
 * @return char
 */
char SPIMNewMode_net(SeiralPortMNewMode_t *NewMode_p);

/**
 * @brief net the SeiralPortMNewMode_t structer in TWI
 *
 * @param NewMode_p
 * @return char
 */
char TWIMNewMode_net(SeiralPortMNewMode_t *NewMode_p);

/**
 * @brief 註冊新mode之通用函式
 *
 * @param exFuncStr_p
 * @param trmFun_p
 * @param recFun_p
 * @param ftmFun_p
 * @param frcFun_p
 * @param TRMRECstr_p
 * @return char
 */
char SeiralPortMNewMode_reg(SeiralPortMNewMode_t *exFuncStr_p, trmFun_t trmFun_p,
                 recFun_t recFun_p, ftmFun_t ftmFun_p, frcFun_t frcFun_p,
                 void *TRMRECstr_p);

/**
 * @brief 呼叫傳送通訊Callback函式
 *
 * @param SERIAL_TYPE
 * @param mode
 * @param ASAID
 * @param RegAdd
 * @param Bytes
 * @param Data_p
 * @return char
 */
char ExFunc_trm(char SERIAL_TYPE, char mode, char ASAID, char RegAdd,
                char Bytes, void *Data_p);
/**
 * @brief 呼叫接收通訊Callback函式
 *
 * @param SERIAL_TYPE
 * @param mode
 * @param ASAID
 * @param RegAdd
 * @param Bytes
 * @param Data_p
 * @return char
 */
char ExFunc_rec(char SERIAL_TYPE, char mode, char ASAID, char RegAdd,
                char Bytes, void *Data_p);

/**
 * @brief 呼叫指標傳送通訊Callback函式
 *
 * @param SERIAL_TYPE
 * @param mode
 * @param ASAID
 * @param RegAdd
 * @param Mask
 * @param Shift
 * @param Data_p
 * @return char
 */
char ExFunc_ftm(char SERIAL_TYPE, char mode, char ASAID, char RegAdd, char Mask,
                char Shift, void *Data_p);

/**
 * @brief 呼叫指標接收通訊Callback函式
 *
 * @param SERIAL_TYPE
 * @param mode
 * @param ASAID
 * @param RegAdd
 * @param Mask
 * @param Shift
 * @param Data_p
 * @return char
 */
char ExFunc_frc(char SERIAL_TYPE, char mode, char ASAID, char RegAdd, char Mask,
                char Shift, void *Data_p);

extern volatile SeiralPortMNewMode_t *ASAUARTExfuncIsrStr;
extern volatile SeiralPortMNewMode_t *ASASPIExfuncIsrStr;
extern volatile SeiralPortMNewMode_t *ASATWIExfuncIsrStr;

#endif
