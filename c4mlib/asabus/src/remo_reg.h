/**
 * @file remo_reg.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.03.29
 * @brief provride a remote accessable register structer
 */

#ifndef C4MLIB_ASABUS_REMO_REG_H
#define C4MLIB_ASABUS_REMO_REG_H

#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/remo_reg.cfg"

#define REMOTE_REG_WRITE 0
#define REMOTE_REG_READ  1

// TODO: 修改下方結構註解
/**
 * @brief 遠端暫存器
 *
 * 遠端暫存器的結構。
 */
typedef struct {
    volatile uint8_t *data_p;  ///< 遠端暫存器之位址。
    uint8_t sz_reg;            ///< 遠端暫存器之位元組數。
    Func_t func_p;             ///< 遠端暫存器修改事件Callback函式。
    void *funcPara_p;  ///< Callback函式接受之參數。(function parameter)
} RemoReg_t;

// TODO: 修改下方結構註解
/**
 * @brief 串列通訊埠管理用資料結構形態
 *
 * 負責處理ASABUS上的ASAUART、ASASPI、ASATWI通訊，此結構中包然解包器狀態機、及REMOREG的實例。
 * 在僕裝置上皆會有一個SerialIsr_t來管理RemoReg及通訊狀態機。
 */
typedef struct {
    const uint8_t
        SERIAL_TYPE;  ///< 紀錄本結構的通訊種類，用於init()函式判斷鏈接目標。
    uint8_t d_id;               ///< 裝置ID。(device id)
    uint8_t sm_status;          ///< 狀態機當前狀態。
    uint8_t reg_counter;        ///< 已註冊的暫存器個數。
    uint8_t byte_counter;       ///< 暫存已接收位元組數。
    uint8_t reg_address;        ///< 暫存封包解讀出的暫存器位址。
    uint8_t rw_mode;            ///< 暫存封包解讀出的 R/W 標示符。
    uint8_t check_sum;          ///< 暫存解包每一步的chechsum結果。
    uint8_t result_message;     ///< 儲存解包器運作狀態編號。
    uint8_t temp[BUFF_MAX_SZ];  ///< 暫存待處理之payload data。
    RemoReg_t
        remo_reg[REG_MAX_COUNT];  ///< 儲存所有已註冊之遠端可讀寫暫存器。
} SerialIsr_t;

// Initialize the SerialIsr_t structer
void SerialIsr_net(SerialIsr_t *ISR_p, void *cbf_p);

// Configure remote accessible register
uint8_t RemoRW_reg(SerialIsr_t *ISRStr_p, void *data_p, uint8_t bytes);

/* Library memory mapping table Start */
extern SerialIsr_t *ASAUARTSerialIsrStr;
extern SerialIsr_t *ASASPISerialIsrStr;
extern SerialIsr_t *ASATWISerialIsrStr;
/* Library memory mapping table End */

#endif  // C4MLIB_ASABUS_ASA_REMO_REG_H
