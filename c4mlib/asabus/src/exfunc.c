#include "c4mlib/asabus/src/exfunc.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/macro/src/std_res.h"

volatile SeiralPortMNewMode_t* ASAUARTExfuncIsrStr = 0;
volatile SeiralPortMNewMode_t* ASASPIExfuncIsrStr = 0;
volatile SeiralPortMNewMode_t* ASATWIExfuncIsrStr = 0;

char TWIMNewMode_net(SeiralPortMNewMode_t* NewMode_p) {
    if (NewMode_p != NULL) {
        NewMode_p->total = 0;
        ASATWIExfuncIsrStr = NewMode_p;
        return NewMode_p->total;
    }
    else {
        DEBUG_INFO("SeiralPortMNewMode_t NULL");
        return 99;
    }
}

char UARTMNewMode_net(SeiralPortMNewMode_t* NewMode_p) {
    if (NewMode_p != NULL) {
        NewMode_p->total = 0;
        ASAUARTExfuncIsrStr = NewMode_p;
        return NewMode_p->total;
    }
    else {
        DEBUG_INFO("SeiralPortMNewMode_t NULL");
        return 99;
    }
}

char SPIMNewMode_net(SeiralPortMNewMode_t* NewMode_p) {
    if (NewMode_p != NULL) {
        NewMode_p->total = 0;
        ASASPIExfuncIsrStr = NewMode_p;
        return NewMode_p->total;
    }
    else {
        DEBUG_INFO("SeiralPortMNewMode_t NULL");
        return 99;
    }
}

char SeiralPortMNewMode_reg(SeiralPortMNewMode_t* exFuncStr_p,
                            trmFun_t trmFun_p, recFun_t recFun_p,
                            ftmFun_t ftmFun_p, frcFun_t frcFun_p,
                            void* TRMRECstr_p) {
    if (exFuncStr_p == NULL) {
        DEBUG_INFO("SeiralPortMNewMode_t NULL");
        return 99;
    }
    uint8_t new_mode = 100 + (exFuncStr_p->total);
    exFuncStr_p->ExTCFB_list[exFuncStr_p->total].trmFun = trmFun_p;
    exFuncStr_p->ExTCFB_list[exFuncStr_p->total].recFun = recFun_p;
    exFuncStr_p->ExTCFB_list[exFuncStr_p->total].ftmFun = ftmFun_p;
    exFuncStr_p->ExTCFB_list[exFuncStr_p->total].frcFun = frcFun_p;
    exFuncStr_p->ExTCFB_list[exFuncStr_p->total].DataStr_p = (void*)TRMRECstr_p;
    exFuncStr_p->total++;

    return new_mode;
}

char ExFunc_trm(char SERIAL_TYPE, char mode, char ASAID, char RegAdd,
                char Bytes, void* Data_p) {
    uint8_t mode_id = mode - 100;
    char chk = 0;
    switch (SERIAL_TYPE) {
        case EXFUNC_TWI:
            if (mode_id > ASATWIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASATWIExfuncIsrStr->ExTCFB_list[mode_id].trmFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASATWIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;

        case EXFUNC_UART:
            if (mode_id > ASAUARTExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].trmFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        case EXFUNC_SPI:
            if (mode_id > ASASPIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASASPIExfuncIsrStr->ExTCFB_list[mode_id].trmFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASASPIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        default:
            break;
    }
    return 99;
}

char ExFunc_rec(char SERIAL_TYPE, char mode, char ASAID, char RegAdd,
                char Bytes, void* Data_p) {
    uint8_t mode_id = mode - 100;
    char chk = 0;
    switch (SERIAL_TYPE) {
        case EXFUNC_TWI:
            if (mode_id > ASATWIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASATWIExfuncIsrStr->ExTCFB_list[mode_id].recFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASATWIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;

        case EXFUNC_UART:
            if (mode_id > ASAUARTExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].recFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        case EXFUNC_SPI:
            if (mode_id > ASASPIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASASPIExfuncIsrStr->ExTCFB_list[mode_id].recFun(
                ASAID, RegAdd, Bytes, Data_p,
                ASASPIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        default:
            break;
    }
    return 99;
}

char ExFunc_ftm(char SERIAL_TYPE, char mode, char ASAID, char RegAdd, char Mask,
                char Shift, void* Data_p) {
    uint8_t mode_id = mode - 100;
    char chk = 0;
    switch (SERIAL_TYPE) {
        case EXFUNC_TWI:
            if (mode_id > ASATWIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASATWIExfuncIsrStr->ExTCFB_list[mode_id].ftmFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASATWIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;

        case EXFUNC_UART:
            if (mode_id > ASAUARTExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].ftmFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        case EXFUNC_SPI:
            if (mode_id > ASASPIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASASPIExfuncIsrStr->ExTCFB_list[mode_id].ftmFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASASPIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        default:
            break;
    }
    return 99;
}

char ExFunc_frc(char SERIAL_TYPE, char mode, char ASAID, char RegAdd, char Mask,
                char Shift, void* Data_p) {
    uint8_t mode_id = mode - 100;
    char chk = 0;
    switch (SERIAL_TYPE) {
        case EXFUNC_TWI:
            if (mode_id > ASATWIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASATWIExfuncIsrStr->ExTCFB_list[mode_id].frcFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASATWIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        case EXFUNC_UART:
            if (mode_id > ASAUARTExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].frcFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASAUARTExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        case EXFUNC_SPI:
            if (mode_id > ASASPIExfuncIsrStr->total) {
                return HAL_ERROR_MODE_SELECT;
            }
            chk = ASASPIExfuncIsrStr->ExTCFB_list[mode_id].frcFun(
                ASAID, RegAdd, Mask, Shift, Data_p,
                ASASPIExfuncIsrStr->ExTCFB_list[mode_id].DataStr_p);
            return chk;
        default:
            break;
    }
    return 99;
}
