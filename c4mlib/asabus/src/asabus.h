#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/hardware/src/hal_uart.h"

#ifndef C4MLIB_ASABUS_H
#    define C4MLIB_ASABUS_H

#    if defined(__AVR_ATmega128__)
#        define UARTM_Inst (UART1_Inst)
#        define UARTS_Inst (UART1_Inst)
#        define SPIM_Inst (SPI_MasterInst)
#        define SPIS_Inst (SPI_SlaveInst)
#    elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
        defined(__AVR_ATmega168__)
#        define UARTS_Inst (UART_Inst)
#    elif defined(__AVR_ATtiny2313__)
#        define UARTS_Inst (UART_Inst)
#    else
#        if !defined(__COMPILING_AVR_LIBC__)
#            warning "device type not defined"
#        endif
#    endif

/* Public Section Start */
void ASABUS_ID_init(void);
void ASABUS_ID_set(char id);

void ASABUS_UART_init(void);
void ASABUS_UART_transmit(char data);
char ASABUS_UART_receive(void);

void ASABUS_SPI_init(void);
char ASABUS_SPI_swap(char data);
/* Public Section End */

#endif  // C4MLIB_ASABUS_H
