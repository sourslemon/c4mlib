/**
 * @file dio.c
 * @author LiYu87
 * @author ya058764
 * @brief DIO及EXT函式實作
 * @date 2019.07.29
 *
 * EXT類函式為DIO的重名，目的是為了與舊版本規格書相容。(2019.06.24)
 */

#include "c4mlib/hardware/src/dio.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#define RES_WARNING_IO_CONFLICT 2

/* public functions declaration section ------------------------------------- */
char DIO_fpt(char LSByte, char Mask, char Shift, char Data);
char DIO_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char DIO_put(char LSByte, char Bytes, void *Data_p);
char DIO_get(char LSByte, char Bytes, void *Data_p);

char DIO_set(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("DIO_fpt")));

char EXT_fpt(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("DIO_fpt")));

char EXT_fgt(char LSByte, char Mask, char Shift, void *Data_p)
    __attribute__((alias("DIO_fgt")));

char EXT_set(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("DIO_fpt")));
/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
char DIO_fpt(char LSByte, char Mask, char Shift, char Data) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 0:
                REGFPT(PORTA, Mask, Shift, Data);
                if ((Mask & DDRA) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 1:
                REGFPT(PORTB, Mask, Shift, Data);
                if ((Mask & DDRB) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 2:
                REGFPT(PORTC, Mask, Shift, Data);
                if ((Mask & DDRC) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 3:
                REGFPT(PORTD, Mask, Shift, Data);
                if ((Mask & DDRD) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 4:
                REGFPT(PORTE, Mask, Shift, Data);
                if ((Mask & DDRE) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 5:
                REGFPT(PORTF, Mask, Shift, Data);
                if ((Mask & DDRF) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 6:
                REGFPT(PORTG, Mask, Shift, Data);
                if ((Mask & DDRG) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 200:
                REGFPT(DDRA, Mask, Shift, Data);
                break;
            case 201:
                REGFPT(DDRB, Mask, Shift, Data);
                break;
            case 202:
                REGFPT(DDRC, Mask, Shift, Data);
                break;
            case 203:
                REGFPT(DDRD, Mask, Shift, Data);
                break;
            case 204:
                REGFPT(DDRE, Mask, Shift, Data);
                break;
            case 205:
                REGFPT(DDRF, Mask, Shift, Data);
                break;
            case 206:
                REGFPT(DDRG, Mask, Shift, Data);
                break;
            case 210:
                REGFPT(EIMSK, Mask, Shift, Data);
                break;
            case 211:
                REGFPT(EICRA, Mask, Shift, Data);
                break;
            case 212:
                REGFPT(EICRB, Mask, Shift, Data);
                break;
            case 213:
                REGFPT(EIFR, Mask, Shift, Data);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char DIO_fgt(char LSByte, char Mask, char Shift, void *Data_p) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 100:
                REGFGT(PINA, Mask, Shift, Data_p);
                if (((~DDRA) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 101:
                REGFGT(PINB, Mask, Shift, Data_p);
                if (((~DDRB) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 102:
                REGFGT(PINC, Mask, Shift, Data_p);
                if (((~DDRC) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
            case 103:
                REGFGT(PIND, Mask, Shift, Data_p);
                if (((~DDRD) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 104:
                REGFGT(PINE, Mask, Shift, Data_p);
                if (((~DDRE) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 105:
                REGFGT(PINF, Mask, Shift, Data_p);
                if (((~DDRF) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 106:
                REGFGT(PING, Mask, Shift, Data_p);
                if (((~DDRG) & Mask) == Mask)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 210:
                REGFGT(EIMSK, Mask, Shift, Data_p);
                break;
            case 211:
                REGFGT(EICRA, Mask, Shift, Data_p);
                break;
            case 212:
                REGFGT(EICRB, Mask, Shift, Data_p);
                break;
            case 213:
                REGFGT(EIFR, Mask, Shift, Data_p);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char DIO_put(char LSByte, char Bytes, void *Data_p) {
    if (Bytes == 1) {
        switch (LSByte) {
            case 0:
                PORTA = *((char *)Data_p);
                if (DDRA == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 1:
                PORTB = *((char *)Data_p);
                if (DDRB == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 2:
                PORTC = *((char *)Data_p);
                if (DDRC == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 3:
                PORTD = *((char *)Data_p);
                if (DDRD == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 4:
                PORTE = *((char *)Data_p);
                if (DDRE == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 5:
                PORTF = *((char *)Data_p);
                if (DDRF == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 6:
                PORTG = *((char *)Data_p);
                if (DDRG == 0xff)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 200:
                DDRA = *((char *)Data_p);
                break;
            case 201:
                DDRB = *((char *)Data_p);
                break;
            case 202:
                DDRC = *((char *)Data_p);
                break;
            case 203:
                DDRD = *((char *)Data_p);
                break;
            case 204:
                DDRE = *((char *)Data_p);
                break;
            case 205:
                DDRF = *((char *)Data_p);
                break;
            case 206:
                DDRG = *((char *)Data_p);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}

char DIO_get(char LSByte, char Bytes, void *Data_p) {
    if (Bytes == 1) {
        switch (LSByte) {
            case 100:
                *((char *)Data_p) = PINA;
                if (DDRA == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 101:
                *((char *)Data_p) = PINB;
                if (DDRB == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 102:
                *((char *)Data_p) = PINC;
                if (DDRC == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 103:
                *((char *)Data_p) = PIND;
                if (DDRD == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 104:
                *((char *)Data_p) = PINE;
                if (DDRE == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 105:
                *((char *)Data_p) = PINF;
                if (DDRF == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            case 106:
                *((char *)Data_p) = PING;
                if (DDRG == 0x00)
                    return RES_OK;
                else
                    return RES_WARNING_IO_CONFLICT;
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}
