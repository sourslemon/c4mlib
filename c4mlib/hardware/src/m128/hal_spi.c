/**
 * @file hal_spi.c
 * @author Deng Xiang-Guan
 * @date 2019.01.05
 * @brief Implement a hardware abstraction layer for atmega128.
 */

// #define USE_C4MLIB_DEBUG
#include "c4mlib/hardware/src/hal_spi.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/io.h>
#include <stddef.h>

// TODO: 確認pin_def的必要性
// TODO: 50UL的逾時定義

/* public functions declaration section ------------------------------------- */

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */
static void M128_SPI_Master_init(void);
static void M128_SPI_Slave_init(void);
static uint8_t M128_SPI_swap(uint8_t data);
static void M128_SPI_multi_swap(uint8_t* trm_data, uint8_t* rec_data,
                                uint8_t sizeOfData);
static void M128_SPI_write_byte(uint8_t data);
static void M128_SPI_write_multi_bytes(uint8_t* data_p, uint8_t sizeOfData);
static uint8_t M128_SPI_read_byte(void);
static void M128_SPI_read_multi_bytes(uint8_t* data_p, uint8_t sizeOfData);
static void M128_SPI_enable_cs(char id);
static void M128_SPI_disable_cs(char id);

static uint8_t M128_SPIS_read_byte(void);
static void M128_SPIS_write_byte(uint8_t sendData);

/* public variables declaration section ------------------------------------- */
TypeOfMasterSPIInst SPI_MasterInst = {
    .init = M128_SPI_Master_init,
    .spi_swap = M128_SPI_swap,
    .spi_multi_swap = M128_SPI_multi_swap,
    .spi_compelete_cb = 0,
    .write_byte = M128_SPI_write_byte,
    .write_multi_bytes = M128_SPI_write_multi_bytes,
    .read_byte = M128_SPI_read_byte,
    .read_multi_bytes = M128_SPI_read_multi_bytes,
    .enable_cs = M128_SPI_enable_cs,
    .disable_cs = M128_SPI_disable_cs};

TypeOfSlaveSPIInst SPI_SlaveInst = {.init = M128_SPI_Slave_init,
                                    .spi_compelete_cb = 0,
                                    .write_byte = M128_SPIS_write_byte,
                                    .read_byte = M128_SPIS_read_byte};

/* static function contents section ----------------------------------------- */
// TODO: Function comment
static void M128_SPI_Master_init(void) {
    // Initialize ASA SPI ID
    ASABUS_ID_init();
    // Setup SPI pins
    DDRB |= (1 << BUS_SPI_MOSI) | (1 << BUS_SPI_SCK) | (1 << BUS_SPI_SS);
    DDRB &= ~(1 << BUS_SPI_MISO);
    // Master CS pin is PF4
    ASA_CS_DDR |= (1 << ASA_CS);
    // SCK oscillator frequency divided 128
    // SPI Master mode
    // Enable SPI
    SPCR |= (1 << SPR0) | (1 << SPR1) | (1 << MSTR) | (1 << SPE);
}

// TODO: Function comment
static void M128_SPI_Slave_init(void) {
    DDRB &= ~(1 << BUS_SPI_MOSI) & ~(1 << BUS_SPI_SCK) & ~(1 << BUS_SPI_SS);
    DDRB |= (1 << BUS_SPI_MISO);
    // Enable SPI
    SPCR = (1 << SPE);
    SPCR |= (1 << SPIE);
    sei();
}

// TODO: Function comment
static uint8_t M128_SPI_swap(uint8_t data) {
    SPDR = data;
    HAL_Time expire_time;
    HAL_get_expired_time_ms(50UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    while (!(SPSR & (1 << SPIF))) {
        if (HAL_timeout_test(expire_time)) {  // Check timeout
            SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
            DEBUG_INFO("SPI swap TIMEOUT\n");
            return 0;
        }
    }
    return SPDR;
}

// TODO: Function comment
static void M128_SPI_multi_swap(uint8_t* trm_data, uint8_t* rec_data,
                                uint8_t sizeOfData) {
    do {
        SPDR = *(trm_data++);
        HAL_Time expire_time;
        HAL_get_expired_time_ms(
            50UL, &expire_time);  // Get current time tick (Unit: 1ms)
        while (!(SPSR & (1 << SPIF))) {
            if (HAL_timeout_test(expire_time)) {  // Check timeout
                SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
                DEBUG_INFO("SPI swap TIMEOUT\n");
                return;
            }
        }
        *(rec_data++) = SPDR;
    } while (--sizeOfData);
}

// TODO: Function comment
static void M128_SPI_write_byte(uint8_t data) {
    SPDR = data;
    HAL_Time expire_time;
    HAL_get_expired_time_ms(50UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    while (!(SPSR & (1 << SPIF))) {
        if (HAL_timeout_test(expire_time)) {  // Check timeout
            SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
            DEBUG_INFO("SPI swap TIMEOUT\n");
            return;
        }
    }
}

// TODO: Function comment
static void M128_SPI_write_multi_bytes(uint8_t* data_p, uint8_t sizeOfData) {
    do {
        SPDR = *(data_p++);
        HAL_Time expire_time;
        HAL_get_expired_time_ms(
            50UL, &expire_time);  // Get current time tick (Unit: 1ms)
        while (!(SPSR & (1 << SPIF))) {
            if (HAL_timeout_test(expire_time)) {  // Check timeout
                SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
                DEBUG_INFO("SPI swap TIMEOUT\n");
                return;
            }
        }
    } while (--sizeOfData);
}

// TODO: Function comment
static uint8_t M128_SPI_read_byte(void) {
    HAL_Time expire_time;
    HAL_get_expired_time_ms(50UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    while (!(SPSR & (1 << SPIF))) {
        if (HAL_timeout_test(expire_time)) {  // Check timeout
            SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
            DEBUG_INFO("SPI swap TIMEOUT\n");
            return 0;
        }
    }
    return SPDR;
}

// TODO: Function comment
static void M128_SPI_read_multi_bytes(uint8_t* data_p, uint8_t sizeOfData) {
    do {
        HAL_Time expire_time;
        HAL_get_expired_time_ms(
            50UL, &expire_time);  // Get current time tick (Unit: 1ms)
        while (!(SPSR & (1 << SPIF))) {
            if (HAL_timeout_test(expire_time)) {  // Check timeout
                SPI_MasterInst.error_code = HAL_ERROR_TIMEOUT;
                DEBUG_INFO("SPI swap TIMEOUT\n");
                return;
            }
        }
        *(data_p++) = SPDR;
    } while (--sizeOfData);
}

// TODO: Function comment
static void M128_SPI_enable_cs(char id) {
    ASABUS_ID_set(id);
    ASA_CS_PORT &= ~(1 << ASA_CS);
}

// TODO: Function comment
static void M128_SPI_disable_cs(char id) {
    ASABUS_ID_set(id);
    ASA_CS_PORT |= (1 << ASA_CS);
    ASABUS_ID_set(0);
}

// TODO: Function comment
static void M128_SPIS_write_byte(uint8_t sendData) {
    SPDR = sendData;
}

// TODO: Function comment
static uint8_t M128_SPIS_read_byte(void) {
    return SPDR;
}
