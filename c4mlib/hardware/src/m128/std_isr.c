/**
 * @file std_isr.h
 * @author Deng Xiang-Guan
 * @date 2019.08.06
 * @brief Implement all INTERNAL_ISR in this file (TIMER_n, EXT_n, SPI_n, TWI_n, UARTTX_n, UARTRX_n).
 */
// #define USE_C4MLIB_DEBUG
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/hardware/src/hal_uart.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/hardware2/src/adc.h"
#include "c4mlib/hardware2/src/adc_imp.h"
#include "c4mlib/hardware2/src/ext_imp.h"
#include "c4mlib/hardware2/src/extint.h"
#include "c4mlib/hardware2/src/pwm.h"
#include "c4mlib/hardware2/src/pwm_imp.h"
#include "c4mlib/hardware2/src/spi.h"
#include "c4mlib/hardware2/src/spi_imp.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/hardware2/src/tim_imp.h"
#include "c4mlib/hardware2/src/twi.h"
#include "c4mlib/hardware2/src/twi_imp.h"
#include "c4mlib/hardware2/src/uart.h"
#include "c4mlib/hardware2/src/uart_imp.h"

/*********************** TIMERn vector start ***********************/
INTERNAL_ISR(TIMER0_OVF_vect) {
    // Make sure you call the net function first.
    if (PwmIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! PwmIntStrList_p[0] is NULL\n");
        return;
    }
    else {
        PwmInt_step(PwmIntStrList_p[0]);
    }
}

INTERNAL_ISR(TIMER0_COMP_vect) {
    // Make sure you call the net function first.
    if (TimIntStrList_p[0] != NULL) {
        TimInt_step(TimIntStrList_p[0]);
    }
    else {
        DEBUG_INFO("Warning !!! TimIntStrList_p[0] is NULL\n");
    }
}

INTERNAL_ISR(TIMER1_OVF_vect) {
    // Make sure you call the net function first.
    if (PwmIntStrList_p[1] == NULL) {
        DEBUG_INFO("Warning !!! PwmIntStrList_p[0] is NULL\n");
        return;
    }
    else {
        PwmInt_step(PwmIntStrList_p[1]);
    }
}

INTERNAL_ISR(TIMER1_COMPA_vect) {
    // Make sure you call the net function first.
    if (TimIntStrList_p[1] == NULL) {
        DEBUG_INFO("Warning !!! TimIntStrList_p[1] is NULL\n");
        return;
    }
    TimInt_step(TimIntStrList_p[1]);
}

INTERNAL_ISR(TIMER1_COMPB_vect) {
    /* Not use this Internal ISR */
}

INTERNAL_ISR(TIMER1_COMPC_vect) {
    /* Not use this Internal ISR */
}

INTERNAL_ISR(TIMER2_OVF_vect) {
    // Make sure you call the net function first.
    if (PwmIntStrList_p[2] == NULL) {
        DEBUG_INFO("Warning !!! PwmIntStrList_p[0] is NULL\n");
        return;
    }
    else {
        PwmInt_step(PwmIntStrList_p[2]);
    }
}

INTERNAL_ISR(TIMER2_COMP_vect) {
    // Make sure you call the net function first.
    if (TimIntStrList_p[2] == NULL) {
        DEBUG_INFO("Warning !!! TimIntStrList_p[2] is NULL\n");
        return;
    }
    TimInt_step(TimIntStrList_p[2]);
}

INTERNAL_ISR(TIMER3_OVF_vect) {
    // Make sure you call the net function first.
    if (PwmIntStrList_p[3] == NULL) {
        DEBUG_INFO("Warning !!! PwmIntStrList_p[3] is NULL\n");
        return;
    }
    else {
        PwmInt_step(PwmIntStrList_p[3]);
    }
}

INTERNAL_ISR(TIMER3_COMPA_vect) {
    // Make sure you call the net function first.
    if (TimIntStrList_p[3] == NULL) {
        DEBUG_INFO("Warning !!! TimIntStrList_p[3] is NULL\n");
        return;
    }
    TimInt_step(TimIntStrList_p[3]);
}

INTERNAL_ISR(TIMER3_COMPB_vect) {
    /* Not use this Internal ISR */
}

INTERNAL_ISR(TIMER3_COMPC_vect) {
    /* Not use this Internal ISR */
}

/*********************** TIMERn compare match vector end ***********************/

/*********************** INTn vector start ***********************/
INTERNAL_ISR(INT0_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[0] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[0]);
}

INTERNAL_ISR(INT1_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[1] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[1] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[1]);
}
INTERNAL_ISR(INT2_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[2] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[2] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[2]);
}

INTERNAL_ISR(INT3_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[3] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[3] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[3]);
}

INTERNAL_ISR(INT4_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[4] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[4] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[4]);
}

INTERNAL_ISR(INT5_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[5] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[5] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[5]);
}

INTERNAL_ISR(INT6_vect) {
   // Make sure you call the net function first.
    if (ExtIntStrList_p[6] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[6] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[6]);
}

INTERNAL_ISR(INT7_vect) {
    // Make sure you call the net function first.
    if (ExtIntStrList_p[7] == NULL) {
        DEBUG_INFO("Warning !!! ExtIntStrList_p[7] is NULL\n");
        return;
    }
    ExtInt_step(ExtIntStrList_p[7]);
}
/*********************** INTn vector end ***********************/

/*********************** TWI vector start ***********************/
/* public function contents section ----------------------------------------- */
INTERNAL_ISR(TWI_vect) { 
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (TwiIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! TwiIntStrList_p[0] is NULL\n");
        return;
    }
    else {
        DEBUG_INFO("Do TWI number 0 interrupt.\r\n");
        TwiInt_step(TwiIntStrList_p[0]);
    }
}
/*********************** TWI vector end ***********************/

/*********************** SPI vector start ***********************/
INTERNAL_ISR(SPI_STC_vect) {
    if (SPI_SlaveInst.spi_compelete_cb != NULL) {
        SPI_SlaveInst.spi_compelete_cb();
    }   
    if (SpiIntStrList_p[0] == NULL) {
        DEBUG_INFO("SpiIntStrList_p SPI number 0 is NULL.\r\n");
        return;
    }
    else {
        DEBUG_INFO("Do SPI number 0 interrupt.\r\n");
        SpiInt_step(SpiIntStrList_p[0]);
    }
}
/*********************** SPI vector end ***********************/

/*********************** USARTn vector start ***********************/
INTERNAL_ISR(USART0_TX_vect) {
    if (UART0_Inst.tx_compelete_cb != NULL) {
       UART0_Inst.tx_compelete_cb(); 
    }
    if (UartTxIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! UartTxIntStrList_p[0] is NULL\n");
        return;
    }
    UartTx_step(UartTxIntStrList_p[0]);
}

INTERNAL_ISR(USART0_RX_vect) {
    if (UART0_Inst.rx_compelete_cb != NULL) {
        UART0_Inst.rx_compelete_cb();
    }
    if (UartRxIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! UartRxIntStrList_p[0] is NULL\n");
        return;
    }
    UartRx_step(UartRxIntStrList_p[0]);
}

INTERNAL_ISR(USART1_TX_vect) {
    if (UART1_Inst.tx_compelete_cb != NULL) {
        UART1_Inst.tx_compelete_cb();
    }     
    if (UartTxIntStrList_p[1] == NULL) {
        DEBUG_INFO("UartTXIntStrList_p UARTTX number 1 is NULL.\r\n");
        return;
    }
    else {
        DEBUG_INFO("Do UART number 1 interrupt.\r\n");
        UartTx_step(UartTxIntStrList_p[1]);
    }
}

INTERNAL_ISR(USART1_RX_vect) {
    if (UART1_Inst.rx_compelete_cb != NULL) {
        UART1_Inst.rx_compelete_cb();
    }
    if (UartRxIntStrList_p[1] == NULL) {
        DEBUG_INFO("UartTXIntStrList_p UARTRX number 1 is NULL.\r\n");
        return;
    }
    else {
        DEBUG_INFO("Do UART number 1 interrupt.\r\n");
        UartRx_step(UartRxIntStrList_p[1]);
    }
}
/*********************** USARTn vector end ***********************/

/*********************** ADC vector start ***********************/
INTERNAL_ISR(ADC_vect) {
    if (AdcIntStrList_p[0] == NULL) {
        DEBUG_INFO("Warning !!! AdcIntStrList_p[0] is NULL\n");
        return;
    }
    else {
        DEBUG_INFO("Do ADC number 0 interrupt.\r\n");
        AdcInt_step(AdcIntStrList_p[0]);
    }
}
/*********************** ADC vector end ***********************/
