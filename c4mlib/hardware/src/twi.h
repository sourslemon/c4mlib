#ifndef C4MLIB_HARDWARE_TWI_H
#define C4MLIB_HARDWARE_TWI_H

/* Public Section Start */
char TWI_set(char LSByte, char Mask, char Shift, char Data);
char TWI_fpt(char LSByte, char Mask, char Shift, char Data);
char TWI_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char TWI_put(char LSbyte, char Bytes, void *Data_p);
char TWI_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_TWI_H
