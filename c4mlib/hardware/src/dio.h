/**
 * @file dio.c
 * @author LiYu87
 * @brief DIO及EXT函式原形
 * @date 2019.07.29
 *
 * EXT_set 與 DIO_set 相同。
 * EXT_fpt 與 DIO_fpt 相同。
 * EXT_fgt 與 DIO_fgt 相同。
 */

#ifndef C4MLIB_HARDWARE_DIO_H
#define C4MLIB_HARDWARE_DIO_H

/* Public Section Start */
char DIO_set(char LSByte, char Mask, char Shift, char Data);
char DIO_fpt(char LSByte, char Mask, char Shift, char Data);
char DIO_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char DIO_put(char LSbyte, char Bytes, void *Data_p);
char DIO_get(char LSByte, char Bytes, void *Data_p);

char EXT_set(char LSByte, char Mask, char Shift, char Data);
char EXT_fpt(char LSByte, char Mask, char Shift, char Data);
char EXT_fgt(char LSByte, char Mask, char Shift, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_DIO_H
