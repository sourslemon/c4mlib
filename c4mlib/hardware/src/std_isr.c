
void USE_C4MLIB_STD_ISR() {
}

#if defined(__AVR_ATmega128__)
#    include "m128/std_isr.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/std_isr.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/std_isr.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
