/**
 * @file common.h
 * @author LiYu87
 * @date 2019.02.22
 * @brief
 *
 * @priority 0
 */
#ifndef C4MLIB_DEVICE_DEVICE_H
#define C4MLIB_DEVICE_DEVICE_H

#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <util/delay.h>

/* Public Section Start */
void C4M_DEVICE_set(void);
/* Public Section End */

#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asakb00/src/kb00.h"
#include "c4mlib/asastp00/src/stp00.h"
#include "c4mlib/device/src/config_str.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/stdio/src/stdio.h"

#include <stdbool.h>

// Global configuration structure
extern AsaConfig_t ASAConfigStr_inst;
extern Asa7s00Para_t ASA_7S00_str;
extern AsaKb00Para_t ASA_KB00_str;
#endif  // C4MLIB_DEVICE_DEVICE_H
