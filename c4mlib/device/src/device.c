#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/exfunc.h"
#include "c4mlib/asakb00/src/kb00.h"
#include "c4mlib/asastp00/src/stp00.h"
#include "c4mlib/device/src/config_str.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/stdio/src/stdio.h"

#include "c4mlib/config/asamodule.cfg"

/* public functions declaration section ------------------------------------- */
void C4M_DEVICE_set(void);

/* define section ----------------------------------------------------------- */

/* Global variables section ------------------------------------------------- */
AsaConfig_t ASAConfigStr_inst;
Asa7s00Para_t ASA_7S00_str = ASA_7S00_PARA_INI;
AsaKb00Para_t ASA_KB00_str = ASA_KB00_PARA_INI;
SeiralPortMNewMode_t TWI_NewMode_st;
SeiralPortMNewMode_t SPI_NewMode_st;
SeiralPortMNewMode_t UART_NewMode_st;
/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
void C4M_DEVICE_set(void) {
    C4M_STDIO_init();
    ASABUS_ID_init();
    ASABUS_SPI_init();
    ASABUS_UART_init();

    // Readback Configure in EEPROM, default start Address: 0
    EEPROM_get(0, sizeof(ASAConfigStr_inst), &ASAConfigStr_inst);

    TWIMNewMode_net(&TWI_NewMode_st);
    SPIMNewMode_net(&SPI_NewMode_st);
    UARTMNewMode_net(&UART_NewMode_st);

    // External function 7S00
    SeiralPortMNewMode_reg(&UART_NewMode_st, ASA_7s00_trm, ASA_7s00_rec,
                           ASA_7S00_ftm, ASA_7S00_frc, &ASA_7S00_str);
    // External function KB00
    SeiralPortMNewMode_reg(&SPI_NewMode_st, ASA_KB00_trm, ASA_KB00_rec,
                           ASA_KB00_ftm, ASA_KB00_frc, &ASA_KB00_str);
    // External function STP00
    SeiralPortMNewMode_reg(&SPI_NewMode_st, ASA_STP00_trm, ASA_STP00_rec,
                           ASA_STP00_ftm, ASA_STP00_frc, &ASA_KB00_str);
}
