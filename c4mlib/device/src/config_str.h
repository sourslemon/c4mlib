/**
 * @file config_str.h
 * @author Ye Chen-Wei
 * @date 2019.01.28
 * @brief Provide ASA non-volatile configuration structure
 */
#ifndef C4MLIB_DEVICE_CONFIG_STR_H
#define C4MLIB_DEVICE_CONFIG_STR_H

#include <stdint.h>

typedef struct {
    uint8_t ASA_ID;
} AsaConfig_t;

#endif  // C4MLIB_DEVICE_CONFIG_STR_H
