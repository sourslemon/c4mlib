/**
 * @file test_intfreqdiv.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請測試者使用示波器檢查PA0~PA2打出三相方波。
 */
#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL
#include "c4mlib/device/src/device.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#include <stddef.h>
#include <stdint.h>

#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"
#include "c4mlib/debug/src/debug.h"

/**
 * @brief : When timer interrrupt, it will generate the three phase 1000Hz square wave.
 * IO 0 : PA0
 * IO 1 : PA1
 * IO 2 : PA2
 */

IntFreqDivStr_t IntFreqDivStr_1;
TimIntStr_t TimInt3 = TIMINT_3_STR_INI;

/* User define function, when timer counters equal phase, it will call. */
void USER_FUNCTION(void* pvData_p);

/* Setting TIMER hardware function start. */
void init_timer();
/* Setting TIMER hardware function end. */

int main() {

    C4M_DEVICE_set();

    /***** Test the  IntFreqDiv component *****/
    printf("======= Test IntFreqDiv component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    printf("Start timer3 with CTC 1000Hz\n");
    init_timer();
    printf("enable Global Interrupt\n");

    uint8_t param_0  = 0;
    uint8_t param_40 = 4;
    uint8_t param_80 = 8;

    //             ISRStr_p          ISRFunc_p      ISRFuncStr_p cycle phase
    uint8_t id0 = IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_0,  12, 0);
    printf("Added 12 cycle,0 phase to IntFreqDiv 1 Component\n");
    uint8_t id1 = IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_40, 12, 4);
    printf("Added 12 cycle,20 phase to IntFreqDiv 1 Component\n");
    uint8_t id2 = IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_80, 12, 8);
    printf("Added 12 cycle,40 phase to IntFreqDiv 1 Component\n");

    IntFreqDiv_en(&IntFreqDivStr_1, id0, true);
    IntFreqDiv_en(&IntFreqDivStr_1, id1, true);
    IntFreqDiv_en(&IntFreqDivStr_1, id2, true);

    printf("Initize the TimInt component with TimInt3\n");

    TimInt_net(&TimInt3, 3);
    printf("Added IntFreqDiv 1 to TimInt3\n");
    uint8_t id =
        TimInt_reg(&TimInt3, (Func_t)IntFreqDiv_step, &IntFreqDivStr_1);
    TimInt_en(&TimInt3, id, true);
    sei();
    DEBUG_INFO("Timer 3 start, check IO work successfully\n");
    while (1) {
        ;  // Block here
    }
}

/**
 * @brief: Hardware timer setting, initialize the TIMER3 with CTC mode, interrupt at 24000 Hz
 * Prescaler: 8
 * Square wave period: 1 ms (1000 Hz)
 * mode: CTC
 */
void init_timer() {
    OCR3A = 57;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00001010;
    TCNT3 = 0;
    ETIMSK |= 1 << OCIE3A;
}

/**
 * @brief: The function user defined, run this function when timer counters equal phase.
 * Interrupt period: 0.5 ms (2000 = 24000/12 Hz)
 * Rigister 12 cycle
 */
void USER_FUNCTION(void* pvData_p) {
    uint8_t* p_param = (uint8_t*)pvData_p;

    if (p_param != NULL) {
        switch (*p_param) {
            case 0:
                PORTA ^= 1;
                break;
            case 4:
                PORTA ^= 2;
                break;
            case 8:
                PORTA ^= 4;
                break;
        }
    }
}
