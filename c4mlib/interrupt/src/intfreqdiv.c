/**
 * @file intfreqdiv.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief
 * 提供ASA函式庫中斷除頻功能，將提供標準step()函式呼叫，根據已設定之cycle與phase，提供條件式觸發呼叫功能。
 */
#include "intfreqdiv.h"
#include <stddef.h>
#include "c4mlib/debug/src/debug.h"
/* Open below comment, it will show the debug printf */
// #define USE_C4MLIB_DEBUG

// TODO
// void IntFreqDiv_init(IntFreqDivStr_t* IntFreqDivStr_p) {
//     // TODO: Add macro initialize code
// }

// Control timer frequency divide functional block
void IntFreqDiv_en(IntFreqDivStr_t* IntFreqDivStr_p, uint8_t Fb_Id, uint8_t enable) {
    // TODO: Check the parameter is valid
    // Makesure the is not nullptr
    if (IntFreqDivStr_p != NULL) {
        IntFreqDivStr_p->fb[Fb_Id].enable = enable;
    }
}

void IntFreqDiv_step(IntFreqDivStr_t* IntFreqDivStr_p) {
    // 依序將已註冊的所以中斷除頻工作進行Counter更新，並完成觸發呼叫
    for (int i = 0; i < IntFreqDivStr_p->total; i++) {
        // Only check enable item
        if (!IntFreqDivStr_p->fb[i].enable) {
            // If there is disable, skip
            continue;
        }

        // 判斷是否為One-shot mode
        if (IntFreqDivStr_p->fb[i].cycle == 0) {
            // 判斷是否倒數完畢
            if (IntFreqDivStr_p->fb[i].counter == 0) {
                // 不自動 Reload動作，單次觸發對應工作
                // TODO: 增加抽象化呼叫方式，相容Lutos排程呼叫。
                IntFreqDivStr_p->fb[i].func_p(IntFreqDivStr_p->fb[i].funcPara_p);

                // 禁能該中斷工作，以達成單次觸發功能。如要再觸發需重新致能
                IntFreqDiv_en(IntFreqDivStr_p, i, 0);
                // 重置計數器
                IntFreqDivStr_p->fb[i].counter = IntFreqDivStr_p->fb[i].phase;
            }

        }
        // Auto-reload mode
        else {
            // 判斷是否倒數完畢
            if (IntFreqDivStr_p->fb[i].counter == 0) {
                // 執行Reload動作
                IntFreqDivStr_p->fb[i].counter = IntFreqDivStr_p->fb[i].cycle;
            }
            // 判斷是否符合相位，phase的基準點為 (counter%cycle)==0的時刻
            if (IntFreqDivStr_p->fb[i].cycle - IntFreqDivStr_p->fb[i].counter ==
                IntFreqDivStr_p->fb[i].phase) {
                // 執行對應工作
                IntFreqDivStr_p->fb[i].func_p(IntFreqDivStr_p->fb[i].funcPara_p);
            }
        }
        IntFreqDivStr_p->fb[i].counter--;
    }
}

// Register the IntFreqDiv item into specific IntFreqDiv Manager
uint8_t IntFreqDiv_reg(IntFreqDivStr_t* IntFreqDivStr_p, Func_t FbFunc_p,
                       void* FbPara_p, uint16_t cycle, uint16_t phase) {
    uint8_t new_IFDStrId = IntFreqDivStr_p->total;
    IntFreqDivStr_p->fb[new_IFDStrId].enable = 0;  // Default Disable Timeout ISR
    // 判斷是否為One-shot模式
    if (cycle == 0) {
        IntFreqDivStr_p->fb[new_IFDStrId].counter = phase;
    } else {
        IntFreqDivStr_p->fb[new_IFDStrId].counter = cycle;
    }

    IntFreqDivStr_p->fb[new_IFDStrId].cycle = cycle;
    IntFreqDivStr_p->fb[new_IFDStrId].phase = phase;
    IntFreqDivStr_p->fb[new_IFDStrId].func_p = FbFunc_p;
    IntFreqDivStr_p->fb[new_IFDStrId].funcPara_p = FbPara_p;
    IntFreqDivStr_p->total++;

    return new_IFDStrId;
}
