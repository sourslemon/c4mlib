/**
 * @file isr_func.h
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @priority 0
 * @brief 提供標準的 ISR FUNCTION 型態。
 */
#ifndef C4MLIB_INTERRUPT_ISR_FUNC_H
#define C4MLIB_INTERRUPT_ISR_FUNC_H

#include "c4mlib/macro/src/std_type.h"

/* Public Section Start */
typedef struct {
    volatile uint8_t enable;     // 禁致能控制決定本中斷中執行函式是否致能
    Func_t func_p;               // 中斷中執行函式
    void* funcPara_p;            // 中斷中執行函式之傳參
} FuncBlockStr_t;
/* Public Section End */

#endif  // C4MLIB_INTERRUPT_ISR_FUNC_H
