/**
 * @file intfreqdiv.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫中斷除頻功能，將提供標準step()函式呼叫，根據已設定之cycle與phase，提供條件式觸發呼叫功能。
 */
#ifndef C4MLIB_COMMON_ASA_INTFREQDIV_H
#define C4MLIB_COMMON_ASA_INTFREQDIV_H

#include "c4mlib/macro/src/std_type.h"
#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct {
    volatile uint16_t cycle;    // 計數觸發週期
    volatile uint16_t phase;    // 計數觸發相位
    volatile uint16_t counter;  // 計數器 計數器固定頻率上數
    volatile uint8_t enable;    // 禁致能控制 決定本逾時ISR是否致能
    volatile Func_t func_p;     // 逾時中斷函式 逾時中斷函式指標，為無回傳 void*傳參函式。
    void* funcPara_p;           // 中斷中執行函式專用結構化資料住址指標
} IntFreqDivISR_t;

typedef struct {
    uint8_t total;                                   // 紀錄已註冊中斷除頻工作
    volatile IntFreqDivISR_t fb[MAX_IFD_FUNCNUM];    // 紀錄所有已註冊的計時中斷
} IntFreqDivStr_t;

// TODO: 可用性分析?，由於都是靜態初始值，所以似乎沒有runtime中初始化的需求
// Initiailize the TypeOfIntFreqDiv
// void IntFreqDiv_init(IntFreqDivStr_t* IntFreqDivStr_p);

// Register the IntFreqDiv item into specific IntFreqDiv Manager
uint8_t IntFreqDiv_reg(IntFreqDivStr_t* IntFreqDivStr_p, Func_t FbFunc_p, void* FbPara_p, uint16_t cycle, uint16_t phase);

// Control timer frequency divide functional block
void IntFreqDiv_en(IntFreqDivStr_t* IntFreqDivStr_p, uint8_t Fb_Id, uint8_t enable);

// Compute the one step for specific IntFreqDiv Manger
void IntFreqDiv_step(IntFreqDivStr_t* IntFreqDivStr_p);
/* Public Section End */

#endif  // C4MLIB_COMMON_ASA_INTFREQDIV_H
