#ifndef C4MLIB_STDIO_STDIO_H
#define C4MLIB_STDIO_STDIO_H

#include <stdio.h>

/* Public Section Start */
void C4M_STDIO_init(void);
/* Public Section End */

#endif  // C4MLIB_STDIO_STDIO_H
