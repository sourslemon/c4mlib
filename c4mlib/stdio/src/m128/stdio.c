#include <avr/io.h>
#include <stdio.h>

#ifndef DEFAULTUARTBAUD
#    define DEFAULTUARTBAUD 38400
#endif

/* public functions declaration section ------------------------------------- */
void C4M_STDIO_init(void);

/* define section ----------------------------------------------------------- */

/* static functions declaration section ------------------------------------- */
static int stdio_putchar(char c, FILE *stream);
static int stdio_getchar(FILE *stream);

/* static variables section ------------------------------------------------- */
static FILE STDIO_BUFFER =
    FDEV_SETUP_STREAM(stdio_putchar, stdio_getchar, _FDEV_SETUP_RW);

/* static function contents section ----------------------------------------- */
static int stdio_putchar(char c, FILE *stream) {
    if (c == '\n')
        stdio_putchar('\r', stream);
    while (!(UCSR0A & (1 << UDRE0)))
        ;
    UDR0 = c;

    return 0;
}

static int stdio_getchar(FILE *stream) {
    int UDR_Buff;
    while (!(UCSR0A & (1 << RXC0)))
        ;
    UDR_Buff = UDR0;

    return UDR_Buff;
}

/* public function contents section ----------------------------------------- */
void C4M_STDIO_init(void) {
    unsigned int baud;

    baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;
    UBRR0H = (unsigned char)(baud >> 8);
    UBRR0L = (unsigned char)baud;

    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
    UCSR0C |= (3 << UCSZ00);

    stdout = &STDIO_BUFFER;
    stdin = &STDIO_BUFFER;
}
