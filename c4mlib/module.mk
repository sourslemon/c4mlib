##
# @file module.mk
# @author LiYu87
# @date 2019.09.23
# @brief 建置c4mlib的makefile模組資料夾 Makefile 主要引用的規則

# 自動搜尋 src test lib 底下 source
VPATH = src/ test/ lib/

# Include Path
# 預設為專案根目錄
IPATH = ../../

# targets
SRCS    = $(wildcard src/*.c)
TARGETS = $(patsubst %.c,%.o,$(notdir $(SRCS)))

# test hexs
TESTSRCS = $(wildcard test/*.c)
TESTHEXES = $(patsubst %.c,%.hex,$(notdir $(TESTSRCS)))

# 生成 hex 所需 objects ，包含此模組及相依模組之 objects
LIBOBJS = $(DEP_OBJS) $(TARGETS)

## 模組名稱 MODULE_NAME
MODULE_NAME = $(notdir ${CURDIR})

## 相依模組 DEP_MODULES
MODULE_IGNORED = ../ config
MODULES = $(filter-out $(MODULE_IGNORED), $(patsubst ../%/, %, $(sort $(dir $(wildcard ../*/)))))
DEP_MODULES = $(filter-out $(MODULE_NAME), $(MODULES))

# serch for dependent objs from other modules
FIND_DEP_OBJS = $(wildcard ../$(DIR)/*.o)
DEP_OBJS = $(foreach DIR,$(DEP_MODULES),$(FIND_DEP_OBJS))

## 預設指令
# 編譯目標檔案
all: $(TARGETS)

# 編譯測試用hex檔案
test: $(TESTHEXES)
	@avr-size $(TESTHEXES)

# 編譯相依模組的 objects
deps:
	@$(foreach DIR,$(DEP_MODULES),echo '===== Building obj file for $(DIR) =====' && make -s -C ../$(DIR) &&) echo 'all modules done!'

# 印出所有目標檔案
print_targets:
	@echo $(TARGETS)

# 印出所有測試用hex檔案
print_testhexes:
	@echo testhexes: $(TESTHEXES)

# 清除此模組內物件
clean:
	-rm -rf *.o *.elf *.hex *.map *.lss

# 清除此相依模組內物件
clean_deps:
	@$(foreach DIR,$(DEP_MODULES),make clean -s -C ../$(DIR) &&) echo 'clean is done!'

.PHONY: all test deps clean clean_deps

include ../gcc.mk
