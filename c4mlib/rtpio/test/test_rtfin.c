/**
 * @file test_rtfin.c
 * @author Yi-Mou
 * @brief rtfio實作
 * @date 2019-09-13
 *
 */
#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"

RealTimeFlagStr_t RealTimeFlagIn_1;

void main(){

    C4M_DEVICE_set();

    RealTimeFlag_net(&RealTimeFlagIn_1,&PINA,0xff,0);
    uint8_t data;
    while(1){
        RealTimeFlagIn_step(&RealTimeFlagIn_1);
        if(RealTimeFlagIn_1.TrigCount==1){
            data=RealTimeFlagIn_1.FlagsValue;
            RealTimeFlagIn_1.TrigCount=0;
        }
        printf("data=%d\n",data);
    }
}
