/**
 * @file test_rtupcount.c
 * @author Yi-Mou
 * @brief realtimeupcount實作
 * @date 2019-08-26
 */
#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtupcount.h"
#include "c4mlib/hardware2/src/tim.h"


RealTimeCountStr_t RealTimeCount_1={0};
TimIntStr_t TimInt2 = TIMINT_2_STR_INI;

void init_timer2(); //timer2 CTC 100Hz

void main(){

    C4M_DEVICE_set();
    TimInt_net(&TimInt2, 2);

    RealTimeCount_1.Fb_Id = TimInt_reg(&TimInt2,RealTimeUpCount_step, &RealTimeCount_1);
    TimInt_en(&TimInt2,RealTimeCount_1.Fb_Id, true);
    
    init_timer2();
    sei();

    uint8_t times=0;
    DDRA=0xFF;
    while(1){
        if(RealTimeCount_1.TrigCount==200){
            times++;
            RealTimeCount_1.TrigCount=0;
        }
        printf("%d\n",times);
    }
}

void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}
