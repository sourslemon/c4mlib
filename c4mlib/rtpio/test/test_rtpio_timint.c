/**
 * @file test_rtpio_timint.c
 * @author Yi-Mou  
 * @brief rtpio在timint模組上實作
 * @date 2019-08-19
 */
#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"
#include "c4mlib/hardware2/src/tim.h"

RealTimePortStr_t RealTimePortOut_1;
TimIntStr_t TimInt2 = TIMINT_2_STR_INI;

void init_timer2(); //timer2 CTC 100Hz

void main(){
    
    C4M_DEVICE_set();

    RealTimePort_net(&RealTimePortOut_1,&PORTA,1);
    TimInt_net(&TimInt2, 2);
    RealTimePortOut_1.Fb_Id = TimInt_reg(&TimInt2,RealTimePortOut_step, &RealTimePortOut_1);
    TimInt_en(&TimInt2, RealTimePortOut_1.Fb_Id, true);
    init_timer2();
    sei();
    
    uint8_t data=0;
    DDRA=0xFF;
    while(1){
        if(RealTimePortOut_1.TrigCount==1){
            RealTimePortOut_1.Buff[0]=data;
            RealTimePortOut_1.TrigCount=0;
            data=~data;
        }
    }
}

void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}
