/**
 * @file test_rtpio_extint.c
 * @author Yi_Mou
 * @brief rtpio在extint模組上實作
 * @date 2019-08-22
 * @copyright Copyright (c) 2019
 */
#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"
#include "c4mlib/hardware2/src/extint.h"

RealTimePortStr_t RealTimePortOut_1;
RealTimePortStr_t RealTimePortIn_1;
ExtIntStr_t ExtInt4 = EXTINT_4_STR_INI;

void main(){

    C4M_DEVICE_set();
    
    RealTimePort_net(&RealTimePortIn_1,&PINA,1);
    ExtInt_net(&ExtInt4,4);
    
    RealTimePortIn_1.Fb_Id = ExtInt_reg(&ExtInt4,RealTimePortIn_step, &RealTimePortIn_1);
    ExtInt_en(&ExtInt4, RealTimePortIn_1.Fb_Id, true);
    
    DDRE = 0xf0;    
    EICRB |= 0xff;  // Setup INT using rising edge
    EIMSK = 0xf0;   // Enable INT
    sei();
   
    uint8_t data=0;
    DDRA=0xff;
    while(1){
        if(RealTimePortIn_1.TrigCount>=1){
            data=RealTimePortIn_1.Buff[0];
            RealTimePortIn_1.TrigCount=0;
        }
        printf("data=%d\n",data);
   }
}
