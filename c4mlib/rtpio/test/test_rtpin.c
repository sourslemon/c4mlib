/**
 * @file test_rtpin.c
 * @author Yi-Mou
 * @brief rtpio實作
 * @date 2019-08-19
 *
 */
#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"

RealTimePortStr_t RealTimePortIn_1;

void main(){

    C4M_DEVICE_set();

    RealTimePort_net(&RealTimePortIn_1,&PINA,1);
    uint8_t data;
    while(1){
        RealTimePortIn_step(&RealTimePortIn_1);
        if(RealTimePortIn_1.TrigCount==1){
            data=RealTimePortIn_1.Buff[0];
             RealTimePortIn_1.TrigCount--;
        }
        printf("data=%d\n",data);
    }
}
