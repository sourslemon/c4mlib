/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount實現
 * @date 2019-08-26
 */

#include "rtupcount.h"
#include <stddef.h>


void RealTimeUpCount_step(RealTimeCountStr_t* RealTimeCount_p){
    RealTimeCount_p->TrigCount++;
}
