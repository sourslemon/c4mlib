/**
 * @file rtpio.c
 * @author Yi-Mou
 * @date 2019.08.16
 * @brief RealTimePort實現
 */

#include <stddef.h>
#include "c4mlib/macro/src/bits_op.h"
#include "rtpio.h"

void RealTimePort_net(RealTimePortStr_t* Str_p, volatile uint8_t* Reg_p, uint8_t Bytes){    
    Str_p->Reg_p=Reg_p;
    Str_p->Bytes=Bytes;
    Str_p->TrigCount=0;   
}

void RealTimePortIn_step(RealTimePortStr_t* RealTimePortStr_p){
    for(int i=0;i<RealTimePortStr_p->Bytes;i++){
        RealTimePortStr_p->Buff[i]=*(RealTimePortStr_p->Reg_p+i);    //先低
    }
    RealTimePortStr_p->TrigCount++;
}

void RealTimePortOut_step(RealTimePortStr_t* RealTimePortStr_p){
    for(int i=0;i<RealTimePortStr_p->Bytes;i++){
        *(RealTimePortStr_p->Reg_p+i)=RealTimePortStr_p->Buff[i];
    }
    RealTimePortStr_p->TrigCount++;
}

void RealTimeFlag_net(RealTimeFlagStr_t* Str_p, volatile uint8_t* Reg_p, uint8_t Mask,uint8_t Shift){    
    Str_p->Reg_p=Reg_p;
    Str_p->Mask=Mask;
    Str_p->Shift=Shift;
    Str_p->TrigCount=0;   
}

void RealTimeFlagIn_step(RealTimeFlagStr_t* RealTimeFlagStr_p){
    REGFGT(*(RealTimeFlagStr_p->Reg_p),RealTimeFlagStr_p->Mask,RealTimeFlagStr_p->Shift,&RealTimeFlagStr_p->FlagsValue);
    RealTimeFlagStr_p->TrigCount++;
}

void RealTimeFlagOut_step(RealTimeFlagStr_t* RealTimeFlagStr_p){
    REGFPT(*(RealTimeFlagStr_p->Reg_p),RealTimeFlagStr_p->Mask,RealTimeFlagStr_p->Shift,RealTimeFlagStr_p->FlagsValue);
    RealTimeFlagStr_p->TrigCount++;
}
