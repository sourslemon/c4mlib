/**
 * @file rtpio.h
 * @author Yi-Mou
 * @brief RealTimePort函式定義
 * @date 2019-08-16
 * 
 */
#ifndef C4MLIB_RTPIO_RTPIO_H
#define C4MLIB_RTPIO_RTPIO_H

#include <stdint.h>

#define MAX_DATA_BYTES 2
typedef struct {
    volatile uint8_t* Reg_p;
    uint8_t Bytes;
    uint8_t Fb_Id;
    volatile uint8_t Buff[MAX_DATA_BYTES];
    volatile uint8_t TrigCount;
}RealTimePortStr_t;

typedef struct {
    volatile uint8_t* Reg_p;
    uint8_t Fb_Id;
    uint8_t Mask;
    uint8_t Shift;
    volatile uint8_t FlagsValue;
    volatile uint8_t TrigCount;
}RealTimeFlagStr_t;


void RealTimePort_net(RealTimePortStr_t* Str_p, volatile uint8_t* Reg_p, uint8_t Bytes);
void RealTimePortIn_step(RealTimePortStr_t* RealTimePortStr_p);
void RealTimePortOut_step(RealTimePortStr_t* RealTimePortStr_p);

void RealTimeFlag_net(RealTimeFlagStr_t* Str_p, volatile uint8_t* Reg_p, uint8_t Mask,uint8_t Shift);
void RealTimeFlagIn_step(RealTimeFlagStr_t* RealTimeFlagStr_p);
void RealTimeFlagOut_step(RealTimeFlagStr_t* RealTimeFlagStr_p);

#endif  // C4MLIB_RTPIO_RTPIO_H
  