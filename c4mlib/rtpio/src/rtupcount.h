/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount定義
 * @date 2019-08-26
 */

#ifndef C4MLIB_RTPIO_RTUPCOUNT_H
#define C4MLIB_RTPIO_RTUPCOUNT_H

#include <stdint.h>
typedef struct {
    uint8_t Fb_Id;
    volatile uint8_t TrigCount;
}RealTimeCountStr_t;
   
void RealTimeUpCount_step(RealTimeCountStr_t* RealTimeCountStr_p);

#endif //C4MLIB_RTPIO_RTUPCOUNT_H
