#define F_CPU 11059200UL

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

#include <util/delay.h>

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    C4M_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    /*--- 測試傳送字串 ---*/
    uint8_t data1_buffer[5] = {9, 8, 7, 6, 5};
    uint8_t data2_buffer[10] = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};

    uint8_t result = 0;

    /***** UARTM Mode4 Transmit test *****/

    printf("【test 1】Master傳送 5 筆資料 (由高到低) \n");
    result = UARTM_trm(4, 0, 0, 5, data1_buffer);
    if (result)
        printf("【test 1】UARTM_trm Fail [%d]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 1】UARTM_trm : data[%u]=%u\n", i, data1_buffer[i]);
    }
    HAL_delay(100);

    /* -------- UARTM Mode4 Receive test -------- */
    // ! 注意 rec 必須接續 trm 後面使用
    printf("【test 2】Master接收 10 筆資料 (由高到低) \n");
    result = UARTM_rec(4, 0, 0, 10, data2_buffer);
    if (result) {
        printf("【test 2】UARTM_rec Fail [%d]\n", result);
    }
    for (int i = 0; i < 10; i++) {
        printf("【test 2】UARTM_rec : data[%u]=%u\n", i, data2_buffer[i]);
    }
    HAL_delay(100);
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
