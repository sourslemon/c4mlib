/**
 * @file asauart_master.c
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Implement ASA UART Master ModeN function
 *
 * @author Wu Cheng-Han
 * @date 2019.6.25
 */

#include "asauart_master.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/exfunc.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/io.h>
#include <stdint.h>

char UARTM_trm(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p) {
    if (RegAdd > 0x80)
        return HAL_ERROR_ADDR;
    if (Bytes == 0)
        return HAL_ERROR_BYTES;
    uint8_t trans_temp = 0;
    uint8_t reciv_temp = 0;
    uint8_t checksum = 0;
    if (Mode >= 100) {
        checksum = ExFunc_trm(EXFUNC_UART, Mode, UartID, RegAdd, Bytes,
                              (uint8_t *)Data_p);
        return checksum;
    }
    switch (Mode) {
        case 0:
            // Mode 0
            // Byte0    Byte1   Byte2   Byte3   Byte4
            // header   UartID  RADD    DATA    REC(header)
            // header S=Header  ChkS=0

            checksum = 0;  // Reset checksum variable for later used.

            trans_temp = CMD_HEADER;
            UARTM_Inst.write_byte(trans_temp);

            // UartID = UartID, Chks=Chks+S
            trans_temp = UartID;
            UARTM_Inst.write_byte(trans_temp);
            checksum += trans_temp;

            // RegAdd = R+Add, Chks=Chks+S
            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);
            checksum += trans_temp;

            // FOR N=0:Bytes-1
            //  DataN = DataN, Chks=Chks+S
            // ENDFOR
            UARTM_Inst.write_multi_bytes(Data_p, Bytes);
            for (int i = 0; i < Bytes; i++) {
                checksum += ((uint8_t *)Data_p)[i];
            }

            // CheckSum = CheckSum
            trans_temp = checksum;
            UARTM_Inst.write_byte(trans_temp);

            // printf("Wait for response\n");
            UARTM_Inst.read_byte(&reciv_temp);
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            else if (reciv_temp != RSP_HEADER)
                return HAL_ERROR_HEADER;
            else
                break;

        case 3:
            UARTM_Inst.write_multi_bytes(Data_p, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error
            break;

        case 4:
            for (int i = 0; i < Bytes; i++) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[Bytes - 1 - i]);
            }
            break;
        case 7:
            // Mode 7
            // Byte0    Byte1
            // RADD     DATA

            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            UARTM_Inst.write_multi_bytes(Data_p, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            else
                break;

        case 8:
            // Mode 6
            // Byte0    Byte1
            // RADD     DATA

            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[i]);
            }

            if (*(UARTM_Inst.error_code)) {
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            }
            else {
                break;
            }

        case 9:
            // Mode 9
            // Byte0    Byte1
            // RADD     DATA

            trans_temp = (RegAdd << 1);
            UARTM_Inst.write_byte(trans_temp);

            UARTM_Inst.write_multi_bytes(Data_p, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            else
                break;

        case 10:
            // Mode 10
            // Byte0    Byte1
            // RADD     DATA

            trans_temp = (RegAdd << 1);
            UARTM_Inst.write_byte(trans_temp);

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[i]);
            }

            if (*(UARTM_Inst.error_code)) {
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            }
            else {
                break;
            }
        case 1:
            // Mode1 先低後高
            // Data+CF (CF向右對齊 Data向左對齊)
            trans_temp = RegAdd | ((uint8_t *)Data_p)[0];
            UARTM_Inst.write_byte(trans_temp);
            HAL_delay(10UL);  // ? 去掉這個會發生錯，why?
            for (int i = 1; i < Bytes; i++) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[i]);
            }

            break;
        case 2:
            // Mode2 先高後低
            // CF+Data (CF向左對齊 Data向右對齊)
            ////printf("trm mode2 call\n");
            trans_temp = RegAdd | ((uint8_t *)Data_p)[Bytes - 1];
            UARTM_Inst.write_byte(trans_temp);
            // HAL_delay(10UL);
            for (int i = Bytes - 2; i >= 0; i--) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[i]);
            }
            break;
        case 5:
            // Mode 5
            // Byte0     Byte1
            // ADDR      DATA
            // DATA由低至高
            // regADDR 2 3 write only
            // regADDR 4 5 read only
            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            UARTM_Inst.write_multi_bytes(Data_p, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            else
                break;
        case 6:
            // Mode 6
            // Byte0     Byte1
            // ADDR      DATA
            // DATA由高至低
            // regADDR 2 3 write only
            // regADDR 4 5 read only
            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.write_byte(((uint8_t *)Data_p)[i]);
            }

            if (*(UARTM_Inst.error_code)) {
                return HAL_ERROR_TIMEOUT;  // Timeout Error
            }
            else {
                break;
            }

        default:
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    return HAL_OK;
}

char UARTM_rec(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p) {
    if (RegAdd > 0x80)
        return HAL_ERROR_ADDR;
    if (Bytes == 0)
        return HAL_ERROR_BYTES;
    uint8_t trans_temp = 0;
    uint8_t reciv_temp = 0;
    uint8_t checksum = 0;
    uint8_t uarts_data_temp[16] = {0};
    if (Mode >= 100) {
        checksum = ExFunc_rec(EXFUNC_UART, Mode, UartID, RegAdd, Bytes,
                           (uint8_t *)Data_p);
        return checksum;
    }
    switch (Mode) {
        case 0:

            checksum = 0;

            trans_temp = CMD_HEADER;
            UARTM_Inst.write_byte(trans_temp);

            trans_temp = UartID;
            UARTM_Inst.write_byte(trans_temp);
            checksum += trans_temp;

            trans_temp = RegAdd | 0x80;  // R/W bit + Register address
            UARTM_Inst.write_byte(trans_temp);
            checksum += trans_temp;

            trans_temp = checksum;
            UARTM_Inst.write_byte(trans_temp);

            // Wait for slave data

            /**** Read Header ****/
            // TODO: Add timeout parameter for this HAL function
            UARTM_Inst.read_byte(&reciv_temp);
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error
            else if (reciv_temp !=
                     RSP_HEADER)  // TODO: Makesure there Master Header and
                                  // Slave header is same
                return HAL_ERROR_HEADER;  // Header not match

            /**** Read Data section ****/
            checksum = 0;
            UARTM_Inst.read_multi_bytes(uarts_data_temp, Bytes);
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                checksum += uarts_data_temp[i];
            }

            /**** Read checksum section ****/
            UARTM_Inst.read_byte(&reciv_temp);
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error
            // printf("Compute checksum: %02X , Receive checksum: %02X \n",
            // checksum, reciv_temp);
            // NOTE: 這邊有神奇魔術，不加上(volatile
            // uint8_t)強制轉型不快取最佳化才正常，小心!
            if ((volatile uint8_t)checksum != (volatile uint8_t)reciv_temp)
                return HAL_ERROR_CHK_SUM;  // Checksum not match
            // If there is no error, move temporary buffer to actual Register
            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;
        case 3:
            /**** Read Data section ****/
            checksum = 0;
            UARTM_Inst.read_multi_bytes(Data_p, Bytes);
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            break;
        case 4:
            /**** Read Data section ****/
            checksum = 0;
            for (int i = 0; i < Bytes; i++) {
                UARTM_Inst.read_byte((uint8_t *)Data_p + Bytes - 1 - i);
            }

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            break;
        case 7:

            trans_temp = RegAdd | 0x80;  // R/W bit + Register address
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            UARTM_Inst.read_multi_bytes(uarts_data_temp, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;

        case 8:

            trans_temp = RegAdd | 0x80;  // R/W bit + Register address
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.read_byte(uarts_data_temp + i);
            }

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;

        case 9:

            trans_temp = (RegAdd << 1) | 0x01;  //  Register address + R/W bit
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            UARTM_Inst.read_multi_bytes(uarts_data_temp, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }
            break;

        case 10:

            trans_temp = (RegAdd << 1) | 0x01;  //  Register address + R/W bit
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.read_byte(uarts_data_temp + i);
            }

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;
        case 1:
            /**** Read Data section ****/
            for (int i = 0; i < Bytes; i++) {
                UARTM_Inst.read_byte((uint8_t *)Data_p + i);
            }
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error
            break;
        case 2:
            /**** Read Data section ****/
            for (int i = 0; i < Bytes; i++) {
                UARTM_Inst.read_byte((uint8_t *)Data_p + Bytes - 1 - i);
            }
            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            break;
        case 5:
            // Mode 5
            // Byte0     Byte1
            // ADDR      DATA
            // DATA由低至高
            // regADDR 2 3 write only
            // regADDR 4 5 read only
            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            UARTM_Inst.read_multi_bytes(uarts_data_temp, Bytes);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;
        case 6:
            // Mode 6
            // Byte0     Byte1
            // ADDR      DATA
            // DATA由高至低
            // regADDR 2 3 write only
            // regADDR 4 5 read only
            trans_temp = RegAdd;
            UARTM_Inst.write_byte(trans_temp);

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = Bytes - 1; i >= 0; i--) {
                UARTM_Inst.read_byte(uarts_data_temp + i);
            }

            if (*(UARTM_Inst.error_code))
                return HAL_ERROR_TIMEOUT;  // Timeout error

            for (int i = 0; i < Bytes; i++) {
                ((uint8_t *)Data_p)[i] = uarts_data_temp[i];
            }

            break;
        default:
            return HAL_ERROR_MODE_SELECT;
            break;
    }

    return HAL_OK;
}

char UARTM_ftm(char Mode, char UartID, char RegAdd, char Mask, char shift,
               char *Data) {
    char result = 0;
    char Reg = 0;
    if (Mode >= 100) {
        result = ExFunc_ftm(EXFUNC_UART, Mode, UartID, RegAdd, Mask, shift,
                         (uint8_t *)Data);
        return result;
    }
    
    if ((Mode > 10 && Mode <100 ) || Mode == 1 || Mode == 2 || Mode == 4 || Mode == 6 ||
        Mode == 8 ||  Mode == 10 )
        return HAL_ERROR_MODE_SELECT;
    if (RegAdd > 0x80)
        return HAL_ERROR_ADDR;

    result = UARTM_rec(Mode, UartID, RegAdd, 1, &Reg);
    if (result)
        return result;

    Reg = (Reg & (~Mask)) | ((*Data << shift) & Mask);
    result = UARTM_trm(Mode, UartID, RegAdd, 1, &Reg);

    return result;
}

char UARTM_frc(char Mode, char UartID, char RegAdd, char Mask, char shift,
               char *Data) {
    char result = 0;
    char Reg = 0;
    if (Mode >= 100) {
        result = ExFunc_frc(EXFUNC_UART, Mode, UartID, RegAdd, Mask, shift,
                         (uint8_t *)Data);
        return result;
    }
    if (Mode > 8 || Mode == 1 || Mode == 2 || Mode == 4 || Mode == 6 ||
        Mode == 8)
        return HAL_ERROR_MODE_SELECT;
    if (RegAdd > 0x80)
        return HAL_ERROR_ADDR;

    result = UARTM_rec(Mode, UartID, RegAdd, 1, &Reg);
    if (result)
        return result;

    Reg = (Reg & (~Mask)) | ((*Data << shift) & Mask);
    *Data = Reg;

    return result;
}
