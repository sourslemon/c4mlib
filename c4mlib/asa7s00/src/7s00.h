#ifndef C4MLIB_7S00_7S00_H
#define C4MLIB_7S00_7S00_H

#include <stdint.h>

#include "c4mlib/config/asamodule.cfg"

/* define section ----------------------------------------------------------- */
#define PAC_7S00_HEADER 0x7D
#define PAC_7S00_DEVICE 0x01
#define PAC_7S00_CMD 0x04
#define PAC_7S00_LENGTH 0x04

#define RES_ERROR_UNSUPPORTED_ASCII 0x07

#define PAC_7S00_MASK 0xFF

/* Public Section Start */

typedef struct {
    uint8_t FFFlags;  // 浮點數旗標，閃爍旗標 (float and flash flag)
    uint8_t ASCII_list[4];  //個位,十位，百位，千位數的ASCII碼。
    uint8_t _7S00_put_reg[4];
} Asa7s00Para_t;

char ASA_7S00_set(char ASA_ID, char LSByte, char Mask, char shift, char Data,
                  Asa7s00Para_t* Str_p);
char ASA_7S00_put(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p);
char ASA_7S00_get(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p);

char ASA_7s00_trm(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);
char ASA_7s00_rec(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);
char ASA_7S00_frc(char ASA_ID, char RegAdd, char Mask, char Shift, void *Data_p,
                  void *Str_p);
char ASA_7S00_ftm(char ASA_ID, char RegAdd, char Mask, char shift, void *Data_p,
                  void *Str_p);


/* Public Section End */

char UART_7S00_Transfer(char ASA_ID, Asa7s00Para_t* Str_p);

#endif 
