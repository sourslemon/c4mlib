#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/device/src/device.h"

#include <avr/io.h>

#include "c4mlib/config/asamodule.cfg"

extern Asa7s00Para_t ASA_7S00_str;

int main() {
    C4M_DEVICE_set();

    int ascii[4];

    for(int i = 0;i<4;i++)
    {
        ascii[i] = 48;
    }

    while (1) {
        ASA_7S00_put(2, 3, 1, &ascii[3], &ASA_7S00_str);
        ASA_7S00_put(2, 2, 1, &ascii[2], &ASA_7S00_str);
        ASA_7S00_put(2, 1, 1, &ascii[1], &ASA_7S00_str);
        ASA_7S00_put(2, 0, 1, &ascii[0], &ASA_7S00_str);
        ascii[3] = 45;
        ascii[2] = ascii[1];
        ascii[1] = ascii[0];
        ascii[0]++;
        // printf("%d\n",as);
        if (ascii[0] == 58) {
            ascii[0] = 48;
        }
        _delay_ms(100);
    }
}
