/**
 * @file test_7s00_2.c
 * @author LiYu87
 * @brief 測試7S00所有可顯示字元
 * @date 2019.08.06
 *
 */
#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/device/src/device.h"

#include <avr/io.h>

#include "c4mlib/config/asamodule.cfg"

extern Asa7s00Para_t ASA_7S00_str;

int main() {
    C4M_DEVICE_set();

    char test[14][4] = {
        {'1', '2', '3', '4'},
        {'5', '6', '7', '8'},
        {'9', '0', '-', '-'},
        {'A', 'B', 'C', 'D'},
        {'E', 'F', '-', '-'},
        {'a', 'b', 'c', 'd'},
        {'e', 'f', '-', '-'},
        {'G', 'H', 'I', 'J'},
        {'-', 'L', 'N', 'O'},
        {'P', 'Q', 'S', 'U'},
        {'/', 'Q', 'S', 'U'},
        {  0,   0,   0,   0},
        {  0,  '-',  0, '-'},
        {'-', '-', '-', '-'},
    };
    printf("start test\n");

    uint8_t res = 0;
    uint8_t i = 0;

    while (1) {
        i = (i + 1) % 14;
        res = ASA_7S00_put(2, 0, 4, test[i], &ASA_7S00_str);
        printf("test %2d : ", i);
        printf("%c ", test[i][0]);
        printf("%c ", test[i][1]);
        printf("%c ", test[i][2]);
        printf("%c ", test[i][3]);
        printf("res = %d", res);
        printf("-res1 = %d\n", res);
        _delay_ms(1000);
    }
}
