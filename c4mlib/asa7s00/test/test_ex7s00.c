/**
 * @file test_exfunc.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-08-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"

#include <avr/io.h>

#include "c4mlib/config/asamodule.cfg"

int main() {
    C4M_DEVICE_set();
    uint8_t data[4] = {0, '-', '1', '2'};
    uint8_t res = 0;
    res = UARTM_trm(100, 2, 0, 4, data);
    printf("res0 = %d\n", res);
}
