/**
 * @file test_master_remorw_mode2.c
 * @author Deng Xiang-Guan
 * @date 2019.08.12
 * @brief Test remote register by using SPI mode 2, please press slave reset first and then press master reset.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 2
#define ASAID 4

// If control flag bit is three
#define testCF 0x03 << 5
#define CF_reg 0x02
#define trm_data 87

uint8_t test_trm1[2] = {CF_reg, trm_data};

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start master mode 2\n");
    SPIM_Inst.init();
    sei();
    while (true) {
        char chk;

        /* Test write remote register SPI mode 2 */
        printf("==== remo one ====\n");
        chk = ASA_SPIM_trm(SPI_MODE, ASAID, testCF, sizeof(test_trm1),
                           &test_trm1[0]);
        printf("chk===%d\n", chk);

        test_trm1[1]++;

        _delay_ms(3000);
    }
}
