/**
 * @file test_slave_as_master.c
 * @author Deng Xiang-Guan
 * @date 2019.08.12
 * @brief Test ASA neutral board as master, its IO work fine or not.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/time/src/hal_time.h"

void init_timer(void);

uint32_t last_time = 0;
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

/**
 * PB0 : /SS
 * PB1 : SCK
 * PB2 : MOSI
 * PB3 : MISO
 */

int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start Master\n");
    DDRF |= 1 << PF4;
    PORTF |= (1 << PF4);

    while (true) {
        printf("enter DDR \n");
        int output;
        scanf("%d", &output);
        DDRB = output;
        printf("enter PORTB \n");
        scanf("%d", &output);
        PORTB = output;
        _delay_ms(5);
        char d = PINB & 0x0f;
        printf("PINB:%d\n", d);
        for (uint8_t i = 0; i < 4; i++) {
            printf("PB%d:%d, ", i, ((d & 0x0f) & (1 << i)) >> i);
        }
        printf("\n");
        PORTF ^= (1 << PF4);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
