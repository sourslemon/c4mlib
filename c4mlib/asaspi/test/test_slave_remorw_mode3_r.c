/**
 * @file test_slave_remorw_mode3_r.c
 * @author Deng Xiang-Guan
 * @date 2019.08.12
 * @brief Test remote register by using SPI mode 3, please press slave reset first and then press master reset.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

void init_timer(void);

uint8_t test_ans1[4] = {15, 16, 17, 18};

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}
int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start slave read, mode 3\n");

    // Initailize Serial SPI remote register type
    SerialIsr_t SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_net(&SPIIsrStr, SPIS3_isr);

    uint8_t reg_1[4] = {0, 0, 0, 0};

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 4);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    char er_flag = false;
    char ok_flag = 0;
    printf("Start test slave remote read write register!!!\n");
    ASASPISerialIsrStr->rw_mode = 0;

    while (true) {
        // Show the register data
        // Read master data
        _delay_ms(3000);
        for (uint8_t i = 0; i < sizeof(reg_1); i++) {
            printf("reg_1[%d]=%d, ", i, reg_1[i]);
            if (reg_1[i] == test_ans1[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_ans1[i] += 4;
        }

        printf("\n");
        printf("\n=================\n");
        if (er_flag) {
            printf("Test slave mode 3 remo-reg fail !!!\n");
            while (1)
                ;
        }
        else {
            printf("Succeed --> %d\n", ok_flag);
        }
        if (ok_flag >= 20) {
            printf(
                "Test master and slave mode 3 remo-reg read write succeed "
                "!!!\n");
            break;
        }
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
