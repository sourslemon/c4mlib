/**
 * @file test_master_mem_trm_mode5.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief Test flash memory recieve by using SPI.
 * @mode using 5, send memory address low bytes first and then high bytes.
 * @Packet command + memory address + data transmitted
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 5
#define ASAID 4

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 1, 2, 3, 4};

int main() {
    // Setup
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test SPIM_Mem_trm mode 5\n");
    while(true) {
        SPIM_Mem_trm(SPI_MODE, ASAID, test_command, 4, &mem_addr, sizeof(test_data), test_data);
        for(int i=0;i<sizeof(test_data);i++) {
            test_data[i] += sizeof(test_data);
        }
        _delay_ms(3000);
    }

}
