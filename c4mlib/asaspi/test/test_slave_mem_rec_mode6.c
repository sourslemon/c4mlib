/**
 * @file test_master_mem_rec_mode6.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief Test flash memory recieve by using slave SPI, this file will return test packet to master SPI.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define test_command 0x02

uint32_t mem_addr = 12341487;

typedef struct SPI_Mem_trm_test {
    volatile uint8_t index;
    volatile uint8_t command;
    volatile uint32_t reg_addr;
    volatile uint8_t data[5];
}test_spi_mem_t;

test_spi_mem_t mem_str = {0, 0, 0, {0, 1, 2, 3, 4}};

ISR(SPI_STC_vect) {
    uint8_t data = SPIS_Inst.read_byte();
    if(mem_str.index == 0) {
        mem_str.command = data;
    }
    else if(mem_str.index < 5) {
        SPIS_Inst.write_byte(mem_str.data[0]);
        if(mem_str.index == 1) {
            mem_str.reg_addr = data << (8*(4-mem_str.index));
        }
        else {
            mem_str.reg_addr += (uint32_t)data << (8*(4-mem_str.index));
        }
        if(mem_str.index == 4) {
            mem_str.data[0] += sizeof(mem_str.data);
        }
    }
    else if((mem_str.index < 10)) {
        SPIS_Inst.write_byte(mem_str.data[mem_str.index-4]);
        mem_str.data[mem_str.index-4] += sizeof(mem_str.data);
    }
    mem_str.index++;
    if(mem_str.index == 10) {
        mem_str.index = 0;
    }
}

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start test SPIS_Mem_rec mode 6\n");
    SPIS_Inst.init();
    sei();
    _delay_ms(3000);
    while(true) {
        cli();
        printf("cmd:%x, regAdd:%lu, data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", 
                mem_str.command, mem_str.reg_addr, mem_str.data[0], mem_str.data[1], mem_str.data[2], mem_str.data[3], mem_str.data[4]);
        sei();
        _delay_ms(3000);

    }

}
