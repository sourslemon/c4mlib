/**
 * @file test_master_mem_rec_mode6.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief Test flash memory recieve by using SPI.
 * @mode using 6, send memory address low bytes first and then high bytes.
 * @Packet command + memory address + data recieved
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 6
#define ASAID 4

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 0, 0, 0, 0};
uint8_t ans_data[5] = {0, 1, 2, 3, 4};

int main() {
    // Setup
    uint16_t success_cnt = 0;
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test SPIM_Mem_rec mode 6\n");
    while(true) {
        uint8_t cnt = 0;
        SPIM_Mem_rec(SPI_MODE, ASAID, test_command, 4, &mem_addr, sizeof(test_data), test_data);
        printf("data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", test_data[0], test_data[1], test_data[2], test_data[3], test_data[4]);
        for(uint8_t i=0;i<4;i++) {
            if(test_data[i] == ans_data[i]) {
                cnt++;
                ans_data[i] += sizeof(ans_data);
            }
        }
        if(cnt == 4) {
            success_cnt++;
        }
        if(success_cnt != 0) {
            printf("Success !!! count:%d\n", success_cnt);
        }
        _delay_ms(3000);
    }

}
