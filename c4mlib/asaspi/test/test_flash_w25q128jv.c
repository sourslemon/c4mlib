/**
 * @file test_flash_w25q128jv.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief Test flash memory read and write by using SPI.
 * @mode using 5, send memory address low bytes first and then high bytes.
 * @Packet command + memory address + data transmitted
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 5
#define ASAID 4

#define write_command 0x02
#define read_command 0x03
#define read_id_command 0x90

uint32_t mem_addr = 0;
uint8_t test_data[4] = {0, 1, 2, 3};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};

int main() {
    // Setup
    uint16_t success_cnt = 0;
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test SPIM_Mem_trm mode 5\n");
    ASABUS_ID_set(ASAID);
    while(true) {
        printf("=================\n");
        SPIM_Inst.enable_cs(ASAID);
        ASABUS_SPI_swap(0x06);
        SPIM_Inst.disable_cs(ASAID);

        SPIM_Mem_trm(SPI_MODE, ASAID, 0x20, 3, &mem_addr, 0, test_data);
        _delay_ms(200);

        SPIM_Inst.enable_cs(ASAID);
        ASABUS_SPI_swap(0x06);
        SPIM_Inst.disable_cs(ASAID);
        SPIM_Mem_trm(SPI_MODE, ASAID, write_command, 3, &mem_addr, sizeof(test_data), test_data);
        ASABUS_ID_set(ASAID);
        for(uint8_t i=0;i<sizeof(test_data);i++) {
            printf("=>%d\n", test_data[i]);
        }

        _delay_ms(20);

        SPIM_Mem_rec(SPI_MODE, ASAID, read_command, 3, &mem_addr, sizeof(test_rec), test_rec);
        ASABUS_ID_set(ASAID);
        for(uint8_t i=0;i<sizeof(test_rec);i++) {
            printf("->%d\n", test_rec[i]);
        }

        _delay_ms(20);

        SPIM_Mem_rec(SPI_MODE, ASAID, read_id_command, 3, &mem_addr, sizeof(test_id), test_id);
        ASABUS_ID_set(ASAID);
        for(uint8_t i=0;i<sizeof(test_id);i++) {
            printf("_>%d\n", test_id[i]);
        }
        ASABUS_ID_set(ASAID);

        uint8_t cnt = 0;
        for(uint8_t i=0;i<sizeof(test_data);i++) {
            if(test_data[i] == test_rec[i]) {
                cnt++;
            }
            test_data[i] += sizeof(test_data);
        }
        if(cnt == sizeof(test_data)) {
            success_cnt++;
            printf("Test success, count:%d\n", success_cnt);
        }
        else {
            printf("Test fail ...\n");
        }


        _delay_ms(3000);
    }

}
