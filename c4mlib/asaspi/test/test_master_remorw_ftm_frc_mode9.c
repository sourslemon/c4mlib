/**
 * @file test_master_remorw_ftm_frc_mode9.c
 * @author Deng Xiang-Guan
 * @date 2019.08.27
 * @brief Test remote register by using SPI mode 9, please press slave reset first and then press master reset.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#include <stdlib.h>

#define SPI_MODE 9
#define ASAID 4

void init_timer(void);

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_trm1[1] = {(0x0f << 4)};
uint8_t test_trm2[1] = {(0x0f)};

int main() {
    // Setup
    char chk;
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start master mode 9\n");
    SPIM_Inst.init();
    sei();
    chk = ASA_SPIM_trm(SPI_MODE, ASAID, 2, 1, test_trm1);
    printf("trm1 chk===%d\n", chk);
    chk = ASA_SPIM_trm(SPI_MODE, ASAID, 3, 1, test_trm2);
    printf("trm2 chk===%d\n", chk);
    while (true) {
        /* Test write remote register SPI mode 0 */
        char *temp_data = (char *)malloc(sizeof(temp_data));
        printf("==== remo one ====\n");
        temp_data[0] = 0x0f;
        chk = ASA_SPIM_ftm(SPI_MODE, ASAID, 2, 0x0f, 0, temp_data);
        printf("chk===%d\n", chk);
        printf("==== remo two ====\n");
        temp_data[0] = 0xf0;
        chk = ASA_SPIM_ftm(SPI_MODE, ASAID, 3, 0xf0, 0, temp_data);
        printf("chk===%d\n", chk);

        char ans1, ans2;
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0x0f, 0, &ans1);
        printf("->%d\n", ans1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0xff, 0, &ans1);
        printf("->%d\n", ans1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xf0, 0, &ans2);
        printf("->%d\n", ans2);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xff, 0, &ans2);
        printf("->%d\n", ans2);

        printf("==================\n");
        if ((ans1 != 0xff) || (ans2 != 0xff)) {
            printf("Test error !!!\n");
            while (true)
                ;
        }
        else {
            printf("Test Success !!!\n");
        }

        _delay_ms(3000);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
