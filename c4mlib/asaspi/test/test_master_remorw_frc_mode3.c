/**
 * @file test_master_remorw_frc_mode3.c
 * @author Deng Xiang-Guan
 * @date 2019.08.12
 * @brief Test remote register by using SPI mode 3, please press slave reset first and then press master reset.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#include <stdlib.h>

#define SPI_MODE 3
#define ASAID 4

#define ans1 87
#define ans2 255

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start master mode 3\n");
    SPIM_Inst.init();
    while (true) {
        char rec1, rec2;
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0xff, 0, &rec1);
        printf("->%d\n", rec1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xff, 0, &rec2);
        printf("->%d\n", rec2);

        printf("==================\n");
        if ((rec1 != 0xff) || (rec2 != 0xff)) {
            printf("Test error !!!\n");
            while (true)
                ;
        }
        else {
            printf("Test Success !!!\n");
        }

        _delay_ms(3000);
    }
}
