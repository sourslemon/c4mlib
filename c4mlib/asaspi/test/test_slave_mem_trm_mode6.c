/**
 * @file test_master_mem_trm.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief Test flash memory recieve by using slave SPI, this file will recieve test packet from master SPI.
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 1, 2, 3, 4};

typedef struct SPI_Mem_trm_test {
    volatile uint8_t index;
    volatile uint8_t command;
    volatile uint32_t reg_addr;
    volatile uint8_t data[5];
}test_spi_mem_t;

test_spi_mem_t mem_str = {0, 0, 0, {0, 0, 0, 0, 0}};

ISR(SPI_STC_vect) {
    uint8_t data = SPIS_Inst.read_byte();
    // printf("%d, %d\n", data, mem_str.index);
    if(mem_str.index == 0) {
        mem_str.command = data;
    }
    else if(mem_str.index < 5) {
        if(mem_str.index == 1) {
            mem_str.reg_addr = data << (8*(4-mem_str.index));
        }
        else {
            mem_str.reg_addr += (uint32_t)data << (8*(4-mem_str.index));
        }
    }
    else if(mem_str.index < 10) {
        mem_str.data[mem_str.index-5] = data;
    }
    mem_str.index++;
    if(mem_str.index == 10) {
        mem_str.index = 0;
    }
}

int main() {
    // Setup
    uint16_t success_cnt = 0;
    C4M_STDIO_init();
    printf("Start test SPIS_Mem_trm mode 6\n");
    SPIS_Inst.init();
    sei();
    _delay_ms(3000);
    while(true) {
        uint8_t temp = 0;
        cli();
        printf("cmd:%x, regAdd:%lu, data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", 
                mem_str.command, mem_str.reg_addr, mem_str.data[0], mem_str.data[1], mem_str.data[2], mem_str.data[3], mem_str.data[4]);
        if(mem_str.command == test_command) {
            if(mem_str.reg_addr == mem_addr) {
                for(uint8_t i=0;i<sizeof(test_data);i++) {
                    if(mem_str.data[i] == test_data[i]) {
                        temp++;
                        test_data[i] += sizeof(test_data);
                    }
                }
                if(temp == 5) {
                    success_cnt++;
                }
            }
        }
        sei();
        if(success_cnt != 0) {
            printf("Success !!! count:%d\n", success_cnt);
        }
        _delay_ms(3000);

    }

}
