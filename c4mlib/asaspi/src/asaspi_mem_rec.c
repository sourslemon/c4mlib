
/**
 * @file asaspi_mem_rec.c
 * @author Deng Xiang-Guan
 * @date 2019.08.28
 * @brief 提供master SPI memory recieve的通訊方式，將兩部分command、memory address 送出去，送完後flash IC會回傳data給master SPI。
 */
#include "asaspi_master.h"

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

/**
 * @brief SPIM_Mem_trm, master SPI recieve data from flash IC.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, command or call it instruction.
 * @param[in] AddBytes, memory address size.
 * @param[in] MemAdd_p, memory address porinter.
 * @param[in] DataBytes, size that will recieve from flash IC.
 * @param[in] Data_p,  data pointer that points to data recieved by master SPI from flash IC.
 * @return char return error code.
 */
char SPIM_Mem_rec(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p) {
    SPIM_Inst.enable_cs(ASAID);
    switch (mode) {
    case 5:
        SPIM_Inst.spi_swap(RegAdd);
        for(int i=0;i<AddBytes;i++) {
            SPIM_Inst.spi_swap(*((char *)MemAdd_p + i));
        }
        for(int i=0;i<DataBytes;i++) {
            *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
        }
        break;

    case 6:
        SPIM_Inst.spi_swap(RegAdd);
        for(int i=AddBytes-1;i>=0;i--) {
            SPIM_Inst.spi_swap(*((char *)MemAdd_p + i));
        }
        for(int i=0;i<DataBytes;i++) {
            *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
        }
        break;

    default:
        SPIM_Inst.disable_cs(ASAID);
        return HAL_ERROR_MODE_SELECT;
        break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}
