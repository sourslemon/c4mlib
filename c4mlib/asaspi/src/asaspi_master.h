/**
 * @file asaspi_master.h
 * @author Deng Xiang-Guan
 * @date 2019.07.08
 * @brief Implement ASA SPI Master-Slave ModeN function
 */

#ifndef C4MLIB_ASASPI_ASASPI_MASTER_H
#define C4MLIB_ASASPI_ASASPI_MASTER_H

// Include library
#include "c4mlib/device/src/device.h"

/* Public Section Start */
char ASA_SPIM_trm(char mode, char ASAID, char RegAdd, char Bytes, void *Data_p);
char ASA_SPIM_rec(char mode, char ASAID, char RegAdd, char Bytes, void *Data_p);
char ASA_SPIM_frc(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p);
char ASA_SPIM_ftm(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p);
char SPIM_Mem_trm(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p);
char SPIM_Mem_rec(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_ASASPI_ASASPI_MASTER_H
