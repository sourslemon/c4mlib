/**
 * @file asaspi_slave.h
 * @author Deng Xiang-Guan
 * @date 2019.07.08
 * @brief Implement ASA SPI Master-Slave ModeN function
 */

#ifndef C4MLIB_ASASPI_ASASPI_SLAVE_H
#define C4MLIB_ASASPI_ASASPI_SLAVE_H

// Include library
#include "c4mlib/device/src/device.h"

/* Mode state descroption */
#ifndef READ_MODE
#    define READ_MODE 0
#endif /* READ_MODE */
#ifndef SPIS_STATE_IDLE
#    define SPIS_STATE_IDLE 0
#endif /* SPIS_STATE_IDLE */
#ifndef SPIS_STATE_WRITE
#    define SPIS_STATE_WRITE 1
#endif /* SPIS_STATE_WRITE */
#ifndef SPIS_STATE_READ
#    define SPIS_STATE_READ 2
#endif /* SPIS_STATE_READ */
#ifndef SPIS_STATE_CNT_MORE_THAN_BYTES
#    define SPIS_STATE_CNT_MORE_THAN_BYTES 3
#endif /* SPIS_STATE_CNT_MORE_THAN_BYTES */
#ifndef SPIS_STATE_CNT_EQUAL_BYTES
#    define SPIS_STATE_CNT_EQUAL_BYTES 4
#endif /* SPIS_STATE_CNT_EQUAL_BYTES */
#ifndef SPIS_STATE_CHECK_OK
#    define SPIS_STATE_CHECK_OK 5
#endif /* SPIS_STATE_CHECK_OK */
#ifndef SPIS_STATE_NO_CONDITION
#    define SPIS_STATE_NO_CONDITION 6
#endif /* SPIS_STATE_NO_CONDITION */
#ifndef SPIS_STATE_CHECK_FAIL
#    define SPIS_STATE_CHECK_FAIL 7
#endif /* SPIS_STATE_CHECK_FAIL */
#ifndef SPIS_STATE_TIMEOUT
#    define SPIS_STATE_TIMEOUT 8
#endif /* SPIS_STATE_TIMEOUT */
#ifndef SPIS_STATE_DATA
#    define SPIS_STATE_DATA 9
#endif /* SPIS_STATE_DATA */

/* Public Section Start */
void SPIS0_isr(void);
void SPIS1_isr(void);
void SPIS2_isr(void);
void SPIS3_isr(void);
void SPIS4_isr(void);
void SPIS7_isr(void);
void SPIS8_isr(void);
void SPIS9_isr(void);
void SPIS10_isr(void);
/* Public Section End */

#endif  // C4MLIB_ASASPI_ASASPI_SLAVE_H
