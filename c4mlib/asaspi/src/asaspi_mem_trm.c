/**
 * @file asaspi_mem_rec.c
 * @author Deng Xiang-Guan
 * @date 2019.08.28
 * @brief 提供master SPI memory transmit的通訊方式，將三部分command、memory address、data 送出去，將data存入falsh IC內。
 */
#include "asaspi_master.h"

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

/**
 * @brief SPIM_Mem_trm, master SPI transmits data to flash IC.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, command or call it instruction.
 * @param[in] AddBytes, memory address size.
 * @param[in] MemAdd_p, memory address porinter.
 * @param[in] DataBytes, size that will transmit to flash IC.
 * @param[in] Data_p,  data pointer that points to data transmited by master SPI.
 * @return char return error code.
 */
char SPIM_Mem_trm(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p) {
    SPIM_Inst.enable_cs(ASAID);
    switch (mode) {
    case 5:
        SPIM_Inst.spi_swap(RegAdd);
        for(int i=0;i<AddBytes;i++) {
            SPIM_Inst.spi_swap(*((char *)MemAdd_p + i));
        }
        for(int i=0;i<DataBytes;i++) {
            SPIM_Inst.spi_swap(*((char *)Data_p + i));
        }
        break;

    case 6:
        SPIM_Inst.spi_swap(RegAdd);
        for(int i=AddBytes-1;i>=0;i--) {
            SPIM_Inst.spi_swap(*((char *)MemAdd_p + i));
        }
        for(int i=0;i<DataBytes;i++) {
            SPIM_Inst.spi_swap(*((char *)Data_p + i));
        }
        break;

    default:
        SPIM_Inst.disable_cs(ASAID);
        return HAL_ERROR_MODE_SELECT;
        break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}
