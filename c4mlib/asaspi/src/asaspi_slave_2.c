/**
 * @file asaspi_slave_2.c
 * @author Deng Xiang-Guan
 * @date 2019.08.12
 * @brief 提供Remote register SPI mode 2 slave端的狀態機，詳細操作請參照ASA_SPI M_S設計書。
 */
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
// #define USE_C4MLIB_DEBUG
// 5bit address + 3bit control flag
void SPIS2_isr(void) {
    uint8_t tempData = 0;
    uint8_t cf_addr = 0;
    switch (ASASPISerialIsrStr->sm_status) {
        case SPIS_STATE_IDLE:
            tempData = SPIS_Inst.read_byte();
            // NOTE this byte is cf and it's unused.
            // cf = (tempData >> 5)
            cf_addr = tempData & 0x1f;
            ASASPISerialIsrStr->reg_address = cf_addr;
            ASASPISerialIsrStr->sm_status = SPIS_STATE_DATA;
            ASASPISerialIsrStr->check_sum = 0;
            ASASPISerialIsrStr->byte_counter =
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .sz_reg -
                1;
            break;
        case SPIS_STATE_DATA:
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
            }
            break;
        default:
            break;
    }
    DEBUG_INFO("tempData:%x, CF_ADDR:%x, status:%d, byte_counter:%d\n",
               tempData, cf_addr, ASASPISerialIsrStr->sm_status,
               ASASPISerialIsrStr->byte_counter);
}
