/**
 * @file KB00.h
 * @author s915888
 * @brief
 * @date 2019/07/18
 *
 */
#ifndef C4MLIB_KB00_KB00_H
#define C4MLIB_KB00_KB00_H

#include <stdint.h>

/* Public Section Start */
typedef struct {
    uint8_t mode;
    uint8_t keymap[16];
} AsaKb00Para_t;

char ASA_KB00_set(char ASA_ID, char LSByte, char Mask, char shift, char Data,
                  AsaKb00Para_t *Str_p);
char ASA_KB00_get(char ASA_ID, char LSByte, char Bytes, void *Data_p,
                  AsaKb00Para_t *Str_p);
/* Public Section End */

char ASA_KB00_ftm(char ASA_ID, char RegAdd, char Mask, char shift, void *Data_p,
                  void *Str_p);
char ASA_KB00_frc(char ASA_ID, char RegAdd, char Mask, char shift, void *Data_p,
                  void *Str_p);
char ASA_KB00_rec(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);
char ASA_KB00_trm(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);

#endif
