/**
 * @file kb00.c
 * @author s915888
 * @brief
 * @date 2019.07.18
 *
 */

#include "c4mlib/asakb00/src/kb00.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/io.h>
#include <util/delay.h>

static uint8_t KB00_comu(uint8_t ASA_ID);

char ASA_KB00_set(char ASA_ID, char LSByte, char Mask, char shift, char Data,
                  AsaKb00Para_t *Str_p) {
    /* check ASAID & shift */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (shift > 7) {
        return HAL_ERROR_SHIFT;
    }
    switch (LSByte) {
        case 200:
            REGFPT(Str_p->mode, Mask, shift, Data);
            break;
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            Str_p->keymap[LSByte - 201] = Data;
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

char ASA_KB00_get(char ASA_ID, char LSByte, char Bytes, void *Data_p,
                  AsaKb00Para_t *Str_p) {
    int key = 0;
    int i;
    /* check ASAID */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    /* get data */
    switch (LSByte) {
        case 100:
            if (Bytes != 1) {
                // Bytes error
                return HAL_ERROR_BYTES;
            }
            else {
                key = KB00_comu(ASA_ID);
                if (Str_p->mode == 0) {
                    // 鍵號模式，回傳按鍵標號
                    *(char *)Data_p = key;
                }
                else if (Str_p->mode == 1) {
                    // ASCII模式，回傳按鍵對應的ASCII
                    *(char *)Data_p = Str_p->keymap[key - 1];
                }
                else {
                    // 未定義，回傳按鍵標號
                    *(char *)Data_p = key;
                }
                return HAL_OK;
            }
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            // 將設定之ASCII表傳送回
            for (i = 0; i < Bytes; i++) {
                *((char *)Data_p + (LSByte - 201 + i)) =
                    (Str_p->keymap[LSByte - 201 + i]);
            }
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

char ASA_KB00_ftm(char ASA_ID, char RegAdd, char Mask, char shift, void *Data_p,
                  void *Str_p) {
    AsaKb00Para_t *p = Str_p;
    /* check ASAID & shift */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (shift > 7) {
        return HAL_ERROR_SHIFT;
    }
    switch (RegAdd) {
        case 200:
            REGFPT(p->mode, Mask, shift, *((uint8_t *)Data_p));
            break;
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            REGFPT(p->keymap[RegAdd - 201], Mask, shift, *((uint8_t *)Data_p));
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

char ASA_KB00_frc(char ASA_ID, char RegAdd, char Mask, char shift, void *Data_p,
                  void *Str_p) {
    AsaKb00Para_t *p = Str_p;

    /* check ASAID & shift */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (shift > 7) {
        return HAL_ERROR_SHIFT;
    }
    switch (RegAdd) {
        case 200:
            REGFGT(p->mode, Mask, shift, Data_p);
            break;
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            REGFGT(p->keymap[RegAdd - 201], Mask, shift, Data_p);
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

char ASA_KB00_rec(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p) {
    AsaKb00Para_t *p = Str_p;
    uint8_t key;
    uint8_t index;
    /* check ASAID */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    /* get data */
    switch (RegAdd) {
        case 100:
            if (Bytes != 1) {
                // Bytes error
                return HAL_ERROR_BYTES;
            }
            else {
                key = KB00_comu(ASA_ID);
                if (p->mode == 0) {
                    // 鍵號模式，回傳按鍵標號
                    *(char *)Data_p = key;
                }
                else if (p->mode == 1) {
                    // ASCII模式，回傳按鍵對應的ASCII
                    *(char *)Data_p = p->keymap[key - 1];
                }
                else {
                    // 未定義，回傳按鍵標號
                    *(char *)Data_p = key;
                }
                return HAL_OK;
            }
        case 200:
            if (Bytes != 1) {
                // Bytes error
                return HAL_ERROR_BYTES;
            }
            else {
                *((char *)Data_p) = p->mode;
            }
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            // 將設定之ASCII表傳送回
            for (uint8_t i = 0; i < Bytes; i++) {
                index = RegAdd - 201 + i;
                ((char *)Data_p)[index] = p->keymap[index];
            }
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

char ASA_KB00_trm(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p) {
    AsaKb00Para_t *p = Str_p;
    uint8_t index;

    /* check ASAID */
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    switch (RegAdd) {
        case 200:
            if (Bytes != 1) {
                return HAL_ERROR_BYTES;
            }
            else {
                p->mode = *((char *)Data_p);
            }
        case 201:
        case 202:
        case 203:
        case 204:
        case 205:
        case 206:
        case 207:
        case 208:
        case 209:
        case 210:
        case 211:
        case 212:
        case 213:
        case 214:
        case 215:
        case 216:
            // TODO: bytes error
            for (uint8_t i = 0; i < Bytes; i++) {
                index = RegAdd - 201 + i;
                p->keymap[index] = ((char *)Data_p)[index];
            }
            break;
        default:
            return HAL_ERROR_ADDR;
    }
    return HAL_OK;
}

uint8_t KB00_comu(uint8_t ASA_ID) {
    uint8_t data1 = 0;
    uint8_t data2 = 0;

    // ASAID通道切換
    ASABUS_ID_set(ASA_ID);

    // 喚回第一筆資料
    ASABUS_SPI_swap(0);
    _delay_ms(10);
    data1 = ASABUS_SPI_swap(0);

    // 喚回第二筆資料
    ASABUS_SPI_swap(0);
    _delay_ms(10);
    data2 = ASABUS_SPI_swap(0);
    DEBUG_INFO("data1=%d,data2=%d\n", data1, data2);

    // 通訊完畢，ASAID切回0
    ASABUS_ID_set(0);

    // 交換回來資料內容
    // 1 -> 無按鍵按壓
    // 2~17 -> 按鍵 1~16
    // 確認資料相同，再作後續動作，否則錯誤
    if (data1 == data2) {
        if (data2 == 1) {
            // 無鍵被壓下
            return 0x00;
        }
        else {
            // 回傳按下按鍵編號
            return data2 - 1;
        }
    }
    else {
        // 兩次資料不同，當作無鍵被壓下
        return 0x00;
    }
}
