#include "c4mlib/asakb00/src/kb00.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"

#include <avr/io.h>

#include "c4mlib/config/asamodule.cfg"

int main()
{
    C4M_DEVICE_set();
    int kb00=1;
    ASA_SPIM_ftm(100, 4, 200, 0xff, 0, &kb00);
    // 設定成鍵號模式

    char rec;
    while (1) {
        ASA_SPIM_rec(100, 4, 100, 1, &rec);
        printf("rec=%d\n",rec);
    }
}
