/**
 * @file asa_twi_slave_mode1.c
 * @author Yuchen
 * @brief Slave TWI mode2 通訊封包函式實現
 * @date 2019-03-29
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

void TWI1_isr(void) {
    DEBUG_INFO("TWI_STATUS---%x\n", TWI_STATUS);
    switch (TWI_STATUS) {
        case TWI_SR_SLA_ACK: {
            DEBUG_INFO("Own SLA+W has been received---%x\n", TWSR);
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_SR_DATA_ACK: {
            DEBUG_INFO(
                "Previously addressed with own SLA+W data has been "
                "received---%x\n",
                TWSR);
            TWICom_ACKCom(USE_ACK);
            //固定將接收的資料存入ID = 2的暫存器
            if (ASATWISerialIsrStr->byte_counter <
                ASATWISerialIsrStr->remo_reg[2].sz_reg) {
                ASATWISerialIsrStr->remo_reg[2]
                    .data_p[ASATWISerialIsrStr->byte_counter] = TWDR;
                ASATWISerialIsrStr->byte_counter++;
            }
            break;
        }
        case TWI_SR_DATA_STO: {
            DEBUG_INFO(
                "A STOP condition or repeated START condition has been "
                "received---%x\n",
                TWSR);
            TWICom_ACKCom(USE_ACK);
            //初始化byte_counter
            ASATWISerialIsrStr->byte_counter = 0;
            break;
        }
        case TWI_ST_SLA_ACK: {
            DEBUG_INFO("Own SLA+R has been received---%x\n", TWSR);
            //第一筆資料傳送
            //固定傳送ID = 2的暫存器資料
            TWDR = ASATWISerialIsrStr->remo_reg[2]
                       .data_p[ASATWISerialIsrStr->byte_counter];
            ASATWISerialIsrStr->byte_counter++;
            //若只傳送一筆資料，傳送完畢後直送NACK
            if (ASATWISerialIsrStr->byte_counter ==
                ASATWISerialIsrStr->remo_reg[2].sz_reg) {
                ASATWISerialIsrStr->byte_counter = 0;
                TWICom_ACKCom(USE_NACK);
            }
            else {
                //若傳送資料不只有一筆，直送ACK
                TWCR |= (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
            }
            break;
        }
        case TWI_ST_DATA_ACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n", TWSR);
            //資料尚未傳送完畢
            //傳送下一筆資料 + ACK
            TWDR = ASATWISerialIsrStr->remo_reg[2]
                       .data_p[ASATWISerialIsrStr->byte_counter];
            ASATWISerialIsrStr->byte_counter++;
            TWICom_ACKCom(USE_ACK);
            //最後一筆資料
            //傳送最後一筆資料 + NACK
            if (ASATWISerialIsrStr->byte_counter ==
                ASATWISerialIsrStr->remo_reg[2].sz_reg) {
                ASATWISerialIsrStr->byte_counter = 0;
                TWICom_ACKCom(USE_NACK);
            }
            break;
        }
        case TWI_ST_Data_last: {
            DEBUG_INFO("Last data byte in TWDR has been transmitted---%x\n",
                       TWSR);
            //最後一筆資料以傳遞完畢
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_ST_DATA_NACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n", TWSR);
            TWICom_ACKCom(USE_ACK);
            break;
        }
    }
}
