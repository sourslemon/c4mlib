/**
 * @file twi.c
 * @author Yuchen
 * @brief implement TWI hardware
 * @date 2019-03-29
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "c4mlib/asatwi/src/twi.h"

#include "c4mlib/debug/src/debug.h"

uint32_t TWITimeoutcounter = TIMEOUTSETTING;

uint8_t TWI_Reg_tram(uint8_t reg) {
    // Prepare the data and start the TWI
    TWDR = reg;
    TWCR = (1 << TWINT) | (1 << TWEN);
    // Blocking here, wait for TWI operation finish and return
    while (!(TWCR & (1 << TWINT))) {
        TWITimeoutcounter--;
        if (TWITimeoutcounter == 0) {
            TWITimeoutcounter = TIMEOUTSETTING;
            return (TIMEOUT_FLAG);
        }
    }
    DEBUG_INFO("[HAL] TWI_Reg_tram_TWI_STATUS = %x\n", TWI_STATUS);
    return (TWI_STATUS);
}
uint8_t TWI_Reg_rec(uint8_t *data) {
    // Enable TWI Acknowledge bit
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
    // Blocking here, wait for TWI operation finish
    while (!(TWCR & (1 << TWINT))) {
        TWITimeoutcounter--;
        // printf("RECEIVE=%d\n",TWITimeoutcounter);
        if (TWITimeoutcounter == 0) {
            TWITimeoutcounter = TIMEOUTSETTING;
            return (TIMEOUT_FLAG);
        }
    }
    // The data was ready, store the data and return
    *data = TWDR;
    DEBUG_INFO("[HAL] TWI_Reg_rec_TWI_STATUS = %x\n", TWI_STATUS);
    return (TWI_STATUS);
}
uint8_t TWI_Reg_recNack(uint8_t *data) {
    // Enable TWI Acknowledge bit
    TWCR = (1 << TWINT) | (1 << TWEN);
    // Blocking here, wait for TWI operation finish
    while (!(TWCR & (1 << TWINT))) {
        TWITimeoutcounter--;
        // printf("RECEIVE=%d\n",TWITimeoutcounter);
        if (TWITimeoutcounter == 0) {
            TWITimeoutcounter = TIMEOUTSETTING;
            return (TIMEOUT_FLAG);
        }
    }
    // The data was ready, store the data and return
    *data = TWDR;
    DEBUG_INFO("[HAL] TWI_Reg_rec_TWI_STATUS = %x\n", TWI_STATUS);
    return (TWI_STATUS);
}

uint8_t TWICom_Start(uint8_t _start) {
    if (_start == 1) {
        TWCR = (1 << TWSTA) | (1 << TWINT) | (1 << TWEN);
        // Blocking here, wait for TWI operation finish
        while (!(TWCR & (1 << TWINT))) {
            TWITimeoutcounter--;
            if (TWITimeoutcounter == 0) {
                TWITimeoutcounter = TIMEOUTSETTING;
                return (TIMEOUT_FLAG);
            }
        }
    }
    DEBUG_INFO("[HAL] TWICom_Start_TWI_STATUS = %x\n", TWI_STATUS);
    return (TWI_STATUS);
}
void TWICom_ACKCom(uint8_t ack_p) {
    if (ack_p == USE_ACK) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
    }
    if (ack_p == USE_NACK) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWIE);
    }
}
void TWICom_Stop(uint8_t stop_p) {
    if (stop_p == 1) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
    }
}
