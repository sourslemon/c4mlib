#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"
#define ARRARY_SIZE 10
#define SLA 0x39

void TWI_Master_set();
uint8_t check = 0;

int main(void) {
    C4M_STDIO_init();
    TWI_Master_set();
    uint8_t data1[ARRARY_SIZE];
    for (int i = 0; i < ARRARY_SIZE; i++) {
        data1[i] = i + 10;
        printf("[MASTER] Transmit mode4 data1[%d] = %d\t\n", i, data1[i]);
    }
    uint8_t Mode = 4;
    uint8_t regadd1 = 0x02;
    uint8_t bytes = ARRARY_SIZE;
    check = TWIM_trm(Mode, SLA, regadd1, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    check = TWIM_rec(Mode, SLA, regadd1, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode4 data1[%d] = %d\t\n", i, data1[i]);
    }
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
