#include "timeout.h"

volatile TimerCntStrType TimerCntStr_inst;

// TODO: Modify to use const MACRO initalize
// Initialize the TimeoutCollector_t
void TimerCntStr_init(TimerCntStrType* p_TimerCntStr) {
    p_TimerCntStr->total = 0;

    // TODO: Can be remove, because total = 0,
    // and check function don't need to use these un-initialize TimerCntStr_inst
    for (int i = 0; i < 10; i++) {
        p_TimerCntStr->timeoutISR_inst[i].counter = 0;
        p_TimerCntStr->timeoutISR_inst[i].p_ISRFunc = 0;
        p_TimerCntStr->timeoutISR_inst[i].p_postSlot = 0;
        p_TimerCntStr->timeoutISR_inst[i].time_limit = 0;
    }
}

// Register TimeoutISR instance
uint8_t Timeout_reg(ISRFunc p_ISRFunc, uint8_t* p_postSlot,
                    uint16_t time_limit) {
    uint8_t new_timeoutId = TimerCntStr_inst.total;
    TimerCntStr_inst.timeoutISR_inst[new_timeoutId].counter = time_limit;
    TimerCntStr_inst.timeoutISR_inst[new_timeoutId].p_ISRFunc = p_ISRFunc;
    TimerCntStr_inst.timeoutISR_inst[new_timeoutId].time_limit = time_limit;
    TimerCntStr_inst.timeoutISR_inst[new_timeoutId].p_postSlot = p_postSlot;
    TimerCntStr_inst.timeoutISR_inst[new_timeoutId].enable =
        0;  // Default Disable Timeout ISR

    TimerCntStr_inst.total++;

    return new_timeoutId;
}

// Timeout tick, need to called by fixed period
void Timeout_tick() {
    for (int i = 0; i < TimerCntStr_inst.total; i++) {
        // printf("[%u] %u , %u\n", i,
        // TimerCntStr_inst.timeoutISR_inst[i].counter,
        // TimerCntStr_inst.timeoutISR_inst[i].time_limit);
        if (TimerCntStr_inst.timeoutISR_inst[i].counter == 0) {
            // If there is not enable, skip
            if (!(TimerCntStr_inst.timeoutISR_inst[i].enable)) {
                continue;
            }

            // Execute timeout ISR
            *(TimerCntStr_inst.timeoutISR_inst[i].p_postSlot) =
                1;  // Post the Timeout Flag first
            TimerCntStr_inst.timeoutISR_inst[i].p_ISRFunc();
            // Exit Timeout ISR routine
            // TODO: Check there is nothing to do or need to reset
            // TimeoutISR_inst
        }
        else {
            TimerCntStr_inst.timeoutISR_inst[i].counter--;
        }
    }
}

// Reset timeout
void Timeout_reset(uint8_t timeout_Id) {
    if (timeout_Id >= TimerCntStr_inst.total) {
        // TODO: Handle ID not exist exception here
        return;
    }

    // Reset the counter and Timeout ISR flag
    TimerCntStr_inst.timeoutISR_inst[timeout_Id].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_Id].time_limit;

    *(TimerCntStr_inst.timeoutISR_inst[timeout_Id].p_postSlot) = 0;
}

// Control timeout
void Timeout_ctl(uint8_t timeout_Id, uint8_t enable) {
    if (timeout_Id >= TimerCntStr_inst.total) {
        // TODO: Handle ID_not_exist exception here
        return;
    }
    TimerCntStr_inst.timeoutISR_inst[timeout_Id].enable = enable;
}
