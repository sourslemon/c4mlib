/**
 * @file test_put_array.c
 * @author LiYu87
 * @brief 測試函式 HMI_put_array
 * @date 2019.08.21
 * 
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    C4M_STDIO_init();

    float data[5] = {1.1, -1, 0, 1, -2.1};

    HMI_put_array(HMI_TYPE_F32, 5, data);

    return 0;
}
