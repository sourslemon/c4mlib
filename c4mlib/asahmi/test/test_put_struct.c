/**
 * @file test_put_struct.c
 * @author LiYu87
 * @brief 測試函式 HMI_put_struct
 * @date 2019.08.21
 *
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

struct st {
    uint8_t a[5];
    uint16_t b[5];
};

int main() {
    C4M_STDIO_init();

    struct st data = {
        .a = {0,1,2,3,4},
        .b = {5,6,7,8,9}
    };

    HMI_put_struct("ui8_5,ui16_5", sizeof(struct st), &data);

    return 0;
}
