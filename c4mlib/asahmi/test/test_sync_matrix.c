/**
 * @file test_sync_matrix.c
 * @author LiYu87
 * @brief 測試傳送矩陣的同步機制
 * @date 2019.08.21
 * 
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    C4M_STDIO_init();

    uint8_t data[2][3] = {{1, 2, 3}, {4, 5, 6}};

    HMI_snput_matrix(HMI_TYPE_I8, 2, 3, data);
    HMI_snget_matrix(HMI_TYPE_I8, 2, 3, data);
    HMI_snput_matrix(HMI_TYPE_I8, 2, 3, data);

    return 0;
}
