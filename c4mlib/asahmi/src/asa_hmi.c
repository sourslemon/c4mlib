/**
 * @file asa_hmi.c
 * @author LiYu87
 * @date 2019.08.21
 * @brief 放置HMI函式實作
 *
 * HMI 代表PC人機，角度是從ASA單板電腦看出去
 * REMO 代表remote，代表遠端的ASA單板電腦，角度是從PC看出去
 */

#include "asa_hmi.h"

#include "c4mlib/device/src/device.h"

#include <string.h>

/* define section ----------------------------------------------------------- */
// defines of error codes
#define HMI_RES_OK 0
// Error code for array exchange
#define HMI_AR_HEADER_ERROR 1
#define HMI_AR_BYTES_ERROR 2
#define HMI_AR_TYPE_ERROR 3
#define HMI_AR_CHKSUM_ERROR 4
// Error code for struct exchange
#define HMI_ST_HEADER_ERROR 1
#define HMI_ST_BYTES_ERROR 2
#define HMI_ST_FS_ERROR 3  ///< FormatString
#define HMI_ST_CHKSUM_ERROR 4

// register define
#define HMI_UCSRA UCSR0A
#define HMI_UDR UDR0

// pin number define
#define HMI_UDRE UDRE0
#define HMI_RXC RXC0

#define HEADER 0xAC
#define PAC_TYPE_AR 1
#define PAC_TYPE_MT 2
#define PAC_TYPE_ST 3

/* static variables section ------------------------------------------------- */
static uint8_t DataSize[10] = {
     1,   2,   4,   8,   1,    2,    4,    8,   4,   8};
//  i8, i16, i32, i64, ui8, ui16, ui32, ui64, f32, f64

static char TypeStr[10][4] = {{"i8"},   {"i16"},  {"i32"},  {"i64"}, {"ui8"},
                              {"ui16"}, {"ui32"}, {"ui64"}, {"f32"}, {"f64"}};

/* public functions declaration section ------------------------------------- */
char HMI_put_array(char Type, char Num, void *Data_p);
char HMI_get_array(char Type, char Num, void *Data_p);

char HMI_put_matrix(char Type, char Dim1, char Dim2, void *Data_p);
char HMI_get_matrix(char Type, char Dim1, char Dim2, void *Data_p);

char HMI_put_struct(char *FormatString, int Bytes, void *Data_p);
char HMI_get_struct(char *FormatString, int Bytes, void *Data_p);

char HMI_snput_array(char Type, char Num, void *Data_p);
char HMI_snget_array(char Type, char Num, void *Data_p);

char HMI_snput_matrix(char Type, char Dim1, char Dim2, void *Data_p);
char HMI_snget_matrix(char Type, char Dim1, char Dim2, void *Data_p);

char HMI_snput_struct(char *FormatString, int Bytes, void *Data_p);
char HMI_snget_struct(char *FormatString, int Bytes, void *Data_p);
/* static functions declaration section ------------------------------------- */
static void uart_put(uint8_t data);
static uint8_t uart_get(void);

/* static function contents section ----------------------------------------- */
static void uart_put(uint8_t data) {
    while (bit_is_clear(HMI_UCSRA, HMI_UDRE)) {
        ;
    }
    HMI_UDR = data;
}

static uint8_t uart_get(void) {
    while (bit_is_clear(HMI_UCSRA, HMI_RXC)) {
        ;
    }
    return HMI_UDR;
}
/* header function contents section ----------------------------------------- */
// async functions
char HMI_put_array(char Type, char Num, void *Data_p) {
    uint8_t chksum = 0;
    uint16_t i;

    uint16_t datalen = Num * DataSize[(uint8_t)Type];
    uint16_t paclen = datalen + 5;

    chksum += PAC_TYPE_AR;
    chksum += Type;
    chksum += Num;
    chksum += datalen >> 8;
    chksum += datalen & 0xFF;
    for (i = 0; i < datalen; i++) {
        chksum += ((char *)Data_p)[i];
    }

    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(paclen >> 8);
    uart_put(paclen & 0xFF);
    uart_put(PAC_TYPE_AR);
    uart_put(Type);
    uart_put(Num);
    uart_put(datalen >> 8);
    uart_put(datalen & 0xFF);
    for (i = 0; i < datalen; i++) {
        uart_put(((char *)Data_p)[i]);
    }
    uart_put(chksum);

    return HMI_RES_OK;
}

char HMI_put_matrix(char Type, char Dim1, char Dim2, void *Data_p) {
    uint8_t chksum = 0;
    uint16_t i;

    uint16_t datalen = Dim1 * Dim2 * DataSize[(uint8_t)Type];
    uint16_t paclen = datalen + 6;

    chksum += PAC_TYPE_MT;
    chksum += Type;
    chksum += Dim1;
    chksum += Dim2;
    chksum += datalen >> 8;
    chksum += datalen & 0xFF;
    for (i = 0; i < datalen; i++) {
        chksum += ((char *)Data_p)[i];
    }

    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(paclen >> 8);
    uart_put(paclen & 0xFF);
    uart_put(PAC_TYPE_MT);
    uart_put(Type);
    uart_put(Dim1);
    uart_put(Dim2);
    uart_put(datalen >> 8);
    uart_put(datalen & 0xFF);
    for (i = 0; i < datalen; i++) {
        uart_put(((char *)Data_p)[i]);
    }
    uart_put(chksum);

    return HMI_RES_OK;
}

char HMI_put_struct(char *FormatString, int Bytes, void *Data_p) {
    uint16_t i;
    uint8_t len_fs = 0;
    uint8_t chksum = 0;

    if (strlen(FormatString) >= 255)  // 資料結構字串長度過長
        return HMI_ST_FS_ERROR;
    len_fs = strlen(FormatString);

    uint16_t paclen = Bytes + len_fs + 4;

    chksum += PAC_TYPE_ST;
    chksum += len_fs;
    chksum += Bytes >> 8;
    chksum += Bytes & 0xFF;

    for (i = 0; i < len_fs; i++) {
        chksum += ((char *)FormatString)[i];
    }
    for (i = 0; i < Bytes; i++) {
        chksum += ((char *)Data_p)[i];
    }

    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(HEADER);
    uart_put(paclen >> 8);
    uart_put(paclen & 0xFF);
    uart_put(PAC_TYPE_ST);
    uart_put(len_fs);
    for (i = 0; i < len_fs; i++) {
        uart_put(((char *)FormatString)[i]);
    }
    uart_put(Bytes >> 8);
    uart_put(Bytes & 0xFF);
    for (i = 0; i < Bytes; i++) {
        uart_put(((char *)Data_p)[i]);
    }
    uart_put(chksum);

    return HMI_RES_OK;
}

char HMI_get_array(char Type, char Num, void *Data_p) {
    uint8_t chksum = 0;

    uint16_t get_pac_len;
    uint8_t get_pac_type;
    uint8_t get_type;
    uint8_t get_num;
    uint8_t get_len_h;
    uint8_t get_len_l;
    uint16_t get_len;
    uint16_t get_chksum;

    uint8_t err_code = 0;

    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;

    get_pac_len = uart_get();
    get_pac_len = (get_pac_len << 8) | uart_get();

    if (get_pac_len < 4) {
        err_code = 1;
        for (uint16_t i = 0; i < get_pac_len; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }

    get_pac_type = uart_get();
    get_type = uart_get();
    get_num = uart_get();
    get_len_h = uart_get();
    get_len_l = uart_get();
    get_len = (get_len_h << 8) | get_len_l;

    if (get_pac_type != PAC_TYPE_AR) {
        err_code = 2;
    }
    else if (get_type != Type) {
        err_code = 3;
    }
    else if (get_num != Num) {
        err_code = 4;
    }
    if (err_code) {
        for (uint16_t i = 0; i < get_pac_len - 5; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }
    else {
        chksum += get_pac_type;
        chksum += get_type;
        chksum += get_num;
        chksum += get_len_h;
        chksum += get_len_l;
        for (uint16_t i = 0; i < get_len; i++) {
            ((uint8_t *)Data_p)[i] = uart_get();
            chksum += ((uint8_t *)Data_p)[i];
        }
        get_chksum = uart_get();  // chksum
        if (chksum != get_chksum) {
            return 4;
        }
    }
    return HMI_RES_OK;
}

char HMI_get_matrix(char Type, char Dim1, char Dim2, void *Data_p) {
    uint8_t chksum = 0;

    uint16_t get_pac_len;
    uint8_t get_pac_type;
    uint8_t get_type;
    uint8_t get_dim1;
    uint8_t get_dim2;
    uint8_t get_len_h;
    uint8_t get_len_l;
    uint16_t get_len;
    uint16_t get_chksum;

    uint8_t err_code = 0;

    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;

    get_pac_len = uart_get();
    get_pac_len = (get_pac_len << 8) | uart_get();

    if (get_pac_len < 6) {
        err_code = 1;
        for (uint16_t i = 0; i < get_pac_len; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }

    get_pac_type = uart_get();
    get_type = uart_get();
    get_dim1 = uart_get();
    get_dim2 = uart_get();
    get_len_h = uart_get();
    get_len_l = uart_get();
    get_len = (get_len_h << 8) | get_len_l;

    if (get_pac_type != PAC_TYPE_MT) {
        err_code = 2;
    }
    else if (get_type != Type) {
        err_code = 3;
    }
    else if (get_dim1 != Dim1) {
        err_code = 4;
    }
    else if (get_dim2 != Dim2) {
        err_code = 5;
    }

    if (err_code) {
        for (uint16_t i = 0; i < get_pac_len - 6; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }
    else {
        chksum += get_pac_type;
        chksum += get_type;
        chksum += get_dim1;
        chksum += get_dim2;
        chksum += get_len_h;
        chksum += get_len_l;
        for (uint16_t i = 0; i < get_len; i++) {
            ((uint8_t *)Data_p)[i] = uart_get();
            chksum += ((uint8_t *)Data_p)[i];
        }
        get_chksum = uart_get();  // chksum
        if (chksum != get_chksum) {
            return 4;
        }
    }
    return HMI_RES_OK;
}

char HMI_get_struct(char *FormatString, int Bytes, void *Data_p) {
    uint8_t chksum = 0;

    uint16_t get_pac_len;
    uint8_t get_pac_type;
    uint8_t get_fs_len;
    uint8_t get_len_h;
    uint8_t get_len_l;
    uint16_t get_len;
    uint16_t get_chksum;

    uint8_t tmp;
    uint8_t err_code = 0;

    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;
    if (uart_get() != HEADER)
        return 1;

    get_pac_len = uart_get();
    get_pac_len = (get_pac_len << 8) | uart_get();

    if (get_pac_len < 2) {
        err_code = 1;
        for (uint16_t i = 0; i < get_pac_len; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }

    get_pac_type = uart_get();
    if (get_pac_type != PAC_TYPE_ST) {
        err_code = 2;
        for (uint16_t i = 0; i < get_pac_len - 1; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }

    get_fs_len = uart_get();
    for (uint8_t i = 0; i < get_fs_len; i++) {
        tmp = uart_get();
        if (tmp != FormatString[i]) {
            err_code = 3;
        }
    }
    if (err_code) {
        for (uint16_t i = 0; i < get_pac_len - 1 - get_fs_len; i++) {
            uart_get();
        }
        uart_get();  // chksum
        return err_code;
    }

    get_len_h = uart_get();
    get_len_l = uart_get();
    get_len = (get_len_h << 8) | get_len_l;

    chksum += get_pac_type;
    chksum += get_fs_len;
    for (uint8_t i = 0; i < get_fs_len; i++) {
        chksum += FormatString[i];
    }
    chksum += get_len_h;
    chksum += get_len_l;
    for (uint16_t i = 0; i < get_len; i++) {
        ((uint8_t *)Data_p)[i] = uart_get();
        chksum += ((uint8_t *)Data_p)[i];
    }
    get_chksum = uart_get();  // chksum
    if (chksum != get_chksum) {
        return 4;
    }

    return HMI_RES_OK;
}

// sync functions
char HMI_snput_array(char Type, char Num, void *Data_p) {
    char s[20];
    printf("~GA, %s_%d\n", TypeStr[(uint8_t)Type], Num);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_put_array(Type, Num, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_put_array(Type, Num, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}

char HMI_snput_matrix(char Type, char Dim1, char Dim2, void *Data_p) {
    char s[20];
    printf("~GM, %s_%dx%d\n", TypeStr[(uint8_t)Type], Dim1, Dim2);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_put_matrix(Type, Dim1, Dim2, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_put_matrix(Type, Dim1, Dim2, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}
char HMI_snput_struct(char *FormatString, int Bytes, void *Data_p) {
    char s[20];
    printf("~GS, %s\n", FormatString);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_put_struct(FormatString, Bytes, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_put_struct(FormatString, Bytes, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}

char HMI_snget_array(char Type, char Num, void *Data_p) {
    char s[20];
    printf("~PA, %s_%d\n", TypeStr[(uint8_t)Type], Num);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_get_array(Type, Num, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_get_array(Type, Num, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}

char HMI_snget_matrix(char Type, char Dim1, char Dim2, void *Data_p) {
    char s[20];
    printf("~PM, %s_%dx%d\n", TypeStr[(uint8_t)Type], Dim1, Dim2);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_get_matrix(Type, Dim1, Dim2, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_get_matrix(Type, Dim1, Dim2, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}

char HMI_snget_struct(char *FormatString, int Bytes, void *Data_p) {
    char s[20];
    printf("~PS, %s\n", FormatString);
    scanf("%s", s);
    if (!strncmp("~ACK", s, 4)) {
        return HMI_get_struct(FormatString, Bytes, Data_p);
    }
    else if (!strncmp("~BZ", s, 3)) {
        scanf("%s", s);
        if (!strncmp("~Ready", s, 6)) {
            printf("~ACK\n");
            return HMI_get_struct(FormatString, Bytes, Data_p);
        }
        else {
        }
    }
    return HMI_RES_OK;
}
