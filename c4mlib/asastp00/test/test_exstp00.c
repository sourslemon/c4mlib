#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/spi.h"
#include "c4mlib/asastp00/src/stp00.h"
#include "c4mlib/asaspi/src/asaspi_master.h"

int data = 200;
int dataput = 1;
int dataputl = -1;

int main() {
    C4M_DEVICE_set();
    SPI_fpt(200, 0x03, 0, 3);  //除頻值設定為f/64
    SPI_fpt(201, 0x01, 0, 1);  //除頻值設定為f/64
    int a = ASA_SPIM_trm(101, 1, 1, 2, &data);
    printf("%d\n", a);
    _delay_ms(300);
    while (1) {
        a = ASA_SPIM_trm(101,1, 3, 2, &dataput);
        printf("%d\n", a);
        _delay_ms(10000);
        a = ASA_SPIM_trm(101, 1, 3, 2, &dataputl);
        printf("%d\n", a);
        _delay_ms(10000);
    }
}
