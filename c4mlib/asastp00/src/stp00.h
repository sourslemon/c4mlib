#ifndef C4MLIB_STP00_STP00_H
#define C4MLIB_STP00_STP00_H

#include <stdint.h>

/* Public Section Start */
char ASA_STP00_trm(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p);
char ASA_STP00_rec(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p);
char ASA_STP00_frc(char ASA_ID, char RegAdd, char Mask, char Shift,
                   void* Data_p, void* Str_p);
char ASA_STP00_ftm(char ASA_ID, char RegAdd, char Mask, char shift,
                   void* Data_p, void* Str_p);
/* Public Section End */

#endif
