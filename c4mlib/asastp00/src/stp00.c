/**
 * @file stp00.c
 * @author s915888
 * @brief
 * @version 0.1
 * @date 2019-08-28
 * RegAdd=1(速度設定暫存器),2(步進步數暫存器),3(降速除頻值暫存器)
 * Bytes=2
 *
 */
#include "c4mlib/asastp00/src/stp00.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/macro/src/std_res.h"

#include <util/delay.h>

char ASA_STP00_trm(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p) {
    if (ASA_ID > 7) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (Bytes != 2) {
        return HAL_ERROR_BYTES;
    }
    if (RegAdd < 1 || RegAdd > 3) {
        return HAL_ERROR_ADDR;
    }
    ASABUS_ID_set(ASA_ID);
    _delay_us(50);
    SPIM_Inst.spi_swap(RegAdd);
#ifdef USE_C4MLIB_DEBUG
    _delay_ms(20);
#else
    _delay_ms(1);
#endif
    for (int i = Bytes - 1; i >= 0; i--) {
        SPIM_Inst.spi_swap(*((char*)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
        _delay_ms(20);
#else
        // _delay_ms(1);
#endif
    }
    ASABUS_ID_set(0);
    return HAL_OK;
}
char ASA_STP00_rec(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p) {
    return HAL_OK;
}
char ASA_STP00_frc(char ASA_ID, char RegAdd, char Mask, char Shift,
                   void* Data_p, void* Str_p) {
    return HAL_OK;
}
char ASA_STP00_ftm(char ASA_ID, char RegAdd, char Mask, char shift,
                   void* Data_p, void* Str_p) {
    return HAL_OK;
}
