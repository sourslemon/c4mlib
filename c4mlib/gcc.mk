##
# @file makefile
# @author LiYu87
# @date 2019.09.23
# @brief 提供c4mlib專案所有gcc相關的編譯方式及參數
#
# 包括如何編譯成obj、elf、hex之規則
# 並提供參數設置 Ex：編譯器資料夾、MCU資訊等等

# Settings
COMPILER_PATH ?=
MCU     ?= atmega128
F_CPU   ?= 11059200

## 是否使用 libc4m.a 去取代所有 .o 來建置
USE_LIBC4MA    ?= 0
## 是否開啟 DEBUG 輸出
DEBUG ?= 0

CROSS   := $(COMPILER_PATH)avr
CC      := $(CROSS)-gcc
AR      := $(CROSS)-ar
SIZE    := $(CROSS)-size
OBJDUMP := $(CROSS)-objdump
OBJCOPY := $(CROSS)-objcopy
NM      := $(CROSS)-nm

GCCVERSION := $(shell '$(CC)' -dumpversion)

## Include Path
IPATH ?= .

## Options common to compile, link and assembly rules
COMMON = -mmcu=$(MCU)

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
CFLAGS += -DF_CPU=$(F_CPU)UL
CFLAGS += -Wall -gdwarf-2 -std=gnu99 -Os
CFLAGS += -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums 
CFLAGS += -Wtype-limits
# NOTE winavr-gcc is gcc version 4.3.3 doesn't have parameter -no-canonical-prefixes 
ifneq "$(GCCVERSION)" "4.3.3"
	CFLAGS += -no-canonical-prefixes 
	# Do not expand any symbolic links, resolve references to /../ or /./, or 
	# make the path absolute when generating a relative prefix.
endif
# 是否開啟 DEBUG 輸出
ifeq "$(DEBUG)" "1"
	CFLAGS += -DUSE_C4MLIB_DEBUG
endif
CFLAGS += -I$(IPATH)

## Assembly specific flags
ASMFLAGS = $(COMMON)
ASMFLAGS += $(CFLAGS)
ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Linker flags
LDFLAGS  = $(COMMON)
# do not link obj file which has no used symbol
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -Wl,-u,USE_C4MLIB_INTERRUPT
# LDFLAGS += -Wl,-u,vfprintf  -Wl,-u,vfscanf -Wl,-Map=$*.map

## Archiver flags
ARFLAGS = rcs

## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom -R .fuse -R .lock -R .signature
HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings

## Libraries
LIBDIRS = 
LIBS = -lm -lprintf_flt -lscanf_flt

## Objects that must be built in order to link
LIBSRC  ?=
LIBOBJS ?=
ifeq ($(USE_LIBC4MA), 1)
	LINKOBJS = $(LINKONLYLIBOBJS) ../libc4m.a $(LIBDIRS) $(LIBS)
else
	LINKOBJS = $(LINKONLYLIBOBJS) $(LIBOBJS) $(LIBDIRS) $(LIBS)
endif

## Build
# Generates a library archive file from the user application,
# which can be linked into other applications
%.a: $(OBJS)
	$(AR) rcs $@ $(OBJS)

##Link
# Generates an ELF debug file from the user application,
# which can be further processed for FLASH and EEPROM data files,
# or used for programming and debugging directly
%.elf: %.o $(LIBOBJS)
	$(CC) $(LDFLAGS) $< $(LIBOBJS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

# Extracts out the loadable FLASH memory data from the project ELF file,
# and creates an Intel HEX format file of it
%.hex: %.elf
	$(OBJCOPY) -O ihex $(HEX_FLASH_FLAGS) $< $@

# Extracts out the loadable FLASH memory data from the project ELF file,
# and creates an Binary format file of it
%.bin: %.elf
	$(OBJCOPY) -O binary -R .eeprom -R .fuse -R .lock -R .signature $< $@

# Extracts out the loadable EEPROM memory data from the project ELF file,
# and creates an Intel HEX format file of it
%.eep: %.elf
	$(OBJCOPY) $(HEX_EEPROM_FLAGS) -O ihex $< $@ || exit 0

# Creates an assembly listing file from an input project ELF file,
# containing interleaved assembly and source data
%.lss: %.elf
	$(OBJDUMP) -h -S $< > $@

# Creates a symbol file listing the loadable and discarded symbols
# from an input project ELF file
%.sym: %.elf
	$(NM) -n $< > $@

# Prints size information of a compiled application (FLASH, RAM and EEPROM usages)
size: ${TARGET}
	$(SIZE) -C --mcu=${MCU} ${TARGET}

version:
	@echo os is $(OS)
	@echo gcc version is $(GCCVERSION)
	$(CC) --version
