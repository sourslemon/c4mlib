/**
 * @file tim.h
 * @author
 * @date
 * @brief
 * 
 */

#ifndef C4MLIB_HARDWARE2_TIM_H
#define C4MLIB_HARDWARE2_TIM_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __tim_set_str {
    uint8_t ClockPreScale;
    uint8_t WaveMode;
    uint8_t WaveoutA;
    uint8_t WaveoutB;
    uint8_t WaveoutC;
    uint8_t TimIntEn;
    uint16_t OCRA;
    uint16_t OCRB;
    uint16_t OCRC;
} TimSetStr_t;

typedef struct __tim_int_str {
    TimSetStr_t TimSet;

    uint8_t (*SetFunc_p)(struct __tim_int_str *);

    uint8_t IntTotal;                                ///< 紀錄已註冊多少中斷
    volatile FuncBlockStr_t IntFb[MAX_TIM_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} TimIntStr_t;

uint8_t TimInt_net(TimIntStr_t *TimIntStr_p, uint8_t Num);

uint8_t TimInt_set(TimIntStr_t *IntStr_p);

uint8_t TimInt_reg(TimIntStr_t *TimIntStr_p, Func_t FbFunc_p, void *FbPara_p);

void TimInt_en(TimIntStr_t *TimIntStr_p, uint8_t Fb_Id, uint8_t enable);
/* Public Section End */

void TimInt_step(TimIntStr_t *TimIntStr_p);

/* Public Section Start */
/*----- Tim Sets Macros -----------------------------------------------------*/
/* ClockPreScale */
#define TIM_CLOCK_PRESCALE_NONE 0
#define TIM_CLOCK_PRESCALE_1 1
#define TIM_CLOCK_PRESCALE_8 2
#define TIM_CLOCK_PRESCALE_32 3
#define TIM_CLOCK_PRESCALE_64 4
#define TIM_CLOCK_PRESCALE_128 5
#define TIM_CLOCK_PRESCALE_256 6
#define TIM_CLOCK_PRESCALE_1024 7

/* WaveMode */  // Waveform Generation Mode
// TODO 更改名稱 WAVEMODE -> MODE、NORMAL -> OFF
#define TIM_WAVEMODE_Normal 0
#define TIM_WAVEMODE_CTC 1

/* WaveOut*/
#define TIM_WAVEOUT_OFF 0
#define TIM_WAVEOUT_NORMAL 1
#define TIM_WAVEOUT_TOGGLE 2
#define TIM_WAVEOUT_CLEAR 3
#define TIM_WAVEOUT_SET 4

/* TimIntEn */
#define TIM_TIMINTEN_DIS 0
#define TIM_TIMINTEN_EN 1
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_TIM_H
