/**
 * @file spi.h
 * @author
 * @date
 * @brief
 * 
 */

#ifndef C4MLIB_HAREWARE2_SPI_H
#define C4MLIB_HAREWARE2_SPI_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __spi_set_str {
    uint8_t MS;         ///< Master or Slave
    uint8_t SpiEn;      ///< SPI enable
    uint8_t SpiIntEn;   ///< SPI Int enalbe
    uint8_t DataOrder;  ///< Data Order
    uint8_t ClockPol;   ///< Clock Polarity
    uint8_t ClockPha;   ///< Clock Phase
    uint8_t ClockDiv;   ///< Clock Diveder
} SpiSetStr_t;

typedef struct __spi_int_str {
    SpiSetStr_t SpiSet;
    
    uint8_t (*SetFunc_p)(struct __spi_int_str*);

    uint8_t IntTotal;                                     ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_SPIIINT_FUNCNUM];   ///< 紀錄所有已註冊的中斷函式
} SpiIntStr_t;

uint8_t SpiInt_net(SpiIntStr_t* IntStr_p, uint8_t Num);

uint8_t SpiInt_set(SpiIntStr_t* IntStr_p);

uint8_t SpiInt_reg(SpiIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p);

void SpiInt_en(SpiIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable);
/* Public Section End */

void SpiInt_step(SpiIntStr_t* IntStr_p);

/*----- Pwm Sets Macros -----------------------------------------------------*/
/* MS - Master or Slave */
#define SPI_MS_SLAVE  0
#define SPI_MS_MASTER 1

/* DataOrder - Data Order */
#define SPI_DATAORDER_LSB 0
#define SPI_DATAORDER_MSB 1

/* ClockPol - Clock Polarity */
#define SPI_POLARITY_LEAD_RISE 0
#define SPI_POLARITY_LEAD_FALL 1

/* ClockPha - Clock Phase */
#define SPI_PHASE_LEAD_SAMPLE 0
#define SPI_PHASE_LEAD_SETUP 1

/* ClockDiv - Clock Diveder */
#define SPI_DIVEDER_4 0
#define SPI_DIVEDER_16 1
#define SPI_DIVEDER_64 2
#define SPI_DIVEDER_128 3
#define SPI_DIVEDER_2 4
#define SPI_DIVEDER_8 5
#define SPI_DIVEDER_32 6
/*---------------------------------------------------------------------------*/

#endif  // C4MLIB_HAREWARE2_SPI_H
