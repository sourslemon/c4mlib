/**
 * @file twi_imp.c
 * @author 
 * @date 
 * @brief 
 */

#if defined(__AVR_ATmega128__)
#    include "m128/twi_imp.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/twi_imp.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/twi_imp.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
