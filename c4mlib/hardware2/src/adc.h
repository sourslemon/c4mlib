/**
 * @file adc.h
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.09.04
 * @brief
 *
 * 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

#ifndef C4MLIB_HARDWARE2_ADC_H
#define C4MLIB_HARDWARE2_ADC_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __adc_set_str {
    uint8_t Enable;
    uint8_t Channel;
    uint8_t ClockPrescaler;
    uint8_t Resolution;
    uint8_t DataAlign;
    uint8_t Reference;
    uint8_t IntEn;
} AdcSetStr_t;

typedef struct __adc_int_str {
    AdcSetStr_t AdcSet;

    uint8_t (*SetFunc_p)(struct __adc_int_str *);
    uint8_t (*EnFunc_p)(struct __adc_int_str *, uint8_t);
    uint8_t (*TrigFunc_p)(struct __adc_int_str *);
    uint8_t (*IsDoneFunc_p)(struct __adc_int_str *);
    uint8_t (*GetConvFunc_p)(struct __adc_int_str *, void *);

    uint8_t IntTotal;                                   ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_ADCINT_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} AdcIntStr_t;

uint8_t AdcInt_net(AdcIntStr_t *IntStr_p, uint8_t Num);

uint8_t AdcInt_set(AdcIntStr_t *IntStr_p);

uint8_t AdcInt_reg(AdcIntStr_t *IntStr_p, Func_t FbFunc_p, void *FbPara_p);

void AdcInt_en(AdcIntStr_t *IntStr_p, uint8_t Fb_Id, uint8_t enable);

uint8_t AdcInt_trig(AdcIntStr_t *IntStr_p);
uint8_t AdcInt_isDone(AdcIntStr_t *IntStr_p);
uint8_t AdcInt_en2(AdcIntStr_t *IntStr_p, uint8_t En);
uint8_t AdcInt_getConv(AdcIntStr_t *IntStr_p, void *data_p);
/* Public Section End */

void AdcInt_step(AdcIntStr_t *IntStr_p);

/* Public Section Start */
/*----- Adc Sets Macros -----------------------------------------------------*/
#define ADC_CHANNEL_0 0
#define ADC_CHANNEL_1 1
#define ADC_CHANNEL_2 2
#define ADC_CHANNEL_3 3
#define ADC_CHANNEL_4 4
#define ADC_CHANNEL_5 5
#define ADC_CHANNEL_6 6
#define ADC_CHANNEL_7 7
#define ADC_CHANNEL_1230MV 8
#define ADC_CHANNEL_GND 9

#define ADC_CLOCKPRESCALER_2 2
#define ADC_CLOCKPRESCALER_4 4
#define ADC_CLOCKPRESCALER_6 6
#define ADC_CLOCKPRESCALER_8 8
#define ADC_CLOCKPRESCALER_16 16
#define ADC_CLOCKPRESCALER_32 32
#define ADC_CLOCKPRESCALER_64 64
#define ADC_CLOCKPRESCALER_128 128

#define ADC_RESOLUTION_10 10

#define ADC_REF_AREF 0
#define ADC_REF_AVCC 1
#define ADC_REF_2560MV 3

#define ADC_DATAALIGN_RIGHT 0
#define ADC_DATAALIGN_LEFT 1
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_ADC_H
