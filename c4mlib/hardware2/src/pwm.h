/**
 * @file pwm.h
 * @author
 * @date
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARE2_PWM_H
#define C4MLIB_HARDWARE2_PWM_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __pwm_set_str {
    uint8_t ClockPreScale;
    uint8_t WaveMode;
    uint8_t PwmOut;
    uint8_t WaveOutA;
    uint8_t WaveOutB;
    uint8_t WaveOutC;
    uint8_t PWMIntEn;
} PwmSetStr_t;

typedef struct __pwm_int_str {
    PwmSetStr_t PwmSet;

    uint8_t (*SetFunc_p)(struct __pwm_int_str*);

    uint8_t IntTotal;                                   ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_PWMINT_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} PwmIntStr_t;

uint8_t PwmInt_net(PwmIntStr_t* IntStr_p, uint8_t Num);

uint8_t PwmInt_set(PwmIntStr_t* IntStr_p);

uint8_t PwmInt_reg(PwmIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p);

void PwmInt_en(PwmIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable);
/* Public Section End */

void PwmInt_step(PwmIntStr_t* IntStr_p);

/* Public Section Start */
/*----- Pwm Sets Macros -----------------------------------------------------*/
/* ClockPreScale */
#define PWM_CLOCK_PRESCALE_NONE    0
#define PWM_CLOCK_PRESCALE_1       1
#define PWM_CLOCK_PRESCALE_8       2
#define PWM_CLOCK_PRESCALE_32      3
#define PWM_CLOCK_PRESCALE_64      4
#define PWM_CLOCK_PRESCALE_128     5
#define PWM_CLOCK_PRESCALE_256     6
#define PWM_CLOCK_PRESCALE_1024    7
#define PWM_CLOCK_PRESCALE_FALLING 8
#define PWM_CLOCK_PRESCALE_RISING  9

/* WaveMode */
#define PWM_WAVE_MODE_PWM     0
#define PWM_WAVE_MODE_FASTPWM 1

/* WaveOut */
#define PWM_WAVEOUT_OFF    0
#define PWM_WAVEOUT_NORMAL 1
#define PWM_WAVEOUT_TOGGLE 2
#define PWM_WAVEOUT_CLEAR  3
#define PWM_WAVEOUT_SET    4

/* PwmOut */
#define PWM_PWMOUT_A 1
#define PWM_PWMOUT_B 2
#define PWM_PWMOUT_C 4
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_PWM_H
