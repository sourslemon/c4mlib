/**
 * @file spi_imp.c
 * @author maxwu84
 * @date 2019.09.04
 * @brief 
 * 
 */

#include "c4mlib/hardware2/src/m128/spi_imp.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/spi.cfg"

static uint8_t spi_hw_init(SpiIntStr_t *IntStr_p);

SpiIntStr_t SpiImp[SPI_HW_NUM] = {
    {
        .SpiSet = SPI_MASTER_HW_SET_CFG,
        .SetFunc_p = spi_hw_init
    }
};

SpiIntStr_t *SpiIntStrList_p[SPI_HW_NUM];

uint8_t spi_hw_init(SpiIntStr_t *IntStr_p) {
    switch (IntStr_p->SpiSet.MS) {
        case SPI_MS_MASTER:
            // Initialize ASA SPI ID
            ASABUS_ID_init();
            // Setup SPI pins
            DDRB |= (1 << BUS_SPI_MOSI) | (1 << BUS_SPI_SCK) | (1 << BUS_SPI_SS);
            DDRB &= ~(1 << BUS_SPI_MISO);
            // Master CS pin is PF4
            ASA_CS_DDR |= (1 << ASA_CS);

            SPCR |= (1 << MSTR);
            break;
        case SPI_MS_SLAVE:
            DDRB &= ~(1 << BUS_SPI_MOSI) & ~(1 << BUS_SPI_SCK) & ~(1 << BUS_SPI_SS);
            DDRB |= (1 << BUS_SPI_MISO);
            
            SPCR &= ~(1 << MSTR);
            SPCR |= (0 << MSTR);

            break;
    }
    
    switch (IntStr_p->SpiSet.SpiIntEn) {
        case DISABLE:
            SPCR &= ~(1 << SPIE);
            SPCR |= (0 << SPIE);
            break;
        case ENABLE:
            SPCR &= ~(1 << SPIE);
            SPCR |= (1 << SPIE);
            break;
    }

    switch (IntStr_p->SpiSet.DataOrder) {
        case SPI_DATAORDER_LSB:
            SPCR &= ~(1 << DORD);
            SPCR |= (1 << DORD);
            break;
        case SPI_DATAORDER_MSB:
            SPCR &= ~(1 << DORD);
            SPCR |= (0 << DORD);
            break;
    }

    switch (IntStr_p->SpiSet.ClockPol) {
        case SPI_POLARITY_LEAD_RISE:
            SPCR &= ~(1 << CPOL);
            SPCR |= (0 << CPOL);
            break;
        case SPI_POLARITY_LEAD_FALL:
            SPCR &= ~(1 << CPOL);
            SPCR |= (1 << CPOL);
            break;
    }

    switch (IntStr_p->SpiSet.ClockPha) {
        case SPI_PHASE_LEAD_SAMPLE:
            SPCR &= ~(1 << CPHA);
            SPCR |= (0 << CPHA);
            break;
        case SPI_PHASE_LEAD_SETUP:
            SPCR &= ~(1 << CPHA);
            SPCR |= (1 << CPHA);
            break;
    }

    switch (IntStr_p->SpiSet.ClockDiv) {
        case SPI_DIVEDER_4:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_DIVEDER_16:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_DIVEDER_64:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_DIVEDER_128:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_DIVEDER_2:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break;
        case SPI_DIVEDER_8:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break;
        case SPI_DIVEDER_32:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break; 
    }

    switch (IntStr_p->SpiSet.SpiEn) {
        case DISABLE:
            SPCR &= ~(1 << SPE);
            SPCR |= (0 << SPE);
            break;
        case ENABLE:
            SPCR &= ~(1 << SPE);
            SPCR |= (1 << SPE);
            break;
    }
    return 0 ;
}
