
/**
 * @file tim_imp.c
 * @author
 * @brief
 * @date 2019.09.04
 *
 */

#include "c4mlib/hardware2/src/m128/tim_imp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/tim.cfg"

static uint8_t tim0_hw_init(TimIntStr_t *str_p);
static uint8_t tim1_hw_init(TimIntStr_t *str_p);
static uint8_t tim2_hw_init(TimIntStr_t *str_p);
static uint8_t tim3_hw_init(TimIntStr_t *str_p);

TimIntStr_t TimImp[TIM_HW_NUM] = {
    {
        .TimSet = TIM0_HW_SET_CFG,
        .SetFunc_p = tim0_hw_init
    },
    {
        .TimSet = TIM1_HW_SET_CFG,
        .SetFunc_p = tim1_hw_init
    },
    {
        .TimSet = TIM2_HW_SET_CFG,
        .SetFunc_p = tim2_hw_init
    },
    {
        .TimSet = TIM3_HW_SET_CFG,
        .SetFunc_p = tim3_hw_init
    }
};

TimIntStr_t *TimIntStrList_p[TIM_HW_NUM];

uint8_t tim0_hw_init(TimIntStr_t *str_p) {
    OCR0 = str_p->TimSet.OCRA;

    switch (str_p->TimSet.ClockPreScale){
        case TIM_CLOCK_PRESCALE_NONE:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case TIM_CLOCK_PRESCALE_1:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case TIM_CLOCK_PRESCALE_8:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case TIM_CLOCK_PRESCALE_32:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (1 << CS00));
            break;
        case TIM_CLOCK_PRESCALE_64:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case TIM_CLOCK_PRESCALE_128:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (1  << CS00));
            break;
        case TIM_CLOCK_PRESCALE_256:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (0 << CS00));
            break;

        case TIM_CLOCK_PRESCALE_1024:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (1 << CS00));
            break;
    }
    switch (str_p->TimSet.WaveMode) {
        case TIM_WAVEMODE_Normal:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((0 << WGM01) | (0 << WGM00));
            break;
        case TIM_WAVEMODE_CTC:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((1 << WGM01) | (0 << WGM00));
            break;
    }
    switch (str_p->TimSet.WaveoutA){
        case TIM_WAVEOUT_NORMAL:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (0 << COM00));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (1 << COM00));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case TIM_TIMINTEN_DIS:
            // 禁能
            TIMSK &= ~(1 << OCIE0);
            TIMSK |= (0 << OCIE0);
            break;
        case TIM_TIMINTEN_EN:
            // 致能
            TIMSK &= ~(1 << OCIE0);
            TIMSK |= (1 << OCIE0);
            break;
    }
    TCNT0 = 0;
    return 0 ;
}

uint8_t tim1_hw_init(TimIntStr_t *str_p) {
    OCR1A = str_p->TimSet.OCRA;
    OCR1B = str_p->TimSet.OCRB;
    OCR1C = str_p->TimSet.OCRC;
    switch (str_p->TimSet.ClockPreScale){
        case TIM_CLOCK_PRESCALE_NONE:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case TIM_CLOCK_PRESCALE_1:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (1 << CS10));
            break;
        case TIM_CLOCK_PRESCALE_8:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (0 << CS10));
            break;
        case TIM_CLOCK_PRESCALE_64:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (1 << CS10));
            break;
        case TIM_CLOCK_PRESCALE_256:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case TIM_CLOCK_PRESCALE_1024:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (1 << CS10));
            break;
    }
    switch (str_p->TimSet.WaveMode) {
        case TIM_WAVEMODE_Normal:
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM11) | (0 << WGM10));
            TCCR1B &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM13) | (0 << WGM12));
            break;
        case TIM_WAVEMODE_CTC:
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM11) | (0 << WGM10));
            TCCR1B &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM13) | (1 << WGM12));
            break;
    }
    switch(str_p->TimSet.WaveoutA){
        case TIM_WAVEOUT_NORMAL:
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (0 << COM1A0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (1 << COM1A0));
            break;
    }
    switch(str_p->TimSet.WaveoutB){
        case TIM_WAVEOUT_NORMAL:
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (0 << COM1B0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (1 << COM1B0));
            break;
    }
    switch(str_p->TimSet.WaveoutC){
        case TIM_WAVEOUT_NORMAL:
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (0 << COM1C0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (1 << COM1C0));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case TIM_TIMINTEN_DIS:
            // 禁能
            TIMSK &= ~(1 << OCIE1A);
            TIMSK |= (0 << OCIE1A);
            break;
        case TIM_TIMINTEN_EN:
            // 致能
            TIMSK &= ~(1 << OCIE1A);
            TIMSK |= (1 << OCIE1A);
            break;
    }
    TCNT1 = 0;
    
    return 0 ;
}

uint8_t tim3_hw_init(TimIntStr_t *str_p) {
    OCR3A = str_p->TimSet.OCRA;
    OCR3B = str_p->TimSet.OCRB;
    OCR3C = str_p->TimSet.OCRC;
    switch (str_p->TimSet.ClockPreScale){
        case TIM_CLOCK_PRESCALE_NONE:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case TIM_CLOCK_PRESCALE_1:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (1 << CS30));
            break;
        case TIM_CLOCK_PRESCALE_8:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (0 << CS30));
            break;
        case TIM_CLOCK_PRESCALE_64:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (1 << CS30));
            break;
        case TIM_CLOCK_PRESCALE_256:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case TIM_CLOCK_PRESCALE_1024:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (1 << CS30));
            break;
    }
    switch (str_p->TimSet.WaveMode) {
        case TIM_WAVEMODE_Normal:
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM31) | (0 << WGM30));
            TCCR3B &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM33) | (0 << WGM32));
            break;
        case TIM_WAVEMODE_CTC:
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM31) | (0 << WGM30));
            TCCR3B &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM33) | (1 << WGM32));
            break;
    }
    
    switch(str_p->TimSet.WaveoutA){
        case TIM_WAVEOUT_NORMAL:
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (0 << COM3A0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (1 << COM3A0));
            break;
    }
    switch(str_p->TimSet.WaveoutB){
        case TIM_WAVEOUT_NORMAL:
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (0 << COM3B0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (1 << COM3B0));
            break;
    }
    switch(str_p->TimSet.WaveoutC){
        case TIM_WAVEOUT_NORMAL:
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (0 << COM3C0));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (1 << COM3C0));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case TIM_TIMINTEN_DIS:
            // 禁能
            ETIMSK &= ~(1 << OCIE3A);
            ETIMSK |= (0 << OCIE3A);
            break;
        case TIM_TIMINTEN_EN:
            // 致能
            ETIMSK &= ~(1 << OCIE3A);
            ETIMSK |= (1 << OCIE3A);
            break;
    }
    TCNT3 = 0;
    
    return 0 ;
}


uint8_t tim2_hw_init(TimIntStr_t *str_p) {
    OCR2 = str_p->TimSet.OCRA;
    
    switch(str_p->TimSet.ClockPreScale){
        case TIM_CLOCK_PRESCALE_NONE:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case TIM_CLOCK_PRESCALE_1:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (1 << CS20));
            break;
        case TIM_CLOCK_PRESCALE_8:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (0 << CS20));
            break;
        case TIM_CLOCK_PRESCALE_64:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (1 << CS20));
            break;
        case TIM_CLOCK_PRESCALE_256:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case TIM_CLOCK_PRESCALE_1024:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (1 << CS20));
            break;
    }
    switch(str_p->TimSet.WaveMode){
        case TIM_WAVEMODE_Normal:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((0 << WGM21) | (0 << WGM20));
            break;
        case TIM_WAVEMODE_CTC:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((1 << WGM21) | (0 << WGM20));
            break;
    }
    
    switch (str_p->TimSet.WaveoutA){
        case TIM_WAVEOUT_NORMAL:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (0 << COM20));
            break;
        case TIM_WAVEOUT_TOGGLE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (1 << COM20));
            break;
    }
    switch(str_p->TimSet.TimIntEn){
        case TIM_TIMINTEN_DIS:
            // 禁能
            TIMSK &= ~(1 << OCIE2);
            TIMSK |= (0 << OCIE2);
            break;
        case TIM_TIMINTEN_EN:
            // 致能
            TIMSK &= ~(1 << OCIE2);
            TIMSK |= (1 << OCIE2);
            break;
    }
    TCNT2 = 0;
    
    return 0 ;
}
