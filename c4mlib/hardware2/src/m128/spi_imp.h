/**
 * @file spi_imp.h
 * @author maxwu84
 * @date 2019.09.04
 * @brief 
 * 
 */

#ifndef C4MLIB_HARDWARE_SPI_IMP_H
#define C4MLIB_HARDWARE_SPI_IMP_H

#include "c4mlib/hardware2/src/spi.h"

/* Public Section Start */
#define SPI_HW_NUM 1
extern SpiIntStr_t SpiImp[SPI_HW_NUM];
extern SpiIntStr_t *SpiIntStrList_p[SPI_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDWARE_SPI_IMP_H
