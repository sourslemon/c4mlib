/**
 * @file pwm_imp.c
 * @author maxwu84
 * @brief
 * @date 2019.09.04
 *
 */

#include "c4mlib/hardware2/src/m128/pwm_imp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/pwm.cfg"

static uint8_t pwm0_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm1_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm2_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm3_hw_init(PwmIntStr_t *str_p);

PwmIntStr_t PwmImp[PWM_HW_NUM] = {
    {
        .PwmSet = PWM0_HW_SET_CFG,
        .SetFunc_p = pwm0_hw_init
    },
    {
        .PwmSet = PWM1_HW_SET_CFG,
        .SetFunc_p = pwm1_hw_init
    },
    {
        .PwmSet = PWM2_HW_SET_CFG,
        .SetFunc_p = pwm2_hw_init
    },
    {
        .PwmSet = PWM3_HW_SET_CFG,
        .SetFunc_p = pwm3_hw_init
    }
};

// PwmIntStrList_p is used in std_isr
PwmIntStr_t *PwmIntStrList_p[PWM_HW_NUM];

uint8_t pwm0_hw_init(PwmIntStr_t *str_p) {
    
    switch (str_p->PwmSet.ClockPreScale) {
        case PWM_CLOCK_PRESCALE_NONE:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_1:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_8:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_32:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (1 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_64:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_128:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_256:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case PWM_CLOCK_PRESCALE_1024:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (1 << CS00));
            break;
    }
    switch (str_p->PwmSet.WaveMode) {
        case PWM_WAVE_MODE_PWM:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((0 << WGM01) | (1 << WGM00));
            break;
        case PWM_WAVE_MODE_FASTPWM:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((1 << WGM01) | (1 << WGM00));
            break;
    }
    switch (str_p->PwmSet.WaveOutA) {
        case PWM_WAVEOUT_NORMAL:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (0 << COM00));
            break;
        case PWM_WAVEOUT_TOGGLE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (1 << COM00));
            break;
        case PWM_WAVEOUT_CLEAR:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((1 << COM01) | (0 << COM00));
            break;
        case PWM_WAVEOUT_SET:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((1 << COM01) | (1 << COM00));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
        
            TIMSK &= ~(1 << TOIE0);
            TIMSK |= (0 << TOIE0);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE0);
            TIMSK |= (1 << TOIE0);
            break;
    }
    switch (str_p->PwmSet.PwmOut) {
        case PWM_PWMOUT_A:
            DDRB = 1<<PB4;
            break;
    }
    return 0 ;
}

uint8_t pwm1_hw_init(PwmIntStr_t *str_p) {
    DDRB |= str_p->PwmSet.PwmOut ;
    
    switch (str_p->PwmSet.ClockPreScale) {
        case PWM_CLOCK_PRESCALE_NONE:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_1:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (1 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_8:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (0 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_64:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (1 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_256:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_1024:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (1 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_FALLING:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (1 << CS11)| (0 << CS10));
            break;
        case PWM_CLOCK_PRESCALE_RISING:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (1 << CS11)| (1 << CS10));
            break;
    }
    switch (str_p->PwmSet.WaveMode) {
        case PWM_WAVE_MODE_PWM:
            /* 
             * mode10：
             * WGM13    WGM12   WGM11   WGM10
             * 1        0       1       0
            */
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((1 << WGM11) | (0 << WGM10));
    
            TCCR1B &= ~((1 << WGM13) | (1 << WGM12));
            TCCR1B |= ((1 << WGM13) | (0 << WGM12));
            break;
        case PWM_WAVE_MODE_FASTPWM:
            /* 
             * mode14：
             * WGM13    WGM12   WGM11   WGM10
             * 1        1       1       0
            */
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((1 << WGM11) | (0 << WGM10));
    
            TCCR1B &= ~((1 << WGM13) | (1 << WGM12));
            TCCR1B |= ((1 << WGM13) | (1 << WGM12));
            break;
    }
    switch (str_p->PwmSet.WaveOutA) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (0 << COM1A0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (1 << COM1A0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((1 << COM1A1) | (0 << COM1A0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((1 << COM1A1) | (1 << COM1A0));
            break;

    }
    switch (str_p->PwmSet.WaveOutB) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (0 << COM1B0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (1 << COM1B0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((1 << COM1B1) | (0 << COM1B0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((1 << COM1B1) | (1 << COM1B0));
            break;

    }
    switch (str_p->PwmSet.WaveOutC) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (0 << COM1C0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (1 << COM1C0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((1 << COM1C1) | (0 << COM1C0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((1 << COM1C1) | (1 << COM1C0));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            TIMSK &= ~(1 << TOIE1);
            TIMSK |= (0 << TOIE1);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE1);
            TIMSK |= (1 << TOIE1);
            break;
    }
    switch (str_p->PwmSet.PwmOut) {
        case PWM_PWMOUT_A:
            DDRB = 1<<PB5;
            break;
        case PWM_PWMOUT_B:
            DDRB = 1<<PB6;
            break;
        case PWM_PWMOUT_C:
            DDRB = 1<<PB7;
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_B ) :
            DDRB = (1<<PB5) | (1<<PB6) ;
            break;
        
        case ( PWM_PWMOUT_B | PWM_PWMOUT_C ) :
            DDRB = (1<<PB5) | (1<<PB7);
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_C ) :
            DDRB = (1<<PB5) | (1<<PB7);
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_B | PWM_PWMOUT_C ) :
            DDRB = (1<<PB5) | (1<<PB6) | (1<<PB7);
            break; 
    }
    return 0;
}

uint8_t pwm2_hw_init(PwmIntStr_t *str_p) {

    switch (str_p->PwmSet.ClockPreScale) {
        case PWM_CLOCK_PRESCALE_NONE:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_1:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (1 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_8:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (0 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_64:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (1 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_256:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_1024:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (1 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_FALLING:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (1 << CS21)| (0 << CS20));
            break;
        case PWM_CLOCK_PRESCALE_RISING:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (1 << CS21)| (1 << CS20));
            break;
    }
    switch (str_p->PwmSet.WaveMode) {
        case PWM_WAVE_MODE_PWM:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((0 << WGM21) | (1 << WGM20));
            break;
        case PWM_WAVE_MODE_FASTPWM:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((1 << WGM21) | (1 << WGM20));
            break;
    }
    switch (str_p->PwmSet.WaveOutA) {
        case PWM_WAVEOUT_NORMAL:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (0 << COM20));
            break;
        case PWM_WAVEOUT_TOGGLE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (1 << COM20));
            break;
        case PWM_WAVEOUT_CLEAR:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((1 << COM21) | (0 << COM20));
            break;
        case PWM_WAVEOUT_SET:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((1 << COM21) | (1 << COM20));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            TIMSK &= ~(1 << TOIE2);
            TIMSK |= (0 << TOIE2);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE2);
            TIMSK |= (1 << TOIE2);
            break;
    }
    
    switch (str_p->PwmSet.PwmOut) {
        case PWM_PWMOUT_A:
            DDRB = 1<<PB7;
            break;
    }
    return 0;
}

uint8_t pwm3_hw_init(PwmIntStr_t *str_p){
    DDRE |= str_p->PwmSet.PwmOut ;
    
    switch (str_p->PwmSet.ClockPreScale) {
        case PWM_CLOCK_PRESCALE_NONE:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_1:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (1 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_8:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (0 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_64:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (1 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_256:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_1024:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (1 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_FALLING:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (1 << CS31)| (0 << CS30));
            break;
        case PWM_CLOCK_PRESCALE_RISING:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (1 << CS31)| (1 << CS30));
            break;
    }
    switch (str_p->PwmSet.WaveMode) {
        case PWM_WAVE_MODE_PWM:
            /* 
             * mode10：
             * WGM33    WGM32   WGM31   WGM30
             * 1        0       1       0
            */
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((1 << WGM31) | (0 << WGM30));
    
            TCCR3B &= ~((1 << WGM33) | (1 << WGM32));
            TCCR3B |= ((1 << WGM33) | (0 << WGM32));
            break;
        case PWM_WAVE_MODE_FASTPWM:
            /* 
             * mode14：
             * WGM33    WGM32   WGM31   WGM30
             * 1        1       1       0
            */
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((1 << WGM31) | (0 << WGM30));
    
            TCCR3B &= ~((1 << WGM33) | (1 << WGM32));
            TCCR3B |= ((1 << WGM33) | (1 << WGM32));
            break;
    }
    switch (str_p->PwmSet.WaveOutA) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (0 << COM1A0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (1 << COM3A0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((1 << COM3A1) | (0 << COM3A0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((1 << COM3A1) | (1 << COM3A0));
            break;
    }
    switch (str_p->PwmSet.WaveOutB) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (0 << COM3B0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (1 << COM3B0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((1 << COM3B1) | (0 << COM3B0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((1 << COM3B1) | (1 << COM3B0));
            break;
    }
    switch (str_p->PwmSet.WaveOutC) {
        case PWM_WAVEOUT_NORMAL:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (0 << COM3C0));
            break;
        case PWM_WAVEOUT_TOGGLE:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (1 << COM3C0));
            break;
        case PWM_WAVEOUT_CLEAR:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((1 << COM3C1) | (0 << COM3C0));
            break;
        case PWM_WAVEOUT_SET:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((1 << COM3C1) | (1 << COM3C0));
            break;
    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            ETIMSK &= ~(1 << TOIE3);
            ETIMSK |= (0 << TOIE3);
            break;
        case ENABLE:
            ETIMSK &= ~(1 << TOIE3);
            ETIMSK |= (1 << TOIE3);
            break;
    }
    
    switch (str_p->PwmSet.PwmOut) {
        case PWM_PWMOUT_A:
            DDRE = 1<<PE3;
            break;
        case PWM_PWMOUT_B:
            DDRE = 1<<PE4;
            break;
        case PWM_PWMOUT_C:
            DDRE = 1<<PE5;
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_B ) :
            DDRE = (1<<PE3) | (1<<PE4) ;
            break;
        case ( PWM_PWMOUT_B | PWM_PWMOUT_C ) :
            DDRE = (1<<PE4) | (1<<PE5);
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_C ) :
            DDRE = (1<<PE3) | (1<<PE5);
            break;
        case ( PWM_PWMOUT_A | PWM_PWMOUT_B | PWM_PWMOUT_C ) :
            DDRE = (1<<PE3) | (1<<PE4) | (1<<PE5);
            break; 
    }
    return 0;

}
