/**
 * @file twi_imp.c
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */

#include "c4mlib/hardware2/src/m128/twi_imp.h"

#include "c4mlib/hardware2/src/twi.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/twi.cfg"

static uint8_t twi_hw_init(TwiIntStr_t *IntStr_p);

#define TWI_SLAVE_ID 0x78   // TODO 看是否能改成 user 自己宣告

TwiIntStr_t TwiImp[TWI_HW_NUM] = {
    {
        .TwiSet = TWI_HW_SET_CFG,
        .SetFunc_p = twi_hw_init
    }
};

TwiIntStr_t *TwiIntStrList_p[TWI_HW_NUM];

uint8_t twi_hw_init(TwiIntStr_t *IntStr_p) {
    TWBR = 12 ;
    switch (IntStr_p->TwiSet.TwiEn) {
        case TWI_TWIEN_DIS :
            TWCR &= ~(1 << TWEN);
            TWCR |= (0 << TWEN) ;
            break;
        case TWI_TWIEN_EN :
            TWCR &= ~(1 << TWEN);
            TWCR |= (1 << TWEN) ;
            break;
    }
    switch (IntStr_p->TwiSet.TwiIntEn){
        case TWI_TWIINTEN_DIS :
            TWCR &= ~(1 << TWIE);
            TWCR |= (0 << TWIE) ;
            break;
        case TWI_TWIINTEN_EN :
            TWCR &= ~(1 << TWIE);
            TWCR |= (1 << TWIE) ;
            break;
    }

    switch (IntStr_p->TwiSet.GeneralCall){
        case TWI_GENERAL_CALL_DIS :
            TWAR &= ~(1 << TWGCE);
            TWAR |= (0 << TWGCE) ;
            break;
        case TWI_GENERAL_CALL_EN :
            TWAR &= ~(1 << TWGCE);
            TWAR |= (1 << TWGCE) ;
            break;
    }

    // TODO TwiSet.SlaveAdd
    switch (IntStr_p->TwiSet.SlaveAdd){
        case TWI_MS_SLAVE :
            TWAR = (TWI_SLAVE_ID<<1) | 0b00000001;
            TWCR |= 1 << TWEA ;
            break;
    }

    return 0 ;
}
