/**
 * @file twi.h
 * @author
 * @date 2019.09.04
 * @brief
 * 
 */

#ifndef C4MLIB_HARDWARE2_TWI_H
#define C4MLIB_HARDWARE2_TWI_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __twi_set_str {
    uint8_t TwiIntEn;
    uint8_t TwiEn;
    uint8_t GeneralCall;
    uint8_t SlaveAdd;
} TwiSetStr_t;

typedef struct __twi_int_str {
    TwiSetStr_t TwiSet;

    uint8_t (*SetFunc_p)(struct __twi_int_str *);  ///< 初始化

    uint8_t IntTotal;                                  ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_TWIINT_FUNCNUM]; ///< 紀錄所有已註冊的中斷函式
} TwiIntStr_t;

// Netting TwiIntStr_p to TwiIntStrList_p
uint8_t TwiInt_net(TwiIntStr_t* TwiIntStr_p, uint8_t Num);

uint8_t TwiInt_set(TwiIntStr_t* IntStr_p);

// Register TWI interrupt function and parameters that user define
uint8_t TwiInt_reg(TwiIntStr_t* TwiIntStr_p, Func_t FbFunc_p, void* FbPara_p);

// Control TWI interrupt functional block
void TwiInt_en(TwiIntStr_t* TwiIntStr_p, uint8_t Fb_Id, uint8_t enable);
/* Public Section End */

// Called by internal ISR, private function
void TwiInt_step(TwiIntStr_t* TwiIntStr_p);

/*----- Twi Sets Macros -----------------------------------------------------*/
/*TWI Interrupt Enable*/
#define TWI_TWIINTEN_DIS 0
#define TWI_TWIINTEN_EN 1

/* TWI Enable Bit */
#define TWI_TWIEN_DIS 0
#define TWI_TWIEN_EN 1

/* TWI General Call Recognition Enable */
#define TWI_GENERAL_CALL_DIS 0
#define TWI_GENERAL_CALL_EN 1

/* TWI (Slave) Address Register */
#define TWI_MS_MASTER 0
#define TWI_MS_SLAVE 1
/*---------------------------------------------------------------------------*/

#endif  // C4MLIB_HARDWARE2_TWI_H
