/**
 * @file uart.h
 * @author
 * @date
 * @brief
 * 
 */

#ifndef C4MLIB_HARDWARE2_UART_H
#define C4MLIB_HARDWARE2_UART_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __uart_set_str {
    uint8_t Parity;     ///< 同位位元
    uint8_t StopBits;   ///< 停止位元
    uint8_t WordBits;   ///< 每筆位元
    uint8_t TxRxEn;     ///< TXRX禁致能
    uint8_t TxRxIntEn;  ///< TXRX中斷禁致能
    uint8_t MutiMcu;    ///< 多MCU
    uint8_t DoubleBREn; ///< 雙倍鮑率禁致能
    uint16_t BaudRate;  ///< 鮑率
} UartSetStr_t;

typedef struct __uart_int_str {
    UartSetStr_t UartSet;

    uint8_t (*SetFunc_p)(struct __uart_int_str *);

    uint8_t TxIntTotal;                                   ///< 紀錄已有多少中斷已註冊
    uint8_t RxIntTotal;                                   ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t TxIntFb[MAX_UARTINT_FUNCNUM]; ///< 紀錄所有已註冊的中斷函式
    volatile FuncBlockStr_t RxIntFb[MAX_UARTINT_FUNCNUM]; ///< 紀錄所有已註冊的中斷函式
} UartIntStr_t;

uint8_t UartInt_net(UartIntStr_t* UartIntStr_p,uint8_t Num);

uint8_t UartInt_set(UartIntStr_t* IntStr_p);

uint8_t UartTxInt_reg(UartIntStr_t* UartIntStr_p, Func_t FbFunc_p, void* FbPara_p);
uint8_t UartRxInt_reg(UartIntStr_t* UartIntStr_p, Func_t FbFunc_p, void* FbPara_p);

void UartTxInt_en(UartIntStr_t* UartIntStr_p, uint8_t Fb_Id, uint8_t enable);
void UartRxInt_en(UartIntStr_t* UartIntStr_p, uint8_t Fb_Id, uint8_t enable);
/* Public Section End */

void UartTx_step(UartIntStr_t* UartIntStr_p);
void UartRx_step(UartIntStr_t* UartIntStr_p);

/* Public Section Start */
/*----- Uart Sets Macros ----------------------------------------------------*/
/* Parity*/
#define UART_PARITY_NONE 0
#define UART_PARITY_ODD  2
#define UART_PARITY_EVEN 3

/* StopBits*/
#define UART_STOPBITS_1 0
#define UART_STOPBITS_2 1

/* WordBits */
#define UART_WORDLENGTH_5 0
#define UART_WORDLENGTH_6 1
#define UART_WORDLENGTH_7 2
#define UART_WORDLENGTH_8 3
#define UART_WORDLENGTH_9 9

/* TxRxEn */
#define UART_TXRXEN_NONE 0
#define UART_TXRXEN_TX   1
#define UART_TXRXEN_RX   2
#define UART_TXRXEN_TXRX 3

/* TxRxIntEn */
#define UART_INTEN_NONE 0
#define UART_INTEN_TX   1
#define UART_INTEN_RX   2
#define UART_INTEN_TXRX 3

/* MultyMCU */
#define UART_MCU_SINGLE 0
#define UART_MCU_MULTY  1

/* DoubleBREn - double baud rate enable */
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_UART_H
