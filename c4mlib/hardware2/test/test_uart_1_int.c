/**
 * @file test_uart_1_int.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief
 *
 * 確認有輸出 1 ，以保證有進入 UART1 TX 中斷
 *
 * 測試裝置：ASA_M128_V2
 * 測試硬體：UART1
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/uart.h"

#include "c4mlib/config/uart.cfg"

void int_func(void *p);

volatile uint16_t C;

int main(void) {
    C4M_DEVICE_set();

    printf("uart 1 interrupt test start ----------------------------------\n");

    UartIntStr_t uart1 = UART_INIT;
    uart1.UartSet.TxRxIntEn = UART_INTEN_TX;

    UartInt_net(&uart1, 1);

    UartTxInt_reg(&uart1, &int_func, 0);
    UartTxInt_en(&uart1, 0, 1);

    UartInt_set(&uart1);

    sei();

    ASABUS_UART_transmit('1');
    _delay_ms(20);
    printf("%d\n", C);

    return 0;
}

void int_func(void *p) {
    C++;
}
