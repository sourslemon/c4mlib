/**
 * @file test_adc_init.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief 測試 ADC 初始化狀況
 *
 * 確認 ADMUX 與 ADCSRA 有改變，確認 set 與 initFunc 有正常連接。
 * 測試裝置：ASA_M128_V2
 * 測試硬體：ADC0
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/adc.h"

#include "c4mlib/config/adc.cfg"

int main() {
    C4M_DEVICE_set();

    printf("ADC0 test ----------------------------------------------------\n");

    uint8_t res;

    AdcIntStr_t ADC0 = ADC0_STR_INI;

    res = AdcInt_net(&ADC0, 0);
    printf("res of AdcInt_net is %d\n", res);

    res = AdcInt_set(&ADC0);
    printf("res of AdcInt_set is %d\n", res);

    printf("ADMUX  = %d\n", ADMUX);
    printf("ADCSRA = %d\n", ADCSRA);

    return 0;
}
