/**
 * @file test_slave_spiint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請確認printf出來的succeed_cnt持續增加。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/spi.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

SpiIntStr_t SpiInt0 = SPIINT_0_STR_INI;

typedef struct context {
    uint8_t addr;
    uint8_t data[2];
    uint8_t cnt;
} UserContext_t;

void init_spi(void);

/* User define function, when SPI interrupt, it will call. */
void USER_FUNCTION(void* pvData_p);

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();

    /***** Test the SPI interrupt*****/
    printf("======= Test SpiInt =======\n");
    printf("Please press salve reset first, and then press master reset.\n");

    // Define user context.
    UserContext_t SPI_context = {0, {0, 0}, 0};

    /* Netting standart interrupt structure to INTERNAL_ISR. */
    SpiInt_net(&SpiInt0, 0);
    /* Register the USER_FUNCTION to SPI interrupt, and SPI_context is the
     * parameters that user feed. */
    uint8_t id1 = SpiInt_reg(&SpiInt0, USER_FUNCTION, &SPI_context);
    SpiInt_en(&SpiInt0, id1, true);

    printf("Enable Global Interrupt\n");
    /** Initialize SPI hardware registers as slave mode. */
    init_spi();
    /** Enable interrupts */
    sei();

    uint8_t succeed_cnt = 0, cnt = 0;
    while (true) {
        _delay_ms(3000);
        if(SPI_context.addr == 0xaa) {
            if(SPI_context.data[0] == cnt++) {
                if(SPI_context.data[1] == cnt++) {
                    succeed_cnt++;
                }
            }
        }
        DEBUG_INFO("addr:%x, data[0]:%x, data[1]:%x\n", SPI_context.addr, SPI_context.data[0], SPI_context.data[1]);
        printf("succeed_cnt : %d\n", succeed_cnt);
    }
}

// Initialize SPI hardware as slave.
void init_spi(void) {
    DDRB &= ~(1 << BUS_SPI_MOSI) & ~(1 << BUS_SPI_SCK) & ~(1 << BUS_SPI_SS);
    DDRB |= (1 << BUS_SPI_MISO);
    // Enable SPI
    SPCR = (1 << SPE);
    SPCR |= (1 << SPIE);
}

void USER_FUNCTION(void* pvData_p) {
    UserContext_t* user_context = pvData_p;
    if(user_context->cnt == 0) {
        user_context->addr = SPDR;
        user_context->cnt++;
    }
    else if(user_context->cnt == 1) {
        user_context->data[0] = SPDR;
        user_context->cnt++;
    }
    else {
        user_context->data[1] = SPDR;
        user_context->cnt = 0;
    }
}
