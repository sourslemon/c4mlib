/**
 * @file test_pwm_2_int.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief
 *
 * 確認有輸出 enter inerttupt!! ，以保證有進入 PWM2 中斷
 * 測試裝置：ASA_M128_V2
 * 測試硬體：TIMER 2
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/pwm.h"

#include "c4mlib/config/pwm.cfg"

void int_func(void *);

int main(void) {
    C4M_DEVICE_set();

    printf("PWM init test ------------------------------------------------\n");
    sei();

    PwmIntStr_t pwm = PWM2_INIT;

    PwmInt_net(&pwm, 0);

    PwmInt_reg(&pwm, int_func, 0);
    PwmInt_en(&pwm, 0, 1);

    PwmInt_set(&pwm);

    while (1) {
        asm("nop");
    }

    return 0;
}

void int_func(void *p) {
    printf("enter inerttupt!! \n");
}
