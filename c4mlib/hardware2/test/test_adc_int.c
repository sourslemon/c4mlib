/**
 * @file test_adc_int.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief 測試 ADC 中斷註冊
 *
 * 確認有輸出enter adc isr!，以保證有進入ADC中斷
 * 測試裝置：ASA_M128_V2
 * 測試硬體：ADC0
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/adc.h"

#include "c4mlib/config/adc.cfg"

void adc_int_func(void *p);

int main() {
    C4M_DEVICE_set();

    printf("ADC0 interrupt test ------------------------------------------\n");
    sei();

    uint8_t res;

    AdcIntStr_t ADC0 = ADC0_STR_INI;
    ADC0.AdcSet.IntEn = ENABLE;

    res = AdcInt_net(&ADC0, 0);
    printf("res of AdcInt_net is %d\n", res);

    res = AdcInt_reg(&ADC0, adc_int_func, 0);
    printf("res of AdcInt_reg is %d\n", res);
    AdcInt_en(&ADC0, 0, true);

    res = AdcInt_set(&ADC0);
    printf("res of AdcInt_set is %d\n", res);

    res = AdcInt_trig(&ADC0);
    printf("res of AdcInt_trig is %d\n", res);

    while (1) {
        asm("nop");
    }

    return 0;
}

void adc_int_func(void *p) {
    printf("enter adc isr!\n");
}
