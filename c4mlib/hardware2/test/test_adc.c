/**
 * @file test_adc.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief 測試ADC所有相關函式及其功能
 *
 * ADC 總成測試，需要確認ADC有正常取值，使用的ADC通道是1.23V，
 * 所以取得的資料應為10bits範圍一半，511。
 * 測試裝置：ASA_M128_V2
 * 測試硬體：ADC0
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/adc.h"

#include "c4mlib/config/adc.cfg"

#define MY_ADC0_SET_STR_INI {               \
    .Enable = ENABLE,                       \
    .Channel = ADC_CHANNEL_1230MV,          \
    .ClockPrescaler = ADC_CLOCKPRESCALER_2, \
    .Resolution = ADC_RESOLUTION_10,        \
    .DataAlign = ADC_DATAALIGN_RIGHT,       \
    .Reference = ADC_REF_AREF,              \
    .IntEn = ENABLE               \
}

#define MY_ADC0_STR_INI {          \
    .AdcSet = MY_ADC0_SET_STR_INI  \
}

void adc_int_func(void *p);

int main() {
    C4M_DEVICE_set();

    printf("ADC0 test start ----------------------------------------------\n");
    sei();

    uint16_t data;
    uint8_t res;

    AdcIntStr_t ADC0 = MY_ADC0_STR_INI;

    res = AdcInt_net(&ADC0, 0);
    printf("res of AdcInt_net is %d\n", res);

    res = AdcInt_reg(&ADC0, adc_int_func, 0);
    printf("res of AdcInt_reg is %d\n", res);
    AdcInt_en(&ADC0, 0, true);

    res = AdcInt_set(&ADC0);
    printf("res of AdcInt_set is %d\n", res);

    printf("ADMUX  = %d\n", ADMUX);
    printf("ADCSRA = %d\n", ADCSRA);

    res = AdcInt_trig(&ADC0);
    printf("res of AdcInt_trig is %d\n", res);

    while (!AdcInt_isDone(&ADC0)) {
        printf("Undone\n");
    }
    res = AdcInt_getConv(&ADC0, &data);
    printf("res of AdcInt_getConv is %d\n", res);
    printf("get data is %d, data should be 511\n", data);

    return 0;
}

void adc_int_func(void *p) {
    printf("enter adc isr!!\n");
}
