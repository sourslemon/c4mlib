/**
 * @file test_master_twiint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請確認printf出來的succeed_cnt持續增加。
 */
#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* User define TWI ID */
#define TWI_SLAVE_ID 0x78
/* Hardware TWI status code from Atmega128 datasheet. */
#define START_ACK 0x08
#define M128_ACK_ADDRESS_RECIEVE 0x40
#define M128_ACK_ADDRESS_TRANSMIT 0x18
#define M128_ACK_DATA_TRANSMIT 0x28

/* Setting TWI hardware function start. */
void init_master_twi(void);
void startTWI();
void stopTWI();
void setTWIAddress(uint8_t address, bool writeModeOrNot);
void setTWIData(uint8_t send_data, bool ackOrNot);
uint8_t getTWIData(bool ackOrNot);
/* Setting TWI hardware function end. */
/* Setting TIMER hardware function start. */
void init_timer(void);
/* Setting TIMER hardware function start. */

ISR(TIMER3_COMPA_vect) { HAL_tick(); }

int main(void) {
    C4M_STDIO_init();
    ASABUS_ID_init();
    /** Initialize I2C(TWI) hardware registers as master mode. */
    init_master_twi();
    HAL_time_init();
    init_timer();
    uint8_t send_data = 0;
    /***** Test the TWI interrupt*****/
    printf("======= Test TWI interrupt =======\n");
    printf("Please press salve reset first, and then press master reset.\n");
    sei();
    while (true) {
        DEBUG_INFO("Start to send TWI data !!!\n");
        startTWI();
        setTWIAddress(TWI_SLAVE_ID, true);
        setTWIData(send_data++, false);
        setTWIData(send_data++, true);
        stopTWI();
        DEBUG_INFO("OK\n");
        HAL_delay(3000);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

void init_master_twi(void) { TWBR = 12; }

void startTWI(void) {
    // Transmit start condition
    TWCR |= (1 << TWSTA);  // TWI Start
    TWCR |= (1 << TWINT);  // Clear TWING flag
    TWCR |= (1 << TWEN);   // TWI Enable
    // Wait for TWINT flag set. This indicates that the START condition has been
    // transmitted.
    while (!(TWCR & (1 << TWINT)))
        ;
    // Check for the acknowledgement
    if ((TWSR & 0xF8) != START_ACK) {
        DEBUG_INFO("Start fail !!!\n");
    }
}

void stopTWI() {
    // Transmit stop condition
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}

void setTWIAddress(uint8_t address, bool writeModeOrNot) {
    if (writeModeOrNot) {
        TWDR = ((address << 1) & (~1));
    } else {
        TWDR = ((address << 1) | (1));
    }
    DEBUG_INFO("TWDR:%x\n", TWDR);
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)))
        // Check for the acknowledge
        _delay_ms(1);
    if (writeModeOrNot) {
        if ((TWSR & 0xF8) != M128_ACK_ADDRESS_TRANSMIT) {
            DEBUG_INFO("Transmission mode, slave not response !!!\n");
            DEBUG_INFO("Status is %x\n", (TWSR & 0xF8));
        }
    } else {
        if ((TWSR & 0xF8) != M128_ACK_ADDRESS_RECIEVE) {
            DEBUG_INFO("Read mode, slave not response !!!\n");
            DEBUG_INFO("Status is %x\n", (TWSR & 0xF8));
        }
    }
}

void setTWIData(uint8_t send_data, bool ackOrNot) {
    TWDR = send_data;
    TWCR = (1 << TWINT) | (1 << TWEN) | (ackOrNot << TWEA);

    while (!(TWCR & (1 << TWINT)))
        ;
    _delay_ms(1);
    if ((TWSR & 0xF8) != M128_ACK_DATA_TRANSMIT) {
        DEBUG_INFO("Set data ACK not valid !!!\n");
        DEBUG_INFO("Status is %x\n", (TWSR & 0xF8));
    }
}

uint8_t getTWIData(bool ackOrNot) {
    if (ackOrNot) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
    } else {
        TWCR = (1 << TWINT) | (1 << TWEN);
    }
    while (!(TWCR & (1 << TWINT)))
        ;
    return TWDR;
}
