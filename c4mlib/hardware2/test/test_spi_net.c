#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/spi.h"

#include "c4mlib/config/spi.cfg"

int main(void) {
    C4M_DEVICE_set();

    printf("test spi init start\n");
    SpiIntStr_t spi = SPI_MASTER_INIT;
    
    SpiInt_net(&spi, 0);
    SpiInt_set(&spi);

    printf("spi.Init = %x, spi =%x \n ",spi.SetFunc_p, &spi);
    spi.SetFunc_p(&spi);
    
    return 0;
}
