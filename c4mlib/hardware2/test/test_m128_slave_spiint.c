/**
 * @file test_m128_slave_spiint.c
 * @author Deng Xiang-Guan
 * @brief Slave recieve data [address data1 data2 dummy] and send data recieved to master.
 * @setting Slave, CPOL=0, CPHA=0, LSB, clock divide 64
 * @date 2019.09.05
 */

#include "c4mlib/hardware2/src/spi.h"
#include "c4mlib/hardware2/src/spi_imp.h"
#include "c4mlib/device/src/device.h"

#define ASA_ID 4
#define TEST_ADDR 0x87

#define MY_SPI_SET_STR_INI {             \
    .MS = SPI_MS_SLAVE,                 \
    .SpiEn = ENABLE,               \
    .SpiIntEn = ENABLE,         \
    .DataOrder = SPI_DATAORDER_MSB,      \
    .ClockPol = SPI_POLARITY_LEAD_RISE,  \
    .ClockPha = SPI_PHASE_LEAD_SAMPLE,   \
    .ClockDiv = SPI_DIVEDER_64             \
}

#define SPI_STR_INI {             \
    .SpiSet = MY_SPI_SET_STR_INI,  \
}

typedef struct context {
    uint16_t cnt;
    uint8_t addr;
    uint8_t flag;
    uint8_t rec[2];
    uint8_t IntTotal;
} UserContext_t;

void USER_FUNCTION(void* pvData_p);

int main() {
    C4M_DEVICE_set();

    printf("SPI slave test \n");

    uint8_t res, id;

    SpiIntStr_t SPI0 = SPI_STR_INI;
    UserContext_t user_context = {0, 0, 0, {0, 0}, sizeof(user_context.rec)};
    
    res = SpiInt_net(&SPI0, 0);
    printf("res of SpiInt_net is %d\n", res);

    id = SpiInt_reg(&SPI0, USER_FUNCTION, &user_context);
    printf("Function block ID is %d\n", id);
    SpiInt_en(&SPI0, id, true);

    res = SpiInt_set(&SPI0);
    printf("res of SpiInt_set is %d\n", res);

    printf("====== hardware register show start ======\n");
    printf("SPCR  (hex):%x\n", SPCR);
    printf("SPSR  (hex):%x\n", SPSR);
    printf("====== hardware register show end ======\n");
    printf("Enable interrupt\n");
    sei();

    uint8_t success_cnt = 0;
    uint8_t ans_cnt = 0;
    while(true) {
        _delay_ms(3000);
        cli();
        printf("slave, rec[0]:%d, rec[2]:%d\n", user_context.rec[0], user_context.rec[1]);
        ans_cnt++;
        if (user_context.rec[0] == ans_cnt) {
            ans_cnt++;
            if (user_context.rec[1] == ans_cnt) {
                success_cnt++;
            }
        }
        if(success_cnt != 0){
            printf("Test slave success %d\n", success_cnt);
        }
        sei();
    }

    return 0;
}

void USER_FUNCTION(void* pvData_p) {
    UserContext_t* user_context = pvData_p;
    uint8_t tempData = SPIS_Inst.read_byte();
    SPIS_Inst.write_byte(tempData);
    if((tempData == TEST_ADDR) && user_context->flag == false) {
        user_context->flag = true;
    }
    else if(user_context->flag) {
        if(user_context->cnt < user_context->IntTotal) {
            user_context->rec[user_context->cnt] = tempData;
        }
         user_context->cnt++;
        // Last one master send dummy, return last data.
        if(user_context->cnt == user_context->IntTotal + 1) {
            user_context->cnt = 0;
            user_context->flag = false;
        }
    }
}
