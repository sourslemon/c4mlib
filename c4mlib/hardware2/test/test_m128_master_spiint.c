/**
 * @file test_m128_master_spiint.c
 * @author Deng Xiang-Guan
 * @brief Master transmits [address data1 data2 dummy] and swaps [dummy address data1 data2] from slave.
 * @setting Master, CPOL=0, CPHA=0, LSB, clock divide 64.
 * @date 2019.09.05
 */

#include "c4mlib/hardware2/src/spi.h"
#include "c4mlib/hardware2/src/spi_imp.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/asabus/src/pin_def.h"

#define ASA_ID 4
#define TEST_ADDR 0x87

#define MY_SPI_SET_STR_INI {             \
    .MS = SPI_MS_MASTER,                 \
    .SpiEn = ENABLE,               \
    .SpiIntEn = ENABLE,         \
    .DataOrder = SPI_DATAORDER_MSB,      \
    .ClockPol = SPI_POLARITY_LEAD_RISE,  \
    .ClockPha = SPI_PHASE_LEAD_SAMPLE,   \
    .ClockDiv = SPI_DIVEDER_64             \
}

#define SPI_STR_INI {             \
    .SpiSet = MY_SPI_SET_STR_INI,  \
}

typedef struct context {
    uint16_t cnt;
    uint8_t addr;
    uint8_t rec[2];
} UserContext_t;

void USER_FUNCTION(void* pvData_p);

int main() {
    C4M_DEVICE_set();

    printf("SPI master test \n");

    uint8_t res, id;

    SpiIntStr_t SPI0 = SPI_STR_INI;
    UserContext_t user_context = {0, 0, {0, 0}};
    
    res = SpiInt_net(&SPI0, 0);
    printf("res of SpiInt_net is %d\n", res);

    id = SpiInt_reg(&SPI0, USER_FUNCTION, &user_context);
    printf("Function block ID is %d\n", id);
    SpiInt_en(&SPI0, id, true);

    res = SpiInt_set(&SPI0);
    printf("res of SpiInt_set is %d\n", res);
    printf("====== hardware register show start ======\n");
    printf("SPCR  (hex):%x\n", SPCR);
    printf("SPSR  (hex):%x\n", SPSR);
    printf("====== hardware register show end ======\n");
    printf("Enable interrupt\n");
    sei();

    uint8_t send_data = 0;
    uint8_t success_cnt = 0;
    uint8_t ans_cnt = 0;
    ASABUS_ID_set(ASA_ID);
    while(true) {
        ASA_CS_PORT &= ~(1 << ASA_CS_SHIFT);
        ASABUS_SPI_swap(TEST_ADDR);
        user_context.addr = ASABUS_SPI_swap(send_data++);
        user_context.rec[0] = ASABUS_SPI_swap(send_data++);  
        user_context.rec[1] = ASABUS_SPI_swap(0);   // dummy
        ASA_CS_PORT |= (1 << ASA_CS_SHIFT);
        printf("master, rec[0]:%d, rec[1]:%d\n", user_context.rec[0], user_context.rec[1]);
        if(user_context.addr == TEST_ADDR) {
            ans_cnt++;
            if (user_context.rec[0] == ans_cnt) {
                ans_cnt++;
                if (user_context.rec[1] == ans_cnt) {
                    success_cnt++;
                }
            }
        }
        
        if(success_cnt != 0){
            printf("Test master success %d\n", success_cnt);
        }
        _delay_ms(3000);
    }

    return 0;
}

void USER_FUNCTION(void* pvData_p) {
    UserContext_t* user_context = pvData_p;
    user_context->cnt++;
    printf("Send SPI data complete, count:%d\n", user_context->cnt);
}
