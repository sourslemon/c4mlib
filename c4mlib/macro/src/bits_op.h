/**
 * @file bits_op.h
 * @author LiYu87
 * @date 2019.01.28
 * @brief Provide ASA standard bitswise operation.
 */

#ifndef C4MLIB_MACRO_BITS_OP_H
#define C4MLIB_MACRO_BITS_OP_H

#define SETBIT(ADDRESS, BIT) ((ADDRESS) |= (1 << BIT))
#define CLRBIT(ADDRESS, BIT) ((ADDRESS) &= ~(1 << BIT))
#define CHKBIT(ADDRESS, BIT) (((ADDRESS) & (1 << BIT)) == (1 << BIT))

#define REGFPT(ADDRESS, MASK, SHIFT, DATA) \
    ((ADDRESS) = (((ADDRESS) & (~MASK)) | (((DATA) << (SHIFT)) & (MASK))))
#define REGFGT(ADDRESS, MASK, SHIFT, DATA_P) \
    ((*((char*)DATA_P)) = (((ADDRESS) & (MASK)) >> (SHIFT)))

#endif  // C4MLIB_MACRO_BITS_OP_H
