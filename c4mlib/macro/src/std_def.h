/**
 * @file std_def.h
 * @author LiYu87
 * @date 2019.09.06
 * @brief Provide C4MLIB standard macros.
 *
 */

#ifndef C4MLIB_MACRO_STD_DEF_H
#define C4MLIB_MACRO_STD_DEF_H

#define DISABLE 0  ///< 0 啟用
#define ENABLE  1  ///< 1 關閉

#define IS_ENABLE_OR_DISABLE(EN) ((EN) == ENABLE || (EN) == DISABLE)

#endif  // C4MLIB_MACRO_STD_DEF_H
